package mako.com.lite.data.helpers.sharedPreferences

import android.content.Context
import android.content.SharedPreferences
import mako.com.lite.LiteApplication.Companion.PREFS_NAME

class SharedPreferencesHelper(context: Context) : SharedPreferencesHelperInterface {
    private var preferences: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    override fun loadBoolean(key: String, defaultValue: Boolean): Boolean {
        return preferences.getBoolean(key, defaultValue)
    }

    override fun loadLong(key: String, defaultValue: Long): Long {
        return preferences.getLong(key, defaultValue)
    }

    override fun loadString(key: String, defaultValue: String): String? {
        return preferences.getString(key, defaultValue)
    }

    override fun loadInt(key: String, defaultValue: Int): Int {
        return preferences.getInt(key, defaultValue)
    }


    override fun edit(block: SharedPreferences.Editor.() -> Unit) {
        val editor = preferences.edit()
        block(editor)
        editor.apply()
    }
}