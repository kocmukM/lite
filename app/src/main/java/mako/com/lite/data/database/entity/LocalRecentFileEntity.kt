package mako.com.lite.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "local_recent_file", primaryKeys = ["file_directory", "file_name"])

data class LocalRecentFileEntity(@ColumnInfo(name = "file_directory") var fileDirectory: String,
                                 @ColumnInfo(name = "file_name") var fileName: String,
                                 @ColumnInfo(name = "file_crypto_password") var fileCryptoPassword: String,
                                 @ColumnInfo(name = "file_open_time") var fileOpenTime: Long)