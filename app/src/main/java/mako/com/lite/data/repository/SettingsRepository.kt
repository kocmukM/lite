package mako.com.lite.data.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import mako.com.lite.LiteApplication.Companion.DEFAULT_FABRIC_VALUE
import mako.com.lite.LiteApplication.Companion.DEFAULT_SYNC_VALUE
import mako.com.lite.LiteApplication.Companion.DEFAULT_TIMEOUT_MILLIS_VALUE
import mako.com.lite.LiteApplication.Companion.FEEDBACK_KEY
import mako.com.lite.LiteApplication.Companion.SYNC_KEY
import mako.com.lite.LiteApplication.Companion.TIMEOUT_MILLIS_KEY
import mako.com.lite.data.helpers.sharedPreferences.SharedPreferencesHelper
import mako.com.lite.data.helpers.sharedPreferences.SharedPreferencesHelperInterface
import javax.inject.Inject

class SettingsRepository @Inject constructor(private val sharedPreferencesHelper: SharedPreferencesHelperInterface) {

    var timeOutMillis = DEFAULT_TIMEOUT_MILLIS_VALUE
    var isFabricActivated = DEFAULT_FABRIC_VALUE
    var syncValue= DEFAULT_SYNC_VALUE

    fun loadSettings(): Deferred<Unit> = CoroutineScope(Dispatchers.Default).async {
        timeOutMillis = sharedPreferencesHelper.loadLong(TIMEOUT_MILLIS_KEY, DEFAULT_TIMEOUT_MILLIS_VALUE)
        isFabricActivated = sharedPreferencesHelper.loadBoolean(FEEDBACK_KEY, DEFAULT_FABRIC_VALUE)
        syncValue = sharedPreferencesHelper.loadBoolean(SYNC_KEY, DEFAULT_SYNC_VALUE)
    }

    fun saveSettings(): Deferred<Unit> = CoroutineScope(Dispatchers.Default).async {
        sharedPreferencesHelper.edit {
            putLong(TIMEOUT_MILLIS_KEY, timeOutMillis)
            putBoolean(FEEDBACK_KEY, isFabricActivated)
            putBoolean(SYNC_KEY,syncValue)
        }
    }

    fun setToDefault(): Deferred<Unit> = CoroutineScope(Dispatchers.Default).async {
        timeOutMillis = DEFAULT_TIMEOUT_MILLIS_VALUE
        isFabricActivated = DEFAULT_FABRIC_VALUE
        syncValue= DEFAULT_SYNC_VALUE
    }

}