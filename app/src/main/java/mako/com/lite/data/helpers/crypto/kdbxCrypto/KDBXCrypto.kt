package mako.com.lite.data.helpers.crypto.kdbxCrypto

import de.slackspace.openkeepass.KeePassDatabase
import de.slackspace.openkeepass.domain.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import mako.com.lite.LiteApplication.Companion.KDBX_DATABASE_NAME
import mako.com.lite.data.helpers.icons.IconHelper
import mako.com.lite.data.repository.dto.EntryDto
import mako.com.lite.data.repository.dto.GroupDto
import mako.com.lite.data.repository.dto.RecentFileDto
import java.io.File
import java.util.*

class KDBXCrypto : KDBXCryptoInterface {
    lateinit var meta: Meta
    lateinit var customIcons: CustomIcons

    @Throws(Exception::class)
    override fun read(kdbxFile: File, password: String, recentFileDto: RecentFileDto): Deferred<GroupDto?> = CoroutineScope(Dispatchers.Default).async {
        val database = KeePassDatabase.getInstance(kdbxFile).openDatabase(password)
        meta = database.meta

        val groupDto = GroupDto()
        groupDto.realIndex = 0
        groupDto.groupName = "root"
        groupDto.groups = LinkedList()
        groupDto.entries = LinkedList()
        val childGroups = database.topGroups
        val groups = LinkedList<GroupDto>()
        for ((i, group) in childGroups.withIndex()) {
            groups.add(reader(group, i, recentFileDto).await())

        }
        groupDto.groups = groups

        val childEntries = database.topEntries
        val entries = LinkedList<EntryDto>()
        for ((i, entry) in childEntries.withIndex()) {
            entries.add(buildEntryDto(entry, i))
        }
        groupDto.entries = entries
        groupDto
    }

    override fun write(groupDto: GroupDto, kdbxFile: File, password: String): Deferred<Unit> = CoroutineScope(Dispatchers.Default).async {
        val group = groupBuilder(groupDto).await()
        val icons = IconHelper.icons
        val iconsBuilder = CustomIconsBuilder()
        for (icon in icons) {
            val customIcon = CustomIconBuilder().uuid(icon.uuid).data(icon.iconData).build()
            iconsBuilder.addIcon(customIcon)
        }

        customIcons = iconsBuilder.build()
        meta = MetaBuilder(KDBX_DATABASE_NAME).customIcons(customIcons).build()


        val keePassFile = KeePassFileBuilder(KDBX_DATABASE_NAME)
                .addTopGroups(group)

        keePassFile.withMeta(meta)

        KeePassDatabase.write(keePassFile.build(), password, kdbxFile.absolutePath)
    }


    private fun reader(group: Group, realIndex: Int, recentFileDto: RecentFileDto): Deferred<GroupDto> = CoroutineScope(Dispatchers.Default).async {
        val groupDto = GroupDto()
        groupDto.realIndex = realIndex
        groupDto.groupName = group.name
        groupDto.groups = LinkedList()
        groupDto.entries = LinkedList()
        val childGroups = group.groups
        val groups = LinkedList<GroupDto>()
        for ((i, childGroup) in childGroups!!.withIndex()) {
            groups.add(reader(childGroup, i, recentFileDto).await())
        }
        groupDto.groups = groups

        val childEntries = group.entries
        val entries = LinkedList<EntryDto>()
        for ((i, entry) in childEntries!!.withIndex()) {
            entries.add(buildEntryDto(entry, i))
        }
        groupDto.entries = entries
        groupDto
    }

    private fun buildEntryDto(entry: Entry, realIndex: Int): EntryDto {
        val entryDto = EntryDto()
        entryDto.realIndex = realIndex
        entryDto.title = entry.title
        entryDto.url = entry.url
        entryDto.username = entry.username
        entryDto.password = entry.password
        entryDto.iconData = entry.iconData
        entryDto.customIconUUID = entry.customIconUuid
        entryDto.notes = entry.notes
        return entryDto
    }

    private fun groupBuilder(groupDto: GroupDto): Deferred<Group> = CoroutineScope(Dispatchers.Default).async {
        val root = GroupBuilder(groupDto.groupName)

        for (entry in groupDto.entries!!) {
            val entryBuilder = EntryBuilder(entry.title)
                    .username(entry.username)
                    .url(entry.url)
                    .password(entry.password)
                    .notes(entry.notes)
            if (entry.customIconUUID != null)
                entryBuilder.customIconUuid(entry.customIconUUID)
            root.addEntry(entryBuilder.build())
        }
        for (group in groupDto.groups!!) {
            root.addGroup(groupBuilder(group).await())
        }
        root.build()
    }
}