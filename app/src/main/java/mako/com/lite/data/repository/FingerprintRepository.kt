package mako.com.lite.data.repository

import mako.com.lite.data.helpers.fingerprint.AndroidIndependentFingerprintCallbacks
import mako.com.lite.data.helpers.fingerprint.FingerprintHelperInterface
import javax.inject.Inject

class FingerprintRepository @Inject constructor(private val fingerprintHelper: FingerprintHelperInterface) {

    @Throws (Exception :: class)
    suspend fun startFingerprinting(cryptoPassword: String, androidIndependentFingerprintCallbacks: AndroidIndependentFingerprintCallbacks) {
        if (!fingerprintHelper.checkIfSensorAvailable()) {
            throw Exception()
        } else {
            fingerprintHelper.setOnDecodeListener(cryptoPassword, androidIndependentFingerprintCallbacks)
        }
    }
}