package mako.com.lite.data.repository

import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.FileCreateListener
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.FileNameListener
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.FileUpdateListener
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.GoogleDriveFileHelperInterface
import mako.com.lite.data.helpers.filesystem.localHelper.FileHelperInterface
import mako.com.lite.data.repository.dto.GoogleRecentFileDto
import mako.com.lite.data.repository.dto.LocalRecentFileDto
import mako.com.lite.data.repository.dto.RecentFileDto
import java.io.File
import javax.inject.Inject

class FileRepository @Inject constructor(private val localFileHelper: FileHelperInterface, private val googleDriveFileHelper: GoogleDriveFileHelperInterface) {
    fun createFile(recentFileDto: RecentFileDto, folder: String?, fileCreateEvents: FileCreateEvents) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                googleDriveFileHelper.createFile(folder!!, recentFileDto.fileName, object : FileCreateListener {
                    override fun onFileCreated(stringFileDriveId: String) {
                        recentFileDto.stringDriveId = stringFileDriveId
                        localFileHelper.createFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))
                        getLocalFile(recentFileDto, object : FileUpdateListener {
                            override fun onFileUpdated(fileCache: File) {
                                fileCreateEvents.onCreated(recentFileDto, fileCache)
                            }

                            override fun onErrorHappened(exception: Exception) {
                                fileCreateEvents.onFail(exception)
                            }

                        })

                    }

                    override fun onFileCreateFailed(e: Exception) {
                        fileCreateEvents.onFail(e)
                    }
                })
            }
            is LocalRecentFileDto -> {
                localFileHelper.createFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))
                getLocalFile(recentFileDto, object : FileUpdateListener {
                    override fun onFileUpdated(fileCache: File) {
                        fileCreateEvents.onCreated(recentFileDto, fileCache)
                    }

                    override fun onErrorHappened(exception: Exception) {
                        fileCreateEvents.onFail(exception)
                    }

                })
            }
        }
    }

    fun updateRemoteFile(recentFileDto: RecentFileDto, fileCache: File) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                googleDriveFileHelper.updateDriveFile(fileCache, recentFileDto.stringDriveId)
            }
            is LocalRecentFileDto -> {
                throw RuntimeException("It is not remote file")
            }
        }
    }

    fun updateLocalFile(recentFileDto: RecentFileDto,src: ByteArray, dst: File) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                localFileHelper.writeToFile(src, dst)
            }
            else -> {
                throw RuntimeException("It is not remote file")
            }
        }
    }

    fun getTmpFile(recentFileDto: RecentFileDto, fileUpdateListener: FileUpdateListener) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                googleDriveFileHelper.updateCacheFile(getTemporaryFile(), recentFileDto.stringDriveId, fileUpdateListener)
            }
            is LocalRecentFileDto -> {
                throw RuntimeException("It is not remote file")
            }
        }
    }

    fun deleteFile(recentFileDto: RecentFileDto) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                localFileHelper.deleteFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))
            }
            is LocalRecentFileDto -> {
                localFileHelper.deleteFile(recentFileDto.fileDirectory, recentFileDto.fileName)
            }
        }
    }

    fun deleteRemoteFile(recentFileDto: RecentFileDto) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                googleDriveFileHelper.deleteDriveFile(recentFileDto.stringDriveId)
            }
            else -> {
                return
            }
        }
    }

    fun getLocalFile(recentFileDto: RecentFileDto, fileUpdateListener: FileUpdateListener) {
        val fileCache = localFileHelper.getFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))
        if (fileCache == null) {
            localFileHelper.createFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))
            fileUpdateListener.onFileUpdated(localFileHelper.getFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))!!)
        } else {
            fileUpdateListener.onFileUpdated(fileCache)
        }
    }

    fun getNewRemoteFile(recentFileDto: RecentFileDto, fileUpdateListener: FileUpdateListener) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                val fileCache = localFileHelper.createFile(getFileDirectory(recentFileDto), getFileName(recentFileDto))
                googleDriveFileHelper.updateCacheFile(fileCache, recentFileDto.stringDriveId, fileUpdateListener)
            }
            is LocalRecentFileDto -> {
                throw RuntimeException("It is not remote file")
            }
        }
    }

    fun getNewFileNameAndExtension(recentFileDto: RecentFileDto, fileNameListener: FileNameListener) {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                googleDriveFileHelper.getFileNameBy(recentFileDto.stringDriveId, fileNameListener)
            }
            is LocalRecentFileDto -> {
                val pointIndex = recentFileDto.fileName.lastIndexOf(".")
                val extension = recentFileDto.fileName.substring(pointIndex)
                fileNameListener.onFileName(recentFileDto.fileName, extension)
            }
        }
    }

    fun lazyGetFile(recentFileDto: RecentFileDto): File {
        return File(getFileDirectory(recentFileDto) + getFileName(recentFileDto))
    }

    fun getFileName(recentFileDto: RecentFileDto): String {
        return when (recentFileDto) {
            is GoogleRecentFileDto -> {
                ("${recentFileDto.stringDriveId}-${recentFileDto.fileName}")
            }
            else -> {
                recentFileDto.fileName
            }
        }
    }

    fun getFileDirectory(recentFileDto: RecentFileDto): String {
        return when (recentFileDto) {
            is GoogleRecentFileDto -> {
                localFileHelper.getGoogleCahceDirectory()
            }
            else -> {
                recentFileDto.fileDirectory
            }
        }
    }

    fun getTemporaryFile(): File {
        return localFileHelper.createFile(localFileHelper.getGoogleCahceDirectory(), "tmp.kdbx")
    }

    fun deleteTemporaryFile() {
        localFileHelper.deleteFile(localFileHelper.getGoogleCahceDirectory(), "tmp.kdbx")
    }

    fun isRemote(recentFileDto: RecentFileDto): Boolean {
        when (recentFileDto) {
            is GoogleRecentFileDto -> {
                return true
            }
            else -> {
                return false
            }
        }
    }

    interface FileCreateEvents {
        fun onCreated(recentFileDto: RecentFileDto, file: File)
        fun onFail(exception: Exception)
    }
}