package mako.com.lite.data.repository.dto

import mako.com.lite.data.database.entity.GoogleRecentFileEntity
import mako.com.lite.data.database.entity.LocalRecentFileEntity
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel


class LocalRecentFileDto(override var fileDirectory: String,
                         override var fileName: String,
                         override var filePassword: String,
                         override var fileOpenTime: Long): RecentFileDto

class GoogleRecentFileDto(override var fileDirectory: String,
                          override var fileName: String,
                          var stringDriveId: String,
                          override var filePassword: String,
                          override var fileOpenTime: Long): RecentFileDto

interface RecentFileDto {
    var fileDirectory: String
    var fileName: String
    var filePassword: String
    var fileOpenTime: Long
}

//===================================================
//                  TO Model
//===================================================

fun LocalRecentFileDto.toModel() = LocalRecentFileModel(fileDirectory, fileName, filePassword, fileOpenTime)

fun GoogleRecentFileDto.toModel() = GoogleRecentFileModel(fileDirectory, fileName, stringDriveId, filePassword, fileOpenTime)

//===================================================
//                  TO Dto
//===================================================

fun LocalRecentFileModel.toDto() = LocalRecentFileDto(fileDirectory, fileName, filePassword, fileOpenTime)
fun LocalRecentFileEntity.toDto() = LocalRecentFileDto(fileDirectory, fileName, fileCryptoPassword, fileOpenTime)

fun GoogleRecentFileModel.toDto() = GoogleRecentFileDto(fileDirectory, fileName, stringDriveId, filePassword, fileOpenTime)
fun GoogleRecentFileEntity.toDto() = GoogleRecentFileDto(fileDirectory, fileName, fileDriveId, fileCryptoPassword, fileOpenTime)

//===================================================
//                  TO Entity
//===================================================

fun LocalRecentFileDto.toEntity() = LocalRecentFileEntity(fileDirectory, fileName, filePassword, fileOpenTime)

fun GoogleRecentFileDto.toEntity() = GoogleRecentFileEntity(fileDirectory, fileName, stringDriveId, filePassword, fileOpenTime)


