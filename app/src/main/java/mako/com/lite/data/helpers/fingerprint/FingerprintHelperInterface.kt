package mako.com.lite.data.helpers.fingerprint

interface FingerprintHelperInterface {
    suspend fun setOnDecodeListener(cryptoPassword: String, androidIndependentFingerprintCallbacks: AndroidIndependentFingerprintCallbacks)
    fun checkIfSensorAvailable(): Boolean
}