package mako.com.lite.data.repository.dto

import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.GroupModel
import mako.com.lite.domain.model.LocalRecentFileModel
import java.util.*

private val starSymbol = Character.toString(9734.toChar())

class GroupDto {
    var realIndex: Int? = null
    var groupName: String? = null
    var groups: MutableList<GroupDto>? = null
    var entries: MutableList<EntryDto>? = null
}

class EntryDto {
    var realIndex: Int? = null
    var title: String? = null
    var url: String? = null
    var username: String? = null
    var password: String? = null
    var iconData: ByteArray? = null
    var customIconUUID: UUID? = null
    var notes: String? = null
}

//===================================================================================
//                               TO Model
//===================================================================================

fun GroupDto.toModel(): GroupModel{
    val groupModel = GroupModel()
    groupModel.realIndex = realIndex
    if (groupName!!.substring(0, 1) == starSymbol) {
        val label = groupName!!.substring(1, groupName!!.length)
        groupModel.label = label
        groupModel.isFavourite = true
    }else{
        groupModel.label = groupName
        groupModel.isFavourite = false
    }
    val groupsModels = LinkedList<GroupModel>()
    for(group in groups!!){
        groupsModels.add(group.toModel())
    }
    groupModel.groups = groupsModels
    groupModel.entries = entries!!.map { it.toModel() }.toMutableList()

    return groupModel
}

fun EntryDto.toModel() = EntryModel().also {
    it.realIndex = realIndex
    if (title!!.substring(0, 1) == starSymbol) {
        val label = title!!.substring(1, title!!.length)
        it.label = label
        it.isFavourite = true
    }else{
        it.label = title
        it.isFavourite = false
    }

    it.url = url
    it.username = username
    it.password = password
    it.iconData = iconData
    it.customIconUUID = customIconUUID
    it.notes = notes
}

//===================================================================================
//                               TO DTO
//===================================================================================


fun EntryModel.toDto() = EntryDto().also{
    it.realIndex = realIndex
    if (isFavourite!!) {
        it.title = starSymbol+label
    }else{
        it.title = label
    }

    it.url = url
    it.username = username
    it.password = password
    it.iconData = iconData
    it.customIconUUID = customIconUUID
    it.notes = notes
}

fun GroupModel.toDto(): GroupDto{
    val groupDto = GroupDto()
    groupDto.realIndex = realIndex
    if(isFavourite!!){
        groupDto.groupName = starSymbol+label
    }else{
        groupDto.groupName = label
    }
    val groupsDto = LinkedList<GroupDto>()
    for(group in groups!!){
        groupsDto.add(group.toDto())
    }
    groupDto.groups = groupsDto
    groupDto.entries = entries!!.map { it.toDto() }.toMutableList()

    return groupDto
}
