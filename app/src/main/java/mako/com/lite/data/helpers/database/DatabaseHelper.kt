package mako.com.lite.data.helpers.database

import android.content.Context
import mako.com.lite.data.database.RecentFileItemDatabase
import mako.com.lite.data.database.dao.GoogleRecentFileDao
import mako.com.lite.data.database.dao.LocalRecentFileDao

class DatabaseHelper (context: Context): DatabaseHelperInterface{
    override var localRecentFilesDao: LocalRecentFileDao = RecentFileItemDatabase.getInstance(context).localRecentFileDao()
    override var googleRecentFilesDao: GoogleRecentFileDao = RecentFileItemDatabase.getInstance(context).googleRecentFileDao()

}