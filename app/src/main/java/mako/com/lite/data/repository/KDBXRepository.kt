package mako.com.lite.data.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import mako.com.lite.data.helpers.crypto.kdbxCrypto.KDBXCrypto
import mako.com.lite.data.repository.dto.EntryDto
import mako.com.lite.data.repository.dto.GroupDto
import mako.com.lite.data.repository.dto.RecentFileDto
import java.io.File
import java.util.*

object KDBXRepository {
    var kdbxCrypto: KDBXCrypto = KDBXCrypto()

    lateinit var kdbxFile: File
    lateinit var localRecentFileDto: RecentFileDto
    lateinit var password: String
    /**
     * Работа с записями файла с паролями производится
     * при помощи адресации
     * Пример адресации до GroupDto
     *               /0/0/1/1/
     * Пример адресации до EntryDto
     *               /0/0/1/1/
     */

    private var rootGroupDto: GroupDto? = null


    @Throws(Exception::class)
    suspend fun openFile(file: File, passwordDb: String, recentFileDto: RecentFileDto): Deferred<GroupDto> = CoroutineScope(Dispatchers.Default).async {
        kdbxFile = file
        password = passwordDb
        localRecentFileDto = recentFileDto
        rootGroupDto = prepareFile().await()!!
        rootGroupDto!!
    }


    fun destroy() {
        rootGroupDto = null
    }

    fun getGroupDtoByPath(pathGroup: String): GroupDto? {
        if (pathGroup == "") {
            return null
        }
        if (pathGroup == "/") {
            return rootGroupDto
        }
        val arrayPath = pathGroup.split("/")


        var currentGroup = rootGroupDto
        for (i in 1 until arrayPath.size - 1) {
            currentGroup = currentGroup!!.groups!!.get(arrayPath[i].toInt())
        }
        return currentGroup!!
    }

    fun getEntryDtoByPath(pathEntry: String): EntryDto {

        val arrayPath = pathEntry.split("/")

        var currentGroup = rootGroupDto
        for (i in 1 until arrayPath.size - 2) {
            currentGroup = currentGroup!!.groups!!.get(arrayPath[i].toInt())
        }
        val returnEntryItem = currentGroup!!.entries!!.get(arrayPath.get(arrayPath.size - 2).toInt())

        return returnEntryItem
    }

    fun setGroupDtoByPath(pathGroup: String, groupItem: GroupDto): Deferred<Unit> {

        val pathGroupArray = pathGroup.split("/")

        if (getPreviousByPath(pathGroup) == "") {
            rootGroupDto = groupItem
            return kdbxCrypto.write(rootGroupDto!!, kdbxFile, password)
        } else if (getPreviousByPath(pathGroup) != "/") {
            val parentGroup = getGroupDtoByPath(getPreviousByPath(pathGroup))
            parentGroup!!.groups!![pathGroupArray[pathGroupArray.lastIndex - 1].toInt()] = groupItem
            return setGroupDtoByPath(getPreviousByPath(pathGroup), parentGroup)
        } else {
            //getPreviousByPath = "/"
            rootGroupDto!!.groups!![pathGroupArray[1].toInt()] = groupItem
            return kdbxCrypto.write(rootGroupDto!!, kdbxFile, password)
        }
    }


    suspend fun addEntry(entryItem: EntryDto, pathGroup: String): Deferred<Unit> {
        val groupItem = getGroupDtoByPath(pathGroup)
        groupItem!!.entries!!.add(entryItem)
        setGroupDtoByPath(pathGroup, groupItem).await()
        return kdbxCrypto.write(rootGroupDto!!, kdbxFile, password)
    }

    fun addGroup(newGroup: GroupDto, pathGroup: String): Deferred<Unit> {
        val groupItem = getGroupDtoByPath(pathGroup)
        groupItem!!.groups!!.add(newGroup)
        return setGroupDtoByPath(pathGroup, groupItem)
    }

    fun deleteGroup(pathGroup: String): Deferred<Unit> {
        val groupItem = getGroupDtoByPath(getPreviousByPath(pathGroup))
        val pathGroupArray = pathGroup.split("/")
        val groupIndex = pathGroupArray.get(pathGroupArray.lastIndex - 1)
        groupItem!!.groups!!.removeAt(groupIndex.toInt())
        for((i,group) in groupItem.groups!!.withIndex()){
            group.realIndex = i
        }
        return setGroupDtoByPath(getPreviousByPath(pathGroup), groupItem)
    }

    fun deleteEntry(pathEntry: String): Deferred<Unit> {
        val groupItem = getGroupDtoByPath(getPreviousByPath(pathEntry))
        val pathEntryArray = pathEntry.split("/")
        val entryIndex = pathEntryArray.get(pathEntryArray.lastIndex - 1)
        groupItem!!.entries!!.removeAt(entryIndex.toInt())
        for((i,entry) in groupItem.entries!!.withIndex()){
            entry.realIndex = i
        }
        return setGroupDtoByPath(getPreviousByPath(pathEntry), groupItem)
    }

    fun getPreviousByPath(pathGroup: String): String {
        var resultPath = pathGroup
        if (pathGroup == "/")
            return ""
        resultPath = resultPath.substring(0, resultPath.length - 1)
        while (resultPath.substring(resultPath.length - 1) != "/") {
            resultPath = resultPath.substring(0, resultPath.length - 1)
        }
        return resultPath
    }


    private fun buildStructure(): Deferred<Unit> {
        rootGroupDto = GroupDto()
        rootGroupDto!!.groupName = "root"
        rootGroupDto!!.groups = LinkedList()
        rootGroupDto!!.entries = LinkedList()
        return kdbxCrypto.write(rootGroupDto!!, kdbxFile, password)
    }

    @Throws(Exception::class)
    private suspend fun prepareFile(): Deferred<GroupDto?> {
        if (kdbxFile.length() == 0L) {
            buildStructure().await()
        }
        return kdbxCrypto.read(kdbxFile, password, localRecentFileDto)
    }


}