package mako.com.lite.data.helpers.filesystem.localHelper

import kotlinx.coroutines.Deferred
import mako.com.lite.data.repository.dto.GoogleRecentFileDto
import mako.com.lite.data.repository.dto.LocalRecentFileDto
import java.io.File

interface FileHelperInterface {
    fun createFile(fileDirectory: String, fileName: String): File

    fun deleteFile(fileDirectory: String, fileName: String): Boolean

    fun getFile(fileDirectory: String, fileName: String): File?

    fun replaceRestrictedSymbolsFromFileNameFrom(fileName: String): String

    fun getGoogleCahceDirectory(): String

    fun writeToFile(src: ByteArray, dst: File)
}

