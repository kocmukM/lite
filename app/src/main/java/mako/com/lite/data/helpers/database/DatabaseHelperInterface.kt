package mako.com.lite.data.helpers.database

import mako.com.lite.data.database.dao.GoogleRecentFileDao
import mako.com.lite.data.database.dao.LocalRecentFileDao

interface DatabaseHelperInterface {
    var localRecentFilesDao: LocalRecentFileDao
    var googleRecentFilesDao: GoogleRecentFileDao
}