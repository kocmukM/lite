package mako.com.lite.data.helpers.sharedPreferences

import android.content.SharedPreferences

interface SharedPreferencesHelperInterface {
    fun loadBoolean(key: String, defaultValue: Boolean): Boolean
    fun loadLong(key: String, defaultValue: Long): Long
    fun loadInt(key: String, defaultValue: Int): Int
    fun loadString(key: String, defaultValue: String): String?
    fun edit(block: SharedPreferences.Editor.() -> Unit)
}