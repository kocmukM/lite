package mako.com.lite.data.helpers.crypto.kdbxCrypto

import kotlinx.coroutines.Deferred
import mako.com.lite.data.repository.dto.GroupDto
import mako.com.lite.data.repository.dto.IconDto
import mako.com.lite.data.repository.dto.RecentFileDto
import java.io.File
import java.util.*

interface KDBXCryptoInterface {
    fun read(kdbxFile: File, password: String, recentFileDto: RecentFileDto): Deferred<GroupDto?>
    fun write(groupDto: GroupDto, kdbxFile: File, password: String): Deferred<Unit>
}