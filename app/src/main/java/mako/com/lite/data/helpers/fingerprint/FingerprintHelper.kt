package mako.com.lite.data.helpers.fingerprint

import android.app.KeyguardManager
import android.content.Context
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.core.os.CancellationSignal
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mako.com.lite.data.helpers.crypto.cipherCrypto.CipherCryptoInterface

class FingerprintHelper(private val context: Context,
                        private val cipherCrypto: CipherCryptoInterface) : FingerprintHelperInterface {
    enum class SensorState {
        NOT_SUPPORTED,
        NOT_BLOCKED,
        NO_FINGERPRINTS,
        READY
    }

    override suspend fun setOnDecodeListener(cryptoPassword: String,
                                             androidIndependentFingerprintCallbacks: AndroidIndependentFingerprintCallbacks) {
        val manager = FingerprintManagerCompat.from(context)
        val cryptoObject = cipherCrypto.getCryptoObject().await() ?: return
        manager.authenticate(cryptoObject, 0, CancellationSignal(), object : FingerprintManagerCompat.AuthenticationCallback() {
            override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
                super.onAuthenticationSucceeded(result)
                CoroutineScope(Dispatchers.Default).launch {
                    val cipher = result!!.cryptoObject.cipher
                    if (cipher != null) {
                        val decodedPassword = cipherCrypto.decryptData(cryptoPassword, cipher).await()
                        if (decodedPassword != null) {
                            androidIndependentFingerprintCallbacks.onAuthenticationSucceeded(decodedPassword)
                        } else {
                            androidIndependentFingerprintCallbacks.onAuthenticationFailed()
                        }
                    } else {

                    }
                }
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                androidIndependentFingerprintCallbacks.onAuthenticationFailed()
            }
        }, null)
    }

    override fun checkIfSensorAvailable(): Boolean {
        return checkSensorState() == SensorState.READY
    }

    private fun checkSensorState(): SensorState {
        if (checkFingerprintCompatibility()) {

            val keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            if (!keyguardManager.isKeyguardSecure) {
                return SensorState.NOT_BLOCKED
            }

            return if (!FingerprintManagerCompat.from(context).hasEnrolledFingerprints()) {
                SensorState.NO_FINGERPRINTS
            } else SensorState.READY

        } else {
            return SensorState.NOT_SUPPORTED
        }

    }

    private fun checkFingerprintCompatibility(): Boolean {
        return FingerprintManagerCompat.from(context).isHardwareDetected
    }
}

interface AndroidIndependentFingerprintCallbacks {
    suspend fun onAuthenticationSucceeded(decryptedPassword: String)
    fun onAuthenticationFailed()
}

