package mako.com.lite.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "google_recent_file")

data class GoogleRecentFileEntity(@ColumnInfo(name = "file_directory") var fileDirectory: String,
                                  @ColumnInfo(name = "file_name") var fileName: String,
                                  @PrimaryKey @ColumnInfo(name = "file_drive_id") var fileDriveId: String,
                                  @ColumnInfo(name = "file_crypto_password") var fileCryptoPassword: String,
                                  @ColumnInfo(name = "file_open_time") var fileOpenTime: Long)