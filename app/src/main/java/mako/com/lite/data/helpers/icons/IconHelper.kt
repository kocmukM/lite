package mako.com.lite.data.helpers.icons

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import mako.com.lite.R
import mako.com.lite.data.repository.dto.IconDto
import java.io.ByteArrayOutputStream
import java.util.*

object IconHelper: IconHelperInterface {
    override var icons: LinkedList<IconDto> =  LinkedList()

    override fun loadIcons(context: Context, iconRepositoryCallbacks: IconRepositoryCallbacks): Deferred<Unit> = CoroutineScope(Dispatchers.Default).async {
        if(icons.isEmpty()) {
            icons.add(IconDto(null, UUID(0, 0), R.drawable.standart_entry, "Standard"))
            icons.add(IconDto(null, UUID(1, 1), R.drawable.facebook, "Facebook"))
            icons.add(IconDto(null, UUID(2, 2), R.drawable.evernote, "Evernote"))
            icons.add(IconDto(null, UUID(3, 3), R.drawable.instagramm, "Instagramm"))
            icons.add(IconDto(null, UUID(4, 4), R.drawable.paypal, "Pay Pal"))
            icons.add(IconDto(null, UUID(5, 5), R.drawable.vk, "VK"))
            icons.add(IconDto(null, UUID(6, 6), R.drawable.yandex, "Yandex"))
            icons.add(IconDto(null, UUID(7, 7), R.drawable.stackoverflow, "Stackoverflow"))
            icons.add(IconDto(null, UUID(8, 8), R.drawable.wifi, "wi-fi"))
            icons.add(IconDto(null, UUID(9, 9), R.drawable.steam, "Steam"))
            icons.add(IconDto(null, UUID(10, 10), R.drawable.aliexpress, "Ali Express"))
            icons.add(IconDto(null, UUID(11, 11), R.drawable.mailru, "Mail.ru"))
            icons.add(IconDto(null, UUID(12, 12), R.drawable.win, "Windows"))
            icons.add(IconDto(null, UUID(13, 13), R.drawable.apple, "Apple"))
            icons.add(IconDto(null, UUID(14, 14), R.drawable.android, "Android"))
            icons.add(IconDto(null, UUID(15, 15), R.drawable.linux, "Linux"))
            for ((i, icon) in icons.withIndex()) {
                val stream = ByteArrayOutputStream()
                BitmapFactory.decodeResource(context.resources, icon.imageResource).compress(Bitmap.CompressFormat.PNG, 100, stream)
                val iconBytes = stream.toByteArray()
                icon.iconData = iconBytes
                iconRepositoryCallbacks.onLoadingProgress(((i.toFloat() / icons.size.toFloat()) * 100).toInt())
            }
        }

    }

    fun getIconByUUID(uuid: UUID): IconDto? {
        for (icon in icons) {
            if (icon.uuid == uuid)
                return icon
        }
        return null
    }
}

interface IconRepositoryCallbacks {
    fun onLoadingProgress(progressPercentage: Int)

}