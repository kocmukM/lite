package mako.com.lite.data.helpers.icons

import android.content.Context
import kotlinx.coroutines.Deferred
import mako.com.lite.data.repository.dto.IconDto
import java.util.*

interface IconHelperInterface {
    var icons: LinkedList<IconDto>
    fun loadIcons(context: Context, iconRepositoryCallbacks: IconRepositoryCallbacks): Deferred<Unit>
}