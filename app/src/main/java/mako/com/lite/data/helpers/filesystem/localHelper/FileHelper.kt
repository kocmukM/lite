package mako.com.lite.data.helpers.filesystem.localHelper


import android.content.Context
import java.io.File
import java.io.FileOutputStream

class FileHelper(private val context: Context) : FileHelperInterface {

    override fun replaceRestrictedSymbolsFromFileNameFrom(fileName: String): String {
        var resultFileName = fileName
        resultFileName = resultFileName.replace("\\", "")
        resultFileName = resultFileName.replace("/", "")
        resultFileName = resultFileName.replace("?", "")
        resultFileName = resultFileName.replace("!", "")
        resultFileName = resultFileName.replace("=", "")
        return resultFileName
    }

    @Throws(Exception::class)
    override fun createFile(fileDirectory: String, fileName: String): File {
        val restrictedSymbols = arrayOf("/", "\\", "?", "!", "\"")
        for (restrictedSymbol in restrictedSymbols) {
            if (fileName.contains(restrictedSymbol))
                throw Exception()
        }


        val newFile = File("$fileDirectory$fileName")

        newFile.createNewFile()
        return newFile
    }

    override fun deleteFile(fileDirectory: String, fileName: String): Boolean {
        val file = File(fileDirectory + fileName)
        return file.delete()
    }

    override fun getFile(fileDirectory: String, fileName: String): File? {
        val file = File(fileDirectory + fileName)
        return if (file.exists()) file else null

    }

    override fun getGoogleCahceDirectory(): String {
        val applicationDirectory = context.applicationInfo.dataDir
        val cacheDirectory = "$applicationDirectory/google_cache/"
        val cacheDirectoryFile = File(cacheDirectory)
        if (!cacheDirectoryFile.exists())
            cacheDirectoryFile.mkdir()
        return cacheDirectory
    }

    override fun writeToFile(src: ByteArray, dst: File) {
        val fileOS = FileOutputStream(dst)
        fileOS.write(src)
        fileOS.flush()
        fileOS.close()
    }
}