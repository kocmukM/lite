package mako.com.lite.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import mako.com.lite.data.database.entity.LocalRecentFileEntity

@Dao
interface LocalRecentFileDao {
    @Query("SELECT * FROM local_recent_file ORDER BY file_open_time DESC")
    fun getRecentFiles(): List<LocalRecentFileEntity>

    @Insert(onConflict = REPLACE)
    fun insertRecentFile(localRecentFileEntityDto: LocalRecentFileEntity)

    @Query("DELETE FROM local_recent_file WHERE file_directory = :fileDirectory AND file_name = :fileName")
    fun deleteRecentFile(fileDirectory: String, fileName: String)
}