package mako.com.lite.data.helpers.crypto.cipherCrypto


import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import kotlinx.coroutines.Deferred
import javax.crypto.Cipher

interface CipherCryptoInterface {
    fun encryptData(password: String): Deferred<String?>
    fun decryptData(base64: String, cipherDecrypter: Cipher): Deferred<String?>
    fun getCryptoObject(): Deferred<FingerprintManagerCompat.CryptoObject?>
}