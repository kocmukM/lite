package mako.com.lite.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import mako.com.lite.LiteApplication.Companion.DATABASE_NAME
import mako.com.lite.LiteApplication.Companion.DATABASE_VERSION
import mako.com.lite.data.database.dao.GoogleRecentFileDao
import mako.com.lite.data.database.dao.LocalRecentFileDao
import mako.com.lite.data.database.entity.GoogleRecentFileEntity
import mako.com.lite.data.database.entity.LocalRecentFileEntity


@Database(entities = [LocalRecentFileEntity::class, GoogleRecentFileEntity::class], version = DATABASE_VERSION)
abstract class RecentFileItemDatabase : RoomDatabase() {

    abstract fun localRecentFileDao(): LocalRecentFileDao
    abstract fun googleRecentFileDao(): GoogleRecentFileDao

    companion object {

        @Volatile
        private var INSTANCE: RecentFileItemDatabase? = null

        fun getInstance(context: Context): RecentFileItemDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        RecentFileItemDatabase::class.java, DATABASE_NAME)
                        .build()
    }


}