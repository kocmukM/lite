package mako.com.lite.data.repository

import kotlinx.coroutines.*
import mako.com.lite.data.database.dao.GoogleRecentFileDao
import mako.com.lite.data.database.dao.LocalRecentFileDao
import mako.com.lite.data.helpers.crypto.cipherCrypto.CipherCryptoInterface
import mako.com.lite.data.helpers.database.DatabaseHelperInterface
import mako.com.lite.data.repository.dto.*
import javax.inject.Inject


class DatabaseRepository @Inject constructor(private val databaseHelper: DatabaseHelperInterface,
                                             private val cipherCrypto: CipherCryptoInterface) {
    private lateinit var localRecentFilesDao: LocalRecentFileDao
    private lateinit var googleRecentFilesDao: GoogleRecentFileDao


    fun selectLocalRecentFiles(): Deferred<MutableList<LocalRecentFileDto>> = CoroutineScope(Dispatchers.Default).async {
        localRecentFilesDao = databaseHelper.localRecentFilesDao
        localRecentFilesDao.getRecentFiles().map { it.toDto() }.toMutableList()
    }

    fun selectGoogleRecentFiles(): Deferred<MutableList<GoogleRecentFileDto>> = CoroutineScope(Dispatchers.Default).async {
        googleRecentFilesDao = databaseHelper.googleRecentFilesDao
        googleRecentFilesDao.getRecentFiles().map { it.toDto() }.toMutableList()
    }

    suspend fun registerLocalFileOpenEvent(fileDirectory: String, fileName: String, password: String) {
        var cryptoPassword = cipherCrypto.encryptData(password).await()
        if (cryptoPassword == null)
            cryptoPassword = ""
        CoroutineScope(Dispatchers.Default).launch {
            localRecentFilesDao = databaseHelper.localRecentFilesDao
            localRecentFilesDao.insertRecentFile(LocalRecentFileDto(fileDirectory, fileName, cryptoPassword, System.currentTimeMillis()).toEntity())
        }
    }

    suspend fun registerGoogleFileOpenEvent(fileDirectory: String, fileName: String, stringDriveId: String, password: String) {
        var cryptoPassword = cipherCrypto.encryptData(password).await()
        if (cryptoPassword == null)
            cryptoPassword = ""
        CoroutineScope(Dispatchers.Default).launch {
            googleRecentFilesDao = databaseHelper.googleRecentFilesDao
            googleRecentFilesDao.insertRecentFile(GoogleRecentFileDto(fileDirectory, fileName, stringDriveId, cryptoPassword, System.currentTimeMillis()).toEntity())
        }
    }

    suspend fun deleteRecentFileModel(recentFileDto: RecentFileDto) {
        when (recentFileDto) {
            is LocalRecentFileDto -> {
                deleteLocalRecentFile(recentFileDto).await()
            }
            is GoogleRecentFileDto -> {
                deleteGoogleRecentFile(recentFileDto).await()
            }
        }
    }

    private fun deleteLocalRecentFile(localRecentFileDto: LocalRecentFileDto) = CoroutineScope(Dispatchers.Default).async {
        localRecentFilesDao = databaseHelper.localRecentFilesDao
        localRecentFilesDao.deleteRecentFile(localRecentFileDto.fileDirectory, localRecentFileDto.fileName)

    }

    private fun deleteGoogleRecentFile(googleRecentFileDto: GoogleRecentFileDto) = CoroutineScope(Dispatchers.Default).async {
        googleRecentFilesDao = databaseHelper.googleRecentFilesDao
        googleRecentFilesDao.deleteRecentFile(googleRecentFileDto.stringDriveId)

    }
}