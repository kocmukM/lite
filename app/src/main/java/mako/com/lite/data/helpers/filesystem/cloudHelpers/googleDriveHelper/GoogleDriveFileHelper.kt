package mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.drive.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mako.com.lite.data.repository.dto.GoogleRecentFileDto
import okio.Okio
import java.io.File
import java.io.FileInputStream
import java.io.IOException

class GoogleDriveFileHelper(private val context: Context) : GoogleDriveFileHelperInterface {

    private lateinit var driveResourceClient: DriveResourceClient
    private var signInAccount: GoogleSignInAccount? = null

    override fun updateCacheFile(fileCache: File, stringDriveId: String, fileUpdateListener: FileUpdateListener) {
        initResourceClient()

        val drive = DriveId.decodeFromString(stringDriveId).asDriveFile()
        val openFileTask = driveResourceClient.openFile(drive, DriveFile.MODE_READ_ONLY)
        openFileTask?.continueWithTask { task ->
            val contents = task.result
            contents!!.inputStream.use {
                try {
                    val sink = Okio.buffer(Okio.sink(fileCache))
                    sink.writeAll(Okio.source(it))
                    sink.close()
                    fileUpdateListener.onFileUpdated(fileCache)
                } catch (e: IOException) {
                    fileUpdateListener.onErrorHappened(e)
                }
            }
            driveResourceClient.discardContents(contents)
        }?.addOnFailureListener { e ->
            fileUpdateListener.onErrorHappened(e)
        }
    }

    override fun updateDriveFile(fileCache: File, stringDriveId: String) {
        initResourceClient()

        CoroutineScope(Dispatchers.Default).launch {
            val drive = DriveId.decodeFromString(stringDriveId).asDriveFile()
            val openFileTask = driveResourceClient.openFile(drive, DriveFile.MODE_WRITE_ONLY)
            openFileTask?.continueWithTask { task ->
                val contents = task.result
                contents!!.outputStream.use {
                    val fileBytes = ByteArray(fileCache.length().toInt())
                    val fileIS = FileInputStream(fileCache)
                    fileIS.read(fileBytes)
                    fileIS.close()
                    it.write(fileBytes)
                    it.flush()
                    it.close()
                }
                driveResourceClient.commitContents(contents, null)
            }
        }
    }

    override fun createFile(stringDriveFolderId: String, fileCache: File, fileName: String, fileCreateListener: FileCreateListener) {
        initResourceClient()

        val drive = DriveId.decodeFromString(stringDriveFolderId).asDriveFolder()
        val metadataBuilder = MetadataChangeSet.Builder()
                .setTitle(fileName)
        driveResourceClient.createContents()?.continueWithTask { task ->
            val contents = task.result
            contents!!.outputStream.use {
                val fileBytes = ByteArray(fileCache.length().toInt())
                val fileIS = FileInputStream(fileCache)
                fileIS.read(fileBytes)
                fileIS.close()
                it.write(fileBytes)
                it.flush()
                it.close()
            }
            driveResourceClient.createFile(drive, metadataBuilder.build(), contents)
        }?.addOnSuccessListener {
            val stringDriveId = it.driveId.toString()
            fileCreateListener.onFileCreated(stringDriveId)
        }?.addOnFailureListener {
            fileCreateListener.onFileCreateFailed(it)
        }
    }

    override fun deleteDriveFile(stringDriveId: String) {
        initResourceClient()

        val drive = DriveId.decodeFromString(stringDriveId).asDriveFile()
        driveResourceClient.delete(drive)
    }

    override fun createFile(stringDriveFolderId: String, fileName: String, fileCreateListener: FileCreateListener) {
        initResourceClient()

        val drive = DriveId.decodeFromString(stringDriveFolderId).asDriveFolder()
        val metadataBuilder = MetadataChangeSet.Builder()
                .setTitle(fileName)
        driveResourceClient.createFile(drive, metadataBuilder.build(), null)
                ?.addOnSuccessListener {
                    val stringDriveId = it.driveId.toString()
                    fileCreateListener.onFileCreated(stringDriveId)
                }?.addOnFailureListener {
                    fileCreateListener.onFileCreateFailed(it)
                }
    }

    override fun getFileNameBy(stringDriveId: String, fileNameListener: FileNameListener) {
        initResourceClient()
        val drive = DriveId.decodeFromString(stringDriveId).asDriveFile()
        driveResourceClient.getMetadata(drive)?.addOnSuccessListener {
            fileNameListener.onFileName(it.originalFilename, it.fileExtension)
        }
    }


    override fun getFolderNameBy(stringDriveId: String, folderNameListener: FolderNameListener) {
        initResourceClient()
        val drive = DriveId.decodeFromString(stringDriveId).asDriveFolder()
        driveResourceClient.getMetadata(drive)?.addOnSuccessListener {
            folderNameListener.onFolderName(it.originalFilename)
        }
    }

    @Throws(Exception::class)
    private fun initResourceClient() {
        signInAccount = GoogleSignIn.getLastSignedInAccount(context)
        if (signInAccount == null)
            throw RuntimeException("User not signed in")
        driveResourceClient = Drive.getDriveResourceClient(context, signInAccount!!)
    }
}

interface FileNameListener {
    fun onFileName(fileName: String, fileExtension: String)
}

interface FolderNameListener {
    fun onFolderName(folderName: String)
}

interface FileUpdateListener {
    fun onFileUpdated(fileCache: File)
    fun onErrorHappened(exception: Exception)
}

interface FileCreateListener {
    fun onFileCreated(stringFileDriveId: String)
    fun onFileCreateFailed(e: Exception)
}