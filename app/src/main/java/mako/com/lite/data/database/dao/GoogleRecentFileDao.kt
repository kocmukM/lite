package mako.com.lite.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mako.com.lite.data.database.entity.GoogleRecentFileEntity

@Dao
interface GoogleRecentFileDao {
    @Query("SELECT * FROM google_recent_file ORDER BY file_open_time DESC")
    fun getRecentFiles(): List<GoogleRecentFileEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecentFile(localRecentFileEntityDto: GoogleRecentFileEntity)

    @Query("DELETE FROM google_recent_file WHERE file_drive_id = :stringDriveId")
    fun deleteRecentFile(stringDriveId: String)
}