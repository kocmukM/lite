package mako.com.lite.data.repository.dto

import java.util.*

class IconDto(var iconData: ByteArray?, var uuid: UUID, var imageResource: Int, var description: String)

