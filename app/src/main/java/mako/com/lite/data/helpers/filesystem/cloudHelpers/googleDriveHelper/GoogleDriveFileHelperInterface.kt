package mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper

import mako.com.lite.data.repository.dto.GoogleRecentFileDto
import java.io.File

interface GoogleDriveFileHelperInterface {
    fun updateCacheFile(fileCache: File, stringDriveId: String, fileUpdateListener: FileUpdateListener)
    fun updateDriveFile(fileCache: File, stringDriveId: String)
    fun deleteDriveFile(stringDriveId: String)
    fun createFile(stringDriveFolderId: String, fileCache: File, fileName: String, fileCreateListener: FileCreateListener)
    fun createFile(stringDriveFolderId: String, fileName: String, fileCreateListener: FileCreateListener)
    fun getFileNameBy(stringDriveId: String, fileNameListener: FileNameListener)
    fun getFolderNameBy(stringDriveId: String, folderNameListener: FolderNameListener)
}