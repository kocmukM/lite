package mako.com.lite.data.helpers.crypto.cipherCrypto

import android.annotation.TargetApi
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import mako.com.lite.LiteApplication.Companion.KEY_ALIAS
import mako.com.lite.LiteApplication.Companion.KEY_STORE
import mako.com.lite.LiteApplication.Companion.TRANSFORMATION
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import java.security.spec.MGF1ParameterSpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource

class CipherCrypto: CipherCryptoInterface {
    private var sKeyStore: KeyStore? = null
    private var sKeyPairGenerator: KeyPairGenerator? = null
    private var sCipher: Cipher? = null

    override fun encryptData(password: String): Deferred<String?> = CoroutineScope(Dispatchers.Default).async {
        var result: String? = null
        try {
            if (prepare() && initCipher(Cipher.ENCRYPT_MODE)) {
                val bytes = sCipher!!.doFinal(password.toByteArray())
                result = Base64.encodeToString(bytes, Base64.NO_WRAP)
            }
        } catch (exception: IllegalBlockSizeException) {
            exception.printStackTrace()
        } catch (exception: BadPaddingException) {
            exception.printStackTrace()
        }

        result
    }

    override fun decryptData(base64: String, cipherDecrypter: Cipher): Deferred<String?> = CoroutineScope(Dispatchers.Default).async {
        var result: String? = null
        try {
            val bytes = Base64.decode(base64, Base64.NO_WRAP)
            result = String(cipherDecrypter.doFinal(bytes))
        } catch (exception: IllegalBlockSizeException) {
            exception.printStackTrace()
        } catch (exception: BadPaddingException) {
            exception.printStackTrace()
        }
        result
    }

    override fun getCryptoObject(): Deferred<FingerprintManagerCompat.CryptoObject?> = CoroutineScope(Dispatchers.Default).async {
        var cryptoObject: FingerprintManagerCompat.CryptoObject? = null
        if (prepare() && initCipher(Cipher.DECRYPT_MODE)) {
            cryptoObject = FingerprintManagerCompat.CryptoObject(sCipher!!)
        }
        cryptoObject
    }


    private fun prepare(): Boolean {
        return getKeyStore() && getCipher() && getKey()
    }


    private fun getKeyStore(): Boolean {
        try {
            sKeyStore = KeyStore.getInstance(KEY_STORE)
            sKeyStore!!.load(null)
            return true
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: CertificateException) {
            e.printStackTrace()
        }

        return false
    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun getKeyPairGenerator(): Boolean {
        try {
            sKeyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, KEY_STORE)
            return true
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }

        return false
    }

    private fun getCipher(): Boolean {
        try {
            sCipher = Cipher.getInstance(TRANSFORMATION)
            return true
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        }

        return false
    }

    private fun getKey(): Boolean {
        try {
            return sKeyStore!!.containsAlias(KEY_ALIAS) || generateNewKey()
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }

        return false

    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun generateNewKey(): Boolean {

        if (getKeyPairGenerator()) {

            try {
                sKeyPairGenerator!!.initialize(
                        KeyGenParameterSpec.Builder(KEY_ALIAS, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                                .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                                .setUserAuthenticationRequired(true)
                                .build())
                sKeyPairGenerator!!.generateKeyPair()
                return true
            } catch (e: InvalidAlgorithmParameterException) {
                e.printStackTrace()
            } catch (e: NoClassDefFoundError) {
                //Android
                e.printStackTrace()
            }catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return false
    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun initCipher(mode: Int): Boolean {
        try {
            sKeyStore!!.load(null)

            when (mode) {
                Cipher.ENCRYPT_MODE -> initEncodeCipher(mode)

                Cipher.DECRYPT_MODE -> initDecodeCipher(mode)
                else -> return false //this cipher is only for encode\decode
            }
            return true

        } catch (exception: KeyPermanentlyInvalidatedException) {
            deleteInvalidKey()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

    private fun initDecodeCipher(mode: Int) {
        val key = sKeyStore!!.getKey(KEY_ALIAS, null) as PrivateKey
        sCipher!!.init(mode, key)
    }

    private fun initEncodeCipher(mode: Int) {
        val key = sKeyStore!!.getCertificate(KEY_ALIAS).publicKey
        val unrestricted = KeyFactory.getInstance(key.algorithm).generatePublic(X509EncodedKeySpec(key.encoded))
        val spec = OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT)
        sCipher!!.init(mode, unrestricted, spec)
    }

    private fun deleteInvalidKey() {
        if (getKeyStore()) {
            try {
                sKeyStore!!.deleteEntry(KEY_ALIAS)
            } catch (e: KeyStoreException) {
                e.printStackTrace()
            }

        }
    }
}