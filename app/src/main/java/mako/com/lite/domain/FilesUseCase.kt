package mako.com.lite.domain

import kotlinx.coroutines.*
import mako.com.lite.LiteApplication
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.FileNameListener
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.FileUpdateListener
import mako.com.lite.data.repository.FileRepository
import mako.com.lite.data.repository.dto.*
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel
import mako.com.lite.domain.model.RecentFileModel
import java.io.File
import java.io.FileInputStream
import javax.inject.Inject

class FilesUseCase @Inject constructor(private val fileRepository: FileRepository) {

    fun getFile(recentFileModel: RecentFileModel): File {
        return fileRepository.lazyGetFile(convert(recentFileModel))
    }

    //==============================================
    // Безопасная выборка файла
    //==============================================

    fun getFile(syncEnabled: Boolean, recentFileModel: RecentFileModel, getFileEvents: GetFileEvents, openChooseListener: OpenChooseListener) {
        if (fileRepository.isRemote(convert(recentFileModel)) && syncEnabled) {
            startRemoteGetStrategy(recentFileModel, getFileEvents, openChooseListener)
        } else {
            fileRepository.getLocalFile(convert(recentFileModel), object : FileUpdateListener {
                override fun onFileUpdated(fileCache: File) {
                    getFileEvents.onFile(fileCache)
                }

                override fun onErrorHappened(exception: Exception) {
                    getFileEvents.onFile(getFile(recentFileModel))
                    getFileEvents.onError(exception)
                }
            })

        }
    }

    private fun startRemoteGetStrategy(recentFileModel: RecentFileModel, getFileEvents: GetFileEvents, openChooseListener: OpenChooseListener) {
        fileRepository.getTmpFile(convert(recentFileModel), object : FileUpdateListener {
            override fun onFileUpdated(fileCache: File) {
                startCompare(recentFileModel, getFileEvents, openChooseListener, fileCache)
            }

            override fun onErrorHappened(exception: Exception) {
                getFileEvents.onFile(getFile(recentFileModel))
                getFileEvents.onError(exception)
            }

        })
    }

    private fun startCompare(recentFileModel: RecentFileModel, getFileEvents: GetFileEvents, openChooseListener: OpenChooseListener, tmpCacheFile: File) {
        fileRepository.getLocalFile(convert(recentFileModel), object : FileUpdateListener {
            override fun onFileUpdated(fileCache: File) {
                doCompare(fileCache, tmpCacheFile, openChooseListener, getFileEvents)
            }

            override fun onErrorHappened(exception: Exception) {
                getFileEvents.onError(exception)
            }
        })
    }

    private fun doCompare(cacheFile: File, tmpCacheFile: File, openChooseListener: OpenChooseListener, getFileEvents: GetFileEvents) {
        CoroutineScope(Dispatchers.Default).launch {
            //Сравниваем скачанные файл с сохраненным
            if (!compareFiles(getByteArrayFromFile(cacheFile).await(), getByteArrayFromFile(tmpCacheFile).await()).await()) {
                openChooseListener.onRequestOpenChooser(tmpCacheFile, cacheFile)
            } else {
                continueWithoutChanges(cacheFile, getFileEvents)
            }
        }
    }

    fun continueWithoutChanges(cacheFile: File, getFileEvents: GetFileEvents) {
        //Удаляем временный файл
        fileRepository.deleteTemporaryFile()
        getFileEvents.onFile(cacheFile)
    }

    fun updateRemoteFile(recentFileModel: RecentFileModel) {
        fileRepository.getLocalFile(convert(recentFileModel), object : FileUpdateListener {
            override fun onFileUpdated(fileCache: File) {
                if (fileRepository.isRemote(convert(recentFileModel))) {
                    fileRepository.updateRemoteFile(convert(recentFileModel), fileCache)
                }
            }

            override fun onErrorHappened(exception: Exception) {
                throw LiteApplication.Exceptions.UpdateRemoteFileException()
            }

        })
    }

    fun updateLocalFile(recentFileModel: RecentFileModel, getFileEvents: GetFileEvents, tmpCacheFile: File) {
        fileRepository.getLocalFile(convert(recentFileModel), object : FileUpdateListener {
            override fun onFileUpdated(fileCache: File) {
                if (fileRepository.isRemote(convert(recentFileModel))) {
                    CoroutineScope(Dispatchers.Default).launch {
                        fileRepository.updateLocalFile(convert(recentFileModel), getByteArrayFromFile(tmpCacheFile).await(), fileCache)
                        getFileEvents.onFile(fileCache)
                    }
                } else {
                    getFileEvents.onFile(fileCache)
                }
            }

            override fun onErrorHappened(exception: Exception) {
                getFileEvents.onError(exception)
            }

        })
    }

    private fun compareFiles(byteFile1: ByteArray, byteFile2: ByteArray): Deferred<Boolean> = CoroutineScope(Dispatchers.Default).async {
        if (byteFile1.size != byteFile2.size) {
            return@async false
        }
        for (i in 0 until (byteFile1.size) - 1) {
            if (byteFile1[i] != byteFile2[i]) {
                return@async false
            }
        }
        true
    }


    private fun getByteArrayFromFile(file: File): Deferred<ByteArray> = CoroutineScope(Dispatchers.Default).async {
        val fileBytes = ByteArray(file.length().toInt())
        val fileIS = FileInputStream(file)
        fileIS.read(fileBytes)
        fileIS.close()
        fileBytes
    }


    //==============================================
    // Открытие нового файла
    //==============================================


    fun openNew(recentFileModel: RecentFileModel, openNewListener: OpenNewListener) {
        fileRepository.getNewFileNameAndExtension(convert(recentFileModel), object : FileNameListener {
            override fun onFileName(fileName: String, fileExtension: String) {
                if (fileExtension != ".kdbx" && fileExtension != "kdbx") {
                    openNewListener.onError(LiteApplication.Exceptions.WrongExtensionException(fileExtension))
                } else {
                    if (fileRepository.isRemote(convert(recentFileModel))) {
                        fileRepository.getNewRemoteFile(convert(recentFileModel), object : FileUpdateListener {
                            override fun onFileUpdated(fileCache: File) {
                                recentFileModel.fileName = fileName
                                recentFileModel.fileDirectory = fileRepository.getFileDirectory(convert(recentFileModel))
                                openNewListener.onOpen(recentFileModel, fileCache)
                            }

                            override fun onErrorHappened(exception: Exception) {
                                openNewListener.onError(exception)
                            }

                        })
                    } else {
                        fileRepository.getLocalFile(convert(recentFileModel), object : FileUpdateListener {
                            override fun onFileUpdated(fileCache: File) {
                                openNewListener.onOpen(recentFileModel, fileCache)
                            }

                            override fun onErrorHappened(exception: Exception) {
                                openNewListener.onError(exception)
                            }

                        })
                    }
                }
            }

        })
    }

    //==============================================
    // Создание нового файла
    //==============================================

    fun createFile(recentFileModel: RecentFileModel, createFileEvents: CreateFileEvents, folder: String?) {
        fileRepository.createFile(convert(recentFileModel), folder, object : FileRepository.FileCreateEvents {
            override fun onCreated(recentFileDto: RecentFileDto, file: File) {
                createFileEvents.onCreated(convert(recentFileDto), file)
            }

            override fun onFail(exception: Exception) {
                createFileEvents.onError(exception)
            }

        })
    }

    //==============================================
    // Запись в файл
    //==============================================

    fun writeToFile(recentFileModel: RecentFileModel) {
        if (fileRepository.isRemote(convert(recentFileModel))) {
            fileRepository.getLocalFile(convert(recentFileModel), object : FileUpdateListener {
                override fun onFileUpdated(fileCache: File) {
                    fileRepository.updateRemoteFile(convert(recentFileModel), fileCache)
                }

                override fun onErrorHappened(exception: Exception) {}

            })

        }
    }

    //==============================================
    // Удаление файла
    //==============================================

    fun deleteFile(recentFileModel: RecentFileModel) {
        fileRepository.deleteFile(convert(recentFileModel))
        fileRepository.deleteRemoteFile(convert(recentFileModel))
    }

    fun deleteCachedFile(recentFileModel: RecentFileModel) {
        if (fileRepository.isRemote(convert(recentFileModel)))
            fileRepository.deleteFile(convert(recentFileModel))
    }


    private fun convert(recentFileModel: RecentFileModel): RecentFileDto {
        return when (recentFileModel) {
            is GoogleRecentFileModel -> {
                recentFileModel.toDto()
            }
            is LocalRecentFileModel -> {
                recentFileModel.toDto()
            }
            else -> {
                throw java.lang.RuntimeException("Something went wrong")
            }
        }
    }

    private fun convert(recentFileDto: RecentFileDto): RecentFileModel {
        return when (recentFileDto) {
            is GoogleRecentFileDto -> {
                recentFileDto.toModel()
            }
            is LocalRecentFileDto -> {
                recentFileDto.toModel()
            }
            else -> {
                throw java.lang.RuntimeException("Something went wrong")
            }
        }
    }

    interface OpenChooseListener {
        fun onRequestOpenChooser(tmpCacheFile: File, cacheFile: File)
    }

    interface GetFileEvents {
        fun onFile(file: File)
        fun onError(e: Exception)
    }

    interface CreateFileEvents {
        fun onCreated(recentFileModel: RecentFileModel, kdbxFile: File)
        fun onError(exception: Exception)
    }

    interface OpenNewListener {
        fun onOpen(recentFileModel: RecentFileModel, fileCache: File)
        fun onError(exception: Exception)
    }
}