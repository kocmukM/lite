package mako.com.lite.domain

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import mako.com.lite.LiteApplication
import mako.com.lite.data.repository.DatabaseRepository
import mako.com.lite.data.repository.KDBXRepository
import mako.com.lite.data.repository.dto.GroupDto
import mako.com.lite.data.repository.dto.toDto
import mako.com.lite.data.repository.dto.toModel
import mako.com.lite.domain.model.*
import java.io.File
import java.util.*
import javax.inject.Inject


/**
 * Свойства расставлены по убыванию
 * значимости
 */
enum class PreviewProperties {
    DEFAULT,
    FAVOURITE_AT_FIRST,
    SORT_BY_LABEL;

}


class PreviewListUseCase @Inject constructor(private val databaseRepository: DatabaseRepository) {
    lateinit var currentGroupModelPath: String

    companion object {
        lateinit var properties: Array<PreviewProperties>
    }


    @Throws(Exception::class)
    suspend fun openFile(recentFileModel: RecentFileModel, kdbxFile: File, password: String, vararg previewProperties: PreviewProperties): PreviewList {
        properties = arrayOf(*previewProperties)
        currentGroupModelPath = "/"
        var openGroupDtoResult = GroupDto()
        when (recentFileModel) {
            is GoogleRecentFileModel -> {
                openGroupDtoResult = KDBXRepository.openFile(kdbxFile, password, recentFileModel.toDto()).await()
                databaseRepository.registerGoogleFileOpenEvent(recentFileModel.fileDirectory, recentFileModel.fileName, recentFileModel.stringDriveId, password)
            }
            is LocalRecentFileModel -> {
                openGroupDtoResult = KDBXRepository.openFile(kdbxFile, password, recentFileModel.toDto()).await()
                databaseRepository.registerLocalFileOpenEvent(recentFileModel.fileDirectory, recentFileModel.fileName, password)
            }
        }
        return setPreviewOrder(openGroupDtoResult.toModel())
    }

    fun openChild(childPreviewModel: PreviewModel, callbacks: PreviewListEvents) {
        val openParentPath = if (childPreviewModel.parentPath == null) {
            currentGroupModelPath
        } else {
            childPreviewModel.parentPath
        }
        if (childPreviewModel.instance == EntryModel::class.java) {
            callbacks.onEntryOpened(openChildEntryAt(childPreviewModel.realIndex, openParentPath!!))
        } else {
            callbacks.onGroupOpened(setPreviewOrder(openChildGroupAt(childPreviewModel.realIndex, openParentPath!!)))
        }
    }

    fun openGroupAt(path: String, callbacks: PreviewListEvents) {
        if (currentGroupModelPath == path)
            return
        currentGroupModelPath = path
        callbacks.onGroupOpened(setPreviewOrder(KDBXRepository.getGroupDtoByPath(path)!!.toModel()))
    }

    fun goBack(callbacks: BackStackEvents) {
        val parentPath = KDBXRepository.getPreviousByPath(currentGroupModelPath)
        if (parentPath == "") {
            callbacks.onSignOut()
            return
        }
        currentGroupModelPath = parentPath
        callbacks.onParentGroupOpened(setPreviewOrder(KDBXRepository.getGroupDtoByPath(parentPath)!!.toModel()))
    }

    suspend fun addEntry(entryModel: EntryModel): PreviewList {
        KDBXRepository.addEntry(entryModel.toDto(), currentGroupModelPath).await()
        return setPreviewOrder(KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.toModel())
    }

    @Throws(Exception::class)
    suspend fun addGroup(groupModel: GroupModel): PreviewList {
        if (groupModel.label == "") throw LiteApplication.Exceptions.GroupWithoutLabelException()
        groupModel.entries = LinkedList()
        groupModel.groups = LinkedList()
        groupModel.realIndex = KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.groups!!.size
        KDBXRepository.addGroup(groupModel.toDto(), currentGroupModelPath).await()
        return setPreviewOrder(KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.toModel())
    }

    suspend fun removeChild(childPreviewModel: PreviewModel): PreviewList {
        val removeParentPath = if (childPreviewModel.parentPath == null) {
            currentGroupModelPath
        } else {
            childPreviewModel.parentPath
        }
        if (childPreviewModel.instance == EntryModel::class.java) {
            return setPreviewOrder(removeEntryAt(childPreviewModel.realIndex, removeParentPath!!))
        } else {
            return setPreviewOrder(removeGroupAt(childPreviewModel.realIndex, removeParentPath!!))
        }

    }


    suspend fun switchFavourite(childPreviewModel: PreviewModel): PreviewList {
        val switchFavouriteParentPath = if (childPreviewModel.parentPath == null) {
            currentGroupModelPath
        } else {
            childPreviewModel.parentPath
        }
        val parentGroupModel = KDBXRepository.getGroupDtoByPath(switchFavouriteParentPath!!)!!.toModel()

        if (childPreviewModel.instance == EntryModel::class.java) {
            parentGroupModel.entries!![childPreviewModel.realIndex].isFavourite = !parentGroupModel.entries!![childPreviewModel.realIndex].isFavourite!!
        } else {
            parentGroupModel.groups!![childPreviewModel.realIndex].isFavourite = !parentGroupModel.groups!![childPreviewModel.realIndex].isFavourite!!
        }
        KDBXRepository.setGroupDtoByPath(switchFavouriteParentPath, parentGroupModel.toDto()).await()
        return setPreviewOrder(KDBXRepository.getGroupDtoByPath(switchFavouriteParentPath)!!.toModel())
    }

    suspend fun changeParentGroupName(newGroupName: String): PreviewList {
        val parentGroupModel = KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.toModel()
        parentGroupModel.label = newGroupName
        KDBXRepository.setGroupDtoByPath(currentGroupModelPath, parentGroupModel.toDto()).await()
        return setPreviewOrder(KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.toModel())
    }

    fun setPreviewOrder(previewList: PreviewList, vararg previewProperties: PreviewProperties): PreviewList {
        properties = arrayOf(*previewProperties)
        return setPreviewOrder(KDBXRepository.getGroupDtoByPath(previewList.currentGroupModelPath)!!.toModel())
    }


    fun setPreviewOrder(groupModel: GroupModel): PreviewList {
        val favouritePreviewModelList = LinkedList<PreviewModel>()
        val otherPreviewModelList = LinkedList<PreviewModel>()
        for (tmpGroupModel in groupModel.groups!!) {
            otherPreviewModelList.add(buildPreviewModelFromGroupModel(tmpGroupModel))
        }
        for (tmpEntryModel in groupModel.entries!!) {
            otherPreviewModelList.add(buildPreviewModelFromEntryModel(tmpEntryModel))
        }
        var favouriteCount = 0
        if (properties.contains(PreviewProperties.FAVOURITE_AT_FIRST)) {
            var i = 0
            while (otherPreviewModelList.size > i) {
                if (otherPreviewModelList[i].isFavourite) {
                    favouritePreviewModelList.add(otherPreviewModelList[i])
                    otherPreviewModelList.removeAt(i)
                    favouriteCount++
                }
                i++
            }
        }

        if (properties.contains(PreviewProperties.SORT_BY_LABEL)) {
            favouritePreviewModelList.sort()
            otherPreviewModelList.sort()
        }
        val previewInterfaceList: MutableList<PreviewInterface> = favouritePreviewModelList.plus(otherPreviewModelList).toMutableList()
        val isEditable = KDBXRepository.getPreviousByPath(currentGroupModelPath) != ""
        previewInterfaceList.add(0, PrevieHeader(groupModel.label!!, isEditable))
        return PreviewList(currentGroupModelPath, previewInterfaceList)
    }

    fun fastSearchBy(keyWord: String): Deferred<PreviewList> = CoroutineScope(Dispatchers.Default).async {
        val searchResult = searchIn(keyWord, KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.toModel(), currentGroupModelPath)
        PreviewList("search", searchResult.toMutableList())
    }

    fun globalSearchBy(keyWord: String): Deferred<PreviewList> = CoroutineScope(Dispatchers.Default).async {
        val searchResult = globalSearch(keyWord, "/", LinkedList())
        PreviewList("search", searchResult.toMutableList())
    }

    fun getBreadcrumbs(): LinkedList<BreadcrumbModel> {
        val breadCrumbList = LinkedList<BreadcrumbModel>()
        var tmpGroupModelPath = currentGroupModelPath
        while (tmpGroupModelPath != "") {
            breadCrumbList.add(BreadcrumbModel(tmpGroupModelPath, KDBXRepository.getGroupDtoByPath(tmpGroupModelPath)!!.toModel().label!!))
            tmpGroupModelPath = KDBXRepository.getPreviousByPath(tmpGroupModelPath)
        }
        breadCrumbList.reverse()
        return breadCrumbList
    }

    //========================================================================
    //                          Private functions
    //========================================================================


    private fun openChildGroupAt(childGroupRealIndex: Int, parentPath: String): GroupModel {
        currentGroupModelPath = "$parentPath$childGroupRealIndex/"
        return KDBXRepository.getGroupDtoByPath(currentGroupModelPath)!!.toModel()
    }

    private fun globalSearch(keyWord: String, searchPath: String, listResult: MutableList<PreviewInterface>): MutableList<PreviewInterface> {
        var tmpListResult = listResult
        val proceedingGroupModel: GroupModel = KDBXRepository.getGroupDtoByPath(searchPath)!!.toModel()
        for (i in 0 until proceedingGroupModel.groups!!.size) {
            tmpListResult = concatLinkedList(tmpListResult, globalSearch(keyWord, "$searchPath$i/", tmpListResult))
        }
        return concatLinkedList(tmpListResult, searchIn(keyWord, proceedingGroupModel, searchPath))
    }

    private fun searchIn(keyWord: String, proceedingGroupModel: GroupModel, thisModelPath: String): MutableList<PreviewInterface> {
        val localResult = LinkedList<PreviewInterface>()

        for (entry in proceedingGroupModel.entries!!) {
            if (entry.label!!.toLowerCase().contains(keyWord.toLowerCase()) ||
                    entry.url!!.toLowerCase() == keyWord.toLowerCase() ||
                    entry.username!!.toLowerCase() == keyWord.toLowerCase()) {
                localResult.add(buildPreviewModelFromEntryModel(entry))
            }
        }

        for (group in proceedingGroupModel.groups!!) {
            if (group.label!!.toLowerCase().contains(keyWord.toLowerCase())) {
                group.parentPath = thisModelPath
                localResult.add(buildPreviewModelFromGroupModel(group))
            }
        }
        if (!localResult.isEmpty()) {
            val isEditable = proceedingGroupModel.parentPath != null
            localResult.add(0, PrevieHeader(proceedingGroupModel.label!!, isEditable))
        }
        return localResult
    }

    private suspend fun removeEntryAt(childEntryRealIndex: Int, parentPath: String): GroupModel {
        KDBXRepository.deleteEntry("$parentPath$childEntryRealIndex/").await()
        return KDBXRepository.getGroupDtoByPath(parentPath)!!.toModel()
    }

    private suspend fun removeGroupAt(childGroupRealIndex: Int, parentPath: String): GroupModel {
        KDBXRepository.deleteGroup("$parentPath$childGroupRealIndex/").await()
        return KDBXRepository.getGroupDtoByPath(parentPath)!!.toModel()
    }

    private fun buildPreviewModelFromGroupModel(groupModel: GroupModel): PreviewModel = PreviewModel(
            groupModel.javaClass,
            groupModel.realIndex!!,
            groupModel.parentPath,
            groupModel.label!!,
            "",
            groupModel.isFavourite!!,
            null)


    private fun buildPreviewModelFromEntryModel(entryModel: EntryModel): PreviewModel = PreviewModel(
            entryModel.javaClass,
            entryModel.realIndex!!,
            currentGroupModelPath,
            entryModel.label!!,
            entryModel.url!!,
            entryModel.isFavourite!!,
            entryModel.iconData!!)


    private fun concatLinkedList(l1: List<PreviewInterface>, l2: List<PreviewInterface>): MutableList<PreviewInterface> {
        return (l1 + l2).toMutableList()
    }

    private fun openChildEntryAt(childEntryRealIndex: Int, parentPath: String): EntryModel {
        return KDBXRepository.getEntryDtoByPath("$parentPath$childEntryRealIndex/").toModel()
    }


}

interface PreviewListEvents {
    fun onGroupOpened(previewList: PreviewList)
    fun onEntryOpened(entryModel: EntryModel)
}

interface BackStackEvents {
    fun onSignOut()
    fun onParentGroupOpened(previewList: PreviewList)
}