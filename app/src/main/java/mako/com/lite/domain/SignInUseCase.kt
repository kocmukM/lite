package mako.com.lite.domain

import mako.com.lite.data.helpers.fingerprint.AndroidIndependentFingerprintCallbacks
import mako.com.lite.data.repository.FingerprintRepository
import mako.com.lite.domain.model.PreviewList
import mako.com.lite.domain.model.RecentFileModel
import java.io.File
import javax.inject.Inject

class SignInUseCase @Inject constructor(private val previewListUseCase: PreviewListUseCase,
                                        private val fingerprintRepository: FingerprintRepository,
                                        private val settingsUseCase: SettingsUseCase) {
    @Throws(Exception::class)
    suspend fun startFingerprinting(recentFileModel: RecentFileModel, events: OpenFileEvents) {
        val cryptoPassword = recentFileModel.filePassword
        if (cryptoPassword == "") throw Exception()
        fingerprintRepository.startFingerprinting(cryptoPassword, object : AndroidIndependentFingerprintCallbacks {
            override suspend fun onAuthenticationSucceeded(decryptedPassword: String) {
                events.onCorrectFinger(decryptedPassword)
            }

            override fun onAuthenticationFailed() {
                events.onWrongFinger()
            }

        })
    }

    suspend fun openFile(recentFileModel: RecentFileModel, kdbxFile: File, decryptedPassword: String): PreviewList {
        val properties = settingsUseCase.getPreviewProperties()
        return previewListUseCase.openFile(recentFileModel, kdbxFile, decryptedPassword, *properties.toTypedArray())
    }
}

interface OpenFileEvents {
    fun onCorrectFinger(decryptedPassword: String)
    fun onWrongFinger()
}