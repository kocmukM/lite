package mako.com.lite.domain

import mako.com.lite.data.repository.DatabaseRepository
import mako.com.lite.data.repository.dto.RecentFileDto
import mako.com.lite.data.repository.dto.toDto
import mako.com.lite.data.repository.dto.toModel
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel
import mako.com.lite.domain.model.RecentFileModel
import java.io.File
import java.util.*
import javax.inject.Inject

class RecentFilesUseCase @Inject constructor(private val databaseRepository: DatabaseRepository,
                                             private val filesUseCase: FilesUseCase) {
    private lateinit var recentFiles: MutableList<RecentFileModel>
    private val onScreenRecentFiles = LinkedList<RecentFileModel>()

    suspend fun init() {
        recentFiles = (databaseRepository.selectLocalRecentFiles().await().map { it.toModel() as RecentFileModel } +
                databaseRepository.selectGoogleRecentFiles().await().map { it.toModel() as RecentFileModel }).toMutableList()

        recentFiles.sort()
    }

    fun getRecentFiles(): List<RecentFileModel> {
        onScreenRecentFiles.clear()
        val maxForRemember = if (recentFiles.size > 6) {
            6
        } else {
            recentFiles.size
        }

        for (i in 0 until maxForRemember) {
            onScreenRecentFiles.add(recentFiles[i])
        }
        return onScreenRecentFiles
    }

    fun getLastOpenedFile(): RecentFileModel? {
        if (!recentFiles.isEmpty()) {
            return recentFiles[0]
        }
        return null
    }

    fun getLocalFileModelFromPath(filePath: String): LocalRecentFileModel {
        val file = File(filePath)
        return LocalRecentFileModel(file.parentFile.absolutePath + "/", file.name, "", 0L)
    }

    suspend fun deleteRecentFileModel(recentFileModel: RecentFileModel) {
        recentFiles.remove(recentFileModel)
        databaseRepository.deleteRecentFileModel(convert(recentFileModel))
        filesUseCase.deleteFile(recentFileModel)
    }

    suspend fun forgetRecentFileModel(recentFileModel: RecentFileModel) {
        recentFiles.remove(recentFileModel)
        databaseRepository.deleteRecentFileModel(convert(recentFileModel))
        filesUseCase.deleteCachedFile(recentFileModel)
    }

    fun convert(recentFileModel: RecentFileModel): RecentFileDto {
        return when (recentFileModel) {
            is LocalRecentFileModel -> {
                recentFileModel.toDto()
            }
            is GoogleRecentFileModel -> {
                recentFileModel.toDto()
            }
            else -> {
                throw Exception()
            }
        }
    }
}


