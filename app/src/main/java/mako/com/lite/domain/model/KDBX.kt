package mako.com.lite.domain.model

import java.io.Serializable
import java.util.*

class GroupModel : Serializable {
    var realIndex: Int? = null
    var parentPath: String? = null
    var label: String? = null
    var groups: MutableList<GroupModel>? = null
    var entries: MutableList<EntryModel>? = null
    var isFavourite: Boolean? = null
}

class EntryModel : Serializable, Comparable<EntryModel> {
    var realIndex: Int? = null
    var label: String? = null
    var url: String? = null
    var username: String? = null
    var password: String? = null
    var iconData: ByteArray? = null
    var customIconUUID: UUID? = null
    var notes: String? = null
    var isFavourite: Boolean? = null

    override fun compareTo(other: EntryModel): Int {
        return (this.label!! + this.url!! + this.username!! + this.password!! + this.notes!!).compareTo((other.label!! + other.url!! + other.username!! + other.password!! + other.notes!!))
    }

    fun isEmpty(): Boolean {
        return label == "" || password == ""
    }
}

class BreadcrumbModel(var path: String,
                      var title: String)

class PreviewModel(var instance: Class<Any>,
                   var realIndex: Int,
                   var parentPath: String?,
                   var title: String,
                   var subtitle: String,
                   var isFavourite: Boolean,
                   var iconData: ByteArray?)
    : PreviewInterface, Comparable<PreviewModel>, Serializable {

    override fun compareTo(other: PreviewModel): Int {
        return this.title.compareTo(other.title)
    }
}

class PrevieHeader(var groupName: String, var isEditable: Boolean) : PreviewInterface, Serializable

class PreviewList(var currentGroupModelPath: String, var previewModels: MutableList<PreviewInterface>) : Serializable

interface PreviewInterface : Serializable