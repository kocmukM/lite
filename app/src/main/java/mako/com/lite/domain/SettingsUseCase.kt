package mako.com.lite.domain

import mako.com.lite.data.helpers.sharedPreferences.SharedPreferencesHelperInterface
import mako.com.lite.presentation.ui.settings.assets.SettingsPick
import mako.com.lite.presentation.ui.settings.assets.SettingsSeek
import mako.com.lite.presentation.ui.settings.assets.SettingsSwitch
import java.util.*
import javax.inject.Inject

class SettingsUseCase @Inject constructor(private val sharedPreferencesHelper: SharedPreferencesHelperInterface) {
    private var timeoutSettingsArray = HashMap<String, Long>()
    private var sortValueArray = arrayOf("Сначала избранное", "Сортировать по имени")

    init {
        timeoutSettingsArray["1 min"] = 1000 * 60
        timeoutSettingsArray["2 min"] = 1000 * 2 * 60
        timeoutSettingsArray["5 min"] = 1000 * 5 * 60
        timeoutSettingsArray["15 min"] = 100 * 15 * 60
        timeoutSettingsArray["never"] = -1
    }

    private companion object {
        const val TIMEOUT_SP_KEY = "timeout"
        const val PASSWORD_LIMIT_SP_KEY = "password_limit"
        const val GOOGLE_SYNC_SP_KEY = "google_sync"
        @Volatile
        private var googleSyncEnabled: Boolean? = null

        const val DEFAULT_TIMEOUT_VALUE_INDEX = 3
        const val DEFAULT_PASSWORD_LIMIT_MIN = 3
        const val PASSWORD_LIMIT_MAX = 20

        const val DEFAULT_GOOGLE_SYNC_VALUE = false

        const val TIMEOUT_LAST_REGISTERED_SP = "last_timeout"

        const val SORT_BY_FAVORITE_KEY = "by_favorite"
        const val SORT_BY_FAVORITE_DEF_VALUE = true
        @Volatile
        private var sortByFavorite: Boolean? = null

        const val SORT_BY_NAME_KEY = "by_name"
        const val SORT_BY_NAME_DEF_VALUE = false
        @Volatile
        private var sortByName: Boolean? = null
    }

    fun getTimeoutSettingsObject(): SettingsPick {
        val pickValue = getTimeoutValue()
        return SettingsPick(timeoutSettingsArray.keys.toMutableList(), arrayOf(pickValue), object : SettingsPick.PickListener {
            override fun onPicked(newValue: Any) {
                sharedPreferencesHelper.edit {
                    putInt(TIMEOUT_SP_KEY, newValue as Int)
                }
            }

        }, SettingsPick.PickType.SINGLE)
    }

    fun getSortSettingsObject(): SettingsPick {
        val selectedPositions = LinkedList<Int>()
        val byFavorite = sortByFavorite
                ?: sharedPreferencesHelper.loadBoolean(SORT_BY_FAVORITE_KEY, SORT_BY_FAVORITE_DEF_VALUE)
        if (byFavorite)
            selectedPositions.add(0)

        val byName = sortByName
                ?: sharedPreferencesHelper.loadBoolean(SORT_BY_NAME_KEY, SORT_BY_NAME_DEF_VALUE)
        if (byName)
            selectedPositions.add(1)
        return SettingsPick(sortValueArray.toMutableList(), selectedPositions.toTypedArray(), object : SettingsPick.PickListener {
            override fun onPicked(newValue: Any) {
                @Suppress("UNCHECKED_CAST")
                val selectedIndicies = newValue as MutableList<Int>
                sharedPreferencesHelper.edit {
                    putBoolean(SORT_BY_FAVORITE_KEY, false)
                    putBoolean(SORT_BY_NAME_KEY, false)
                    sortByFavorite = false
                    sortByName = false
                }
                for (index in selectedIndicies) {
                    when (index) {
                        0 -> {
                            sharedPreferencesHelper.edit {
                                putBoolean(SORT_BY_FAVORITE_KEY, true)
                                sortByFavorite = true
                            }

                        }
                        1 -> {
                            sharedPreferencesHelper.edit {
                                putBoolean(SORT_BY_NAME_KEY, true)
                                sortByName = true
                            }
                        }
                    }
                }
            }

        }, SettingsPick.PickType.MULTIPLE)
    }

    fun getPreviewProperties(): List<PreviewProperties> {
        val properties = LinkedList<PreviewProperties>()
        val byFavorite = sortByFavorite ?: sharedPreferencesHelper.loadBoolean(SORT_BY_FAVORITE_KEY, SORT_BY_FAVORITE_DEF_VALUE)
        if (byFavorite)
            properties.add(PreviewProperties.FAVOURITE_AT_FIRST)

        val byName = sortByName ?: sharedPreferencesHelper.loadBoolean(SORT_BY_NAME_KEY, SORT_BY_NAME_DEF_VALUE)
        if (byName)
            properties.add(PreviewProperties.SORT_BY_LABEL)
        if (properties.isEmpty())
            properties.add(PreviewProperties.DEFAULT)
        return properties
    }

    fun saveBySortByVavorite(value: Boolean) {
        sharedPreferencesHelper.edit {
            putBoolean(SORT_BY_FAVORITE_KEY, value)
        }
        sortByFavorite = value
    }

    fun saveSortByName(value: Boolean) {
        sharedPreferencesHelper.edit {
            putBoolean(SORT_BY_NAME_KEY, value)
        }
        sortByName = value
    }

    fun getSortByFavoriteValue(): Boolean {
        return sortByFavorite ?: sharedPreferencesHelper.loadBoolean(SORT_BY_FAVORITE_KEY, SORT_BY_FAVORITE_DEF_VALUE)
    }

    fun getSortByNameValue(): Boolean {
        return sortByName ?:  sharedPreferencesHelper.loadBoolean(SORT_BY_NAME_KEY, SORT_BY_NAME_DEF_VALUE)
    }

    private fun getTimeoutValue(): Int {
        return sharedPreferencesHelper.loadInt(TIMEOUT_SP_KEY, DEFAULT_TIMEOUT_VALUE_INDEX)
    }

    private fun getTimeoutMillis(): Long {
        return timeoutSettingsArray.values.toMutableList()[getTimeoutValue()]
    }

    fun saveLastSessionEnd() {
        sharedPreferencesHelper.edit {
            putLong(TIMEOUT_LAST_REGISTERED_SP, System.currentTimeMillis())
        }
    }

    fun needReLogin(): Boolean {
        val lastTimeout = sharedPreferencesHelper.loadLong(TIMEOUT_LAST_REGISTERED_SP, System.currentTimeMillis())
        val waitTime = getTimeoutMillis()
        if (waitTime == -1L)
            return false
        if (System.currentTimeMillis() - lastTimeout > waitTime)
            return true
        return false
    }

    @Suppress("unused")
    fun getPasswordLimitSettings(): SettingsSeek {
        val seekValue = getPasswordLimit()
        return SettingsSeek(seekValue, object : SettingsSeek.SeekListener {
            override fun onSeekStateChanged(newValue: Int) {
                sharedPreferencesHelper.edit {
                    putInt(PASSWORD_LIMIT_SP_KEY, newValue)
                }
            }

        }, DEFAULT_PASSWORD_LIMIT_MIN, PASSWORD_LIMIT_MAX)
    }

    fun getPasswordLimit(): Int {
        return sharedPreferencesHelper.loadInt(PASSWORD_LIMIT_SP_KEY, DEFAULT_PASSWORD_LIMIT_MIN)
    }

    fun getGoogleSyncSettings(): SettingsSwitch {
        val switchValue = getGoogleSyncEnabled()
        return SettingsSwitch(switchValue, object : SettingsSwitch.SwitchListener {
            override fun onSwitchStateChanged(newState: Boolean) {
                sharedPreferencesHelper.edit {
                    putBoolean(GOOGLE_SYNC_SP_KEY, newState)
                }
                googleSyncEnabled = newState
            }

        })
    }

    fun getGoogleSyncEnabled(): Boolean {
        return googleSyncEnabled ?: sharedPreferencesHelper.loadBoolean(GOOGLE_SYNC_SP_KEY, DEFAULT_GOOGLE_SYNC_VALUE)
    }
}