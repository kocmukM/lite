package mako.com.lite.domain

import mako.com.lite.LiteApplication
import mako.com.lite.data.repository.KDBXRepository
import mako.com.lite.data.repository.dto.toDto
import mako.com.lite.data.repository.dto.toModel
import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.PreviewList
import javax.inject.Inject

class EntryUseCase @Inject constructor(private val previewListUseCase: PreviewListUseCase) {

    suspend fun saveEntry(oldEntryModel: EntryModel, newEntryModel: EntryModel, parentPath: String) {
        if (oldEntryModel == newEntryModel) return
        if (newEntryModel.label!!.isEmpty())
            throw LiteApplication.Exceptions.EmptyLabelException()
        val parentGroupDto = KDBXRepository.getGroupDtoByPath(parentPath)
        parentGroupDto!!.entries!![newEntryModel.realIndex!!] = newEntryModel.toDto()
        KDBXRepository.setGroupDtoByPath(parentPath, parentGroupDto).await()
    }

    suspend fun addEntry(newEntryModel: EntryModel, parentPath: String) {
        if (newEntryModel.label!!.isEmpty())
            throw LiteApplication.Exceptions.EmptyLabelException()
        val parentGroupDto = KDBXRepository.getGroupDtoByPath(parentPath)
        val realIndex = parentGroupDto!!.entries!!.size
        newEntryModel.realIndex = realIndex
        parentGroupDto.entries!!.add(newEntryModel.toDto())
        KDBXRepository.setGroupDtoByPath(parentPath, parentGroupDto).await()
    }

    fun getParentPreviewList(parentPath: String): PreviewList {
        val parentGroupModel = KDBXRepository.getGroupDtoByPath(parentPath)!!.toModel()
        previewListUseCase.currentGroupModelPath = parentPath
        return previewListUseCase.setPreviewOrder(parentGroupModel)
    }
}