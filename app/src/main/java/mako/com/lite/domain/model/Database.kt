package mako.com.lite.domain.model

import java.io.Serializable

class LocalRecentFileModel(override var fileDirectory: String,
                           override var fileName: String,
                           override var filePassword: String,
                           override var fileOpenTime: Long) : Serializable, RecentFileModel {
    override fun compareTo(other: RecentFileModel): Int {
        return other.fileOpenTime.compareTo(this.fileOpenTime)
    }
}

class GoogleRecentFileModel(override var fileDirectory: String,
                            override var fileName: String,
                            var stringDriveId: String,
                            override var filePassword: String,
                            override var fileOpenTime: Long) : Serializable, RecentFileModel {
    var isSynchronized = true
    override fun compareTo(other: RecentFileModel): Int {
        return other.fileOpenTime.compareTo(this.fileOpenTime)
    }
}

interface RecentFileModel : Serializable, Comparable<RecentFileModel> {
    var fileDirectory: String
    var fileName: String
    var filePassword: String
    var fileOpenTime: Long
}


