package mako.com.lite

import android.app.Application
import mako.com.lite.di.components.ApplicationComponent
import mako.com.lite.di.components.DaggerApplicationComponent
import mako.com.lite.di.modules.ApplicationModule

class LiteApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        const val DATABASE_VERSION = 1

        const val DATABASE_NAME = "lite.db"

        const val KDBX_DATABASE_NAME = "passwords.db"

        const val KEY_ALIAS = "androidkeepasslitealiasdonotchangeit"
        const val KEY_STORE = "AndroidKeyStore"
        const val TRANSFORMATION = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"

        const val PREFS_NAME="KeePassLitePref"

        const val DEFAULT_TIMEOUT_MILLIS_VALUE=60000L
        const val TIMEOUT_MILLIS_KEY="timeout_limit"

        const val DEFAULT_FABRIC_VALUE = BuildConfig.DEFAULT_FABRIC_VALUE
        const val FEEDBACK_KEY="feedback_property"

        const val DEFAULT_SYNC_VALUE=false
        const val SYNC_KEY="sync_property"

        private var mApplicationComponent: ApplicationComponent? = null
        var instance: LiteApplication? = null
        
        fun getComponent(): ApplicationComponent {
            if (mApplicationComponent == null) {
                mApplicationComponent = DaggerApplicationComponent.builder()
                        .applicationModule(ApplicationModule(instance!!))
                        .build()
            }
            return mApplicationComponent as ApplicationComponent
        }
    }

    class Exceptions {
        class GroupWithoutLabelException : Exception()
        class UpdateRemoteFileException : Exception()
        class WrongExtensionException(var extension: String) : Exception()
        class EmptyLabelException : Exception()
        class EmptyPasswordException : Exception()
        class ShortPasswordException(passwordLimit: Int) : Exception()

        class EmptyFileNameException : Exception()
        class WrongConfirmationPasswordException : Exception()
        class GoogleAuthorizationFailedException(): Exception()
    }


}