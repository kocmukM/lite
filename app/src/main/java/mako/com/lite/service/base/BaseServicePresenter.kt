package mako.com.lite.service.base

import java.lang.ref.WeakReference

abstract class BaseServicePresenter <T : ServiceInterface>: ServicePresenter<T> {
    private lateinit var mvpService: WeakReference<T>

    override fun attachService(service: T) {
        this.mvpService = WeakReference(service)
    }

    override fun detachService() {
        mvpService.clear()
    }
}