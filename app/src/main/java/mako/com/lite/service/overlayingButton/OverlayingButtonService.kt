package mako.com.lite.service.overlayingButton

import android.app.Service
import android.content.Intent
import android.graphics.PixelFormat
import android.os.IBinder
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import mako.com.lite.R
import mako.com.lite.service.base.BaseService
import javax.inject.Inject


class OverlayingButtonService: BaseService(), OverlayingButton.OverlayingButtonService {
    @Inject
    lateinit var presenter: OverlayingButtonPresenter

    lateinit var copyLayout: View

    lateinit var windowManager: WindowManager

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        presenter.attachService(this)
        presenter.startWork("","","")
        return Service.START_NOT_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        copyLayout = LayoutInflater.from(this).inflate(R.layout.item_overlay, null)

        val params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)

        params.gravity = Gravity.TOP and  Gravity.END
        params.x = 0
        params.y = 100

        windowManager = this.getSystemService(WINDOW_SERVICE) as WindowManager
        windowManager.addView(copyLayout, params)
    }

    override fun setButtonPosition(positionX: Int, positionY: Int) {

    }
}