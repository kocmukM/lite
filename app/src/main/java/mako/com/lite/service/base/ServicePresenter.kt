package mako.com.lite.service.base

interface ServicePresenter<in S: ServiceInterface> {
    fun attachService(service: S)
    fun detachService()
}