package mako.com.lite.service.overlayingButton

import mako.com.lite.service.base.ServiceInterface
import mako.com.lite.service.base.ServicePresenter

interface OverlayingButton {
    interface OverlayingButtonService: ServiceInterface {
        fun setButtonPosition(positionX: Int, positionY: Int)
    }
    interface OverlayingButtonPresenter: ServicePresenter<OverlayingButtonService> {
        fun startWork(url: String, username: String, password: String)
    }
}