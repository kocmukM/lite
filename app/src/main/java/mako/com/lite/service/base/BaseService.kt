package mako.com.lite.service.base

import android.app.Service
import mako.com.lite.LiteApplication
import mako.com.lite.di.components.DaggerServiceComponent
import mako.com.lite.di.components.ServiceComponent

abstract class BaseService : Service() {
    lateinit var serviceComponent: ServiceComponent

    override fun onCreate() {
        super.onCreate()
        serviceComponent =  DaggerServiceComponent.builder()
                .applicationComponent(LiteApplication.getComponent())
                .build()

    }


}