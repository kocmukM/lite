package mako.com.lite.service.overlayingButton

import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.service.base.BaseServicePresenter
import javax.inject.Inject

class OverlayingButtonPresenter @Inject constructor(private val router: RouterInterface) :
        BaseServicePresenter<OverlayingButton.OverlayingButtonService>(),
        OverlayingButton.OverlayingButtonPresenter {

    override fun startWork(url: String, username: String, password: String) {

    }

}