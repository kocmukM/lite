package mako.com.lite.presentation.ui.signin

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import mako.com.lite.domain.OpenFileEvents
import mako.com.lite.domain.SettingsUseCase
import mako.com.lite.domain.SignInUseCase
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import mako.com.lite.presentation.vibro.VibroHelperInterface
import java.io.File
import javax.inject.Inject

class SignInPresenter @Inject constructor(private val router: RouterInterface,
                                          private val signInUseCase: SignInUseCase,
                                          private val settingsUseCase: SettingsUseCase,
                                          private val vibroHelper: VibroHelperInterface) : BasePresenter<SignInInterface.SignInView>(), SignInInterface.SignInPresenter {


    lateinit var file: File
    lateinit var recentFileModel: RecentFileModel
    override fun startWork(recentFileModel: RecentFileModel, kdbxFile: File) {
        this.file = kdbxFile
        this.recentFileModel = recentFileModel
        view.run {
            setFileName(recentFileModel.fileName)
            when (recentFileModel) {
                is GoogleRecentFileModel -> {
                    setGoogleDriveSource()
                }
                is LocalRecentFileModel -> {
                    setFileDirectory(recentFileModel.fileDirectory)
                }
            }

        }
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            if (recentFileModel.filePassword != "") {
                try {
                    signInUseCase.startFingerprinting(recentFileModel, object : OpenFileEvents {
                        override fun onCorrectFinger(decryptedPassword: String) {
                            onCorrectFingerprint(decryptedPassword)
                        }

                        override fun onWrongFinger() {
                            CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                                vibroHelper.vibrateTwice()
                                view.setWrongFingerprint()
                            }
                        }
                    })
                } catch (e: Exception) {
                    hideFingerprint()
                }
            } else {
                hideFingerprint()
            }
        }
    }

    override fun anotherActionButtonClicked() {
        router.showStartScreen(true)
    }

    override fun signInButtonClicked(password: String) {
        view.animateOpenTask()
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            view.hideKeyboard()
            try {
                val previewList = signInUseCase.openFile(recentFileModel, file, password)
                settingsUseCase.saveLastSessionEnd()
                view.startTransitionAnimation().await()
                router.showListScreen(previewList, recentFileModel, true, false)
            } catch (e: Exception) {
                view.animFailOpenTask()
                e.printStackTrace()
                view.showError(e)
            }
        }
    }

    private fun onCorrectFingerprint(decryptedPassword: String) {
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            vibroHelper.vibrate()
            view.setCorrectFingerprint()
            signInButtonClicked(decryptedPassword)
        }
    }

    private fun hideFingerprint() {
        view.hideFingerprint()
    }

    override fun backPressed() {
        router.goBack()
    }
}