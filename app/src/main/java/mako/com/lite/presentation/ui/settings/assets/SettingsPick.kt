package mako.com.lite.presentation.ui.settings.assets

class SettingsPick(val range: MutableList<String>, val selectedPositions: Array<Int>, val listener: PickListener, val pickType: PickType) : SettingsBase() {
    interface PickListener {
        fun onPicked(newValue: Any)
    }

    enum class PickType {
        SINGLE,
        MULTIPLE
    }
}