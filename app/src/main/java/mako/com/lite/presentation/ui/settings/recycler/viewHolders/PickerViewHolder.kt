package mako.com.lite.presentation.ui.settings.recycler.viewHolders

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.item_list_multi_picker.view.*
import kotlinx.android.synthetic.main.item_list_single_picker.view.*
import kotlinx.android.synthetic.main.item_settings_picker.view.*
import kotlinx.android.synthetic.main.popup_picker.*
import mako.com.lite.R
import mako.com.lite.presentation.ui.settings.assets.SettingsBase
import mako.com.lite.presentation.ui.settings.assets.SettingsPick


class PickerViewHolder(itemView: View, val activity: Activity) : BaseViewHolder(itemView) {
    lateinit var dialog: AlertDialog
    lateinit var selectedIndexes: MutableList<Int>
    override fun performSetData(settingsBase: SettingsBase) {
        super.performSetData(settingsBase)
        val data = settingsBase as SettingsPick
        itemView.setOnClickListener {
            drawPopupNumberPicker(data)
        }
        if (data.pickType == SettingsPick.PickType.SINGLE)
            itemView.settings_pick_value.text = data.range[data.selectedPositions[0]]
    }

    private fun drawPopupNumberPicker(data: SettingsPick) {

        selectedIndexes = data.selectedPositions.toMutableList()


        val builder = AlertDialog.Builder(context)
        val dialogView = activity.layoutInflater.inflate(R.layout.popup_picker, null)
        builder.setView(dialogView)
        dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        dialog.show()

        if (data.pickType == SettingsPick.PickType.SINGLE)
            dialog.list_options.adapter = SinglePickListAdapter(context, data.range)
        else
            dialog.list_options.adapter = MultiPickListAdapter(context, data.range)
        dialog.buttons_container.visibility = View.VISIBLE

        dialog.save_pick_button.setOnClickListener {
            if (data.pickType == SettingsPick.PickType.SINGLE) {
                data.listener.onPicked(selectedIndexes[0])
            } else {
                data.listener.onPicked(selectedIndexes)
            }
            dialog.dismiss()
        }

        dialog.cancel_pick_button.setOnClickListener {
            dialog.dismiss()
        }

    }

    internal inner class SinglePickListAdapter(context: Context, private val data: MutableList<String>) : BaseAdapter() {
        lateinit var selectedRadio: ImageButton

        private val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


        override fun getCount(): Int {
            return data.size
        }

        override fun getItem(position: Int): Any {
            return data[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        @SuppressLint("ViewHolder")
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = inflater.inflate(R.layout.item_list_single_picker, null)
            val textView = view.findViewById<TextView>(R.id.list_element_text)
            textView.text = data[position]

            val radio = view.list_element_radio
            if (selectedIndexes[0] == position) {
                selectedRadio = radio
//                selectedRadio.setBackgroundResource(R.drawable.checked)
            }

            view.setOnClickListener {
                performClick(position, radio)
            }
            radio.setOnClickListener {
                performClick(position, radio)
            }
            return view
        }


        private fun performClick(position: Int, radio: ImageButton) {
            selectedRadio.setBackgroundResource(R.drawable.unchecked)
            selectedRadio = radio
            selectedRadio.setBackgroundResource(R.drawable.checked)

            selectedIndexes.clear()
            selectedIndexes.add(position)
            itemView.settings_pick_value.text = data[position]

        }
    }

    internal inner class MultiPickListAdapter(context: Context, private val data: MutableList<String>) : BaseAdapter() {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


        override fun getCount(): Int {
            return data.size
        }

        override fun getItem(position: Int): Any {
            return data[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        @SuppressLint("ViewHolder")
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = inflater.inflate(R.layout.item_list_multi_picker, null)
            val textView = view.findViewById<TextView>(R.id.list_element_text)
            textView.text = data[position].toString()

            val check = view.findViewById<CheckBox>(R.id.list_element_check)
            for (selectedIndex in selectedIndexes) {
                if (selectedIndex == position)
                    check.isChecked = true
            }

            view.setOnClickListener {
                performCick(position, view.list_element_check)
            }
            view.list_element_check.setOnClickListener {
                performCick(position, view.list_element_check)
            }
            return view
        }

        private fun performCick(position: Int, check: CheckBox) {
            if (selectedIndexes.contains(position)) {
                selectedIndexes.remove(position)
                check.isChecked = false
            } else {
                selectedIndexes.add(position)
                check.isChecked = true
            }
        }
    }
}