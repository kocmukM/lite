package mako.com.lite.presentation.ui.startup

import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import mako.com.lite.data.helpers.icons.IconRepositoryCallbacks
import mako.com.lite.domain.FilesUseCase
import mako.com.lite.domain.RecentFilesUseCase
import mako.com.lite.domain.SettingsUseCase
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.LogInListener
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooser
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooserInterface
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import mako.com.lite.presentation.ui.main.RuntimePermissions
import java.io.File
import javax.inject.Inject

class StartupPresenter @Inject constructor(private val router: RouterInterface,
                                           private val permissions: RuntimePermissions,
                                           private val recentFilesUseCase: RecentFilesUseCase,
                                           val googleDriveService: GoogleDriveServiceInterface,
                                           private val filesUseCase: FilesUseCase,
                                           private val fileStrategyChooser: FileStrategyChooserInterface,
                                           private val settingsUseCase: SettingsUseCase)
    : BasePresenter<StartupInterface.StartupView>(), StartupInterface.StartupPresenter {

    var loadingProgress = 0

    var loadingTask: Job? = null

    var recentFileModel: RecentFileModel? = null
    var mCacheFile: File? = null

    override fun startWork() {
        view.hideSearchIfNeed()
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            delay(5 * 1000)
            val currentTask = loadingTask
            if (currentTask != null && currentTask.isActive) {
                view.showSkipLoadingButton()
            }
        }
        permissions.requestPermissions(object : RuntimePermissions.PermissionsEvents {
            override fun onGranted() {
                if (settingsUseCase.getGoogleSyncEnabled()) {
                    googleDriveService.logInGoogle(object : LogInListener {
                        override fun onLoggedInGoogle() {
                            startLoading(true)
                        }

                        override fun onErrorLogIn(exception: Exception) {
                            startLoading(true)
                            view.showError(exception)
                        }

                    })
                } else {
                    startLoading(false)
                }
            }

            override fun onDenied() {
                router.showNoPermissionsScreen()
            }

            override fun onBlocked() {
                router.showNoPermissionsScreen()
            }
        })
    }

    override fun onSkipClicked() {
        loadingTask?.cancel()
        if (recentFileModel != null && mCacheFile != null) {
            finish(recentFileModel!!, mCacheFile!!)
        } else {
            CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                loadIcons(1.0).await()
                router.showStartScreen(false)
            }
        }
    }

    private fun startLoading(googleSyncEnabled: Boolean) {
        loadingTask = CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            recentFilesUseCase.init()
            val recentFileModel = recentFilesUseCase.getLastOpenedFile()
            if (recentFileModel == null) {
                loadIcons(1.0).await()
                view.showToolbar()
                router.showStartScreen(false)
                return@launch
            }
            filesUseCase.getFile(googleSyncEnabled, recentFileModel,

                    object : FilesUseCase.GetFileEvents {
                        override fun onFile(file: File) {
                            mCacheFile = file
                            finish(recentFileModel, file)
                        }

                        override fun onError(e: Exception) {
                            view.showError(e)
                        }

                    },

                    object : FilesUseCase.OpenChooseListener {
                        override fun onRequestOpenChooser(tmpCacheFile: File, cacheFile: File) {
                            loadingTask = CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                                fileStrategyChooser.showChooser(object : FileStrategyChooser.ChooserCallbacks {
                                    override fun onReplaceByGoogleClicked() {
                                        filesUseCase.updateLocalFile(recentFileModel, object : FilesUseCase.GetFileEvents {
                                            override fun onFile(file: File) {
                                                mCacheFile = file
                                                finish(recentFileModel, file)
                                            }

                                            override fun onError(e: Exception) {
                                                view.showError(e)
                                            }

                                        }, tmpCacheFile)
                                    }

                                    override fun onReplaceByLocalClicked() {
                                        try {
                                            filesUseCase.updateRemoteFile(recentFileModel)
                                        } catch (e: Exception) {
                                            view.showError(e)
                                        }
                                        finish(recentFileModel, cacheFile)
                                    }

                                    override fun onOpenLocalClicked() {
                                        filesUseCase.continueWithoutChanges(cacheFile, object : FilesUseCase.GetFileEvents {
                                            override fun onFile(file: File) {
                                                mCacheFile = file
                                                finish(recentFileModel, file)
                                            }

                                            override fun onError(e: Exception) {
                                                view.showError(e)
                                            }

                                        })
                                    }

                                })
                            }

                        }

                    })

        }
    }

    private fun finish(recentFileModel: RecentFileModel, file: File) {
        loadingTask = CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            loadIcons(1.0).await()
            view.showToolbar()
            router.showSignInScreen(recentFileModel, file, false, false)

        }
    }

    private fun changeLoadingProgress(progressPercentage: Int) {
        view.run {
            setLoadingProgress(progressPercentage)
        }
    }

    private fun loadIcons(k: Double): Deferred<Unit> = CoroutineScope(kotlinx.coroutines.Dispatchers.Main).async {
        view.run {
            setLoadingProgressTest(StartupFragment.LoadingStatus.LOADING_IMAGES)
            loadIcons(object : IconRepositoryCallbacks {
                override fun onLoadingProgress(progressPercentage: Int) {
                    loadingProgress += (progressPercentage * k).toInt()
                    changeLoadingProgress(loadingProgress)
                }

            })

        }
    }

    override fun backPressed() {
        router.goBack()
    }

}