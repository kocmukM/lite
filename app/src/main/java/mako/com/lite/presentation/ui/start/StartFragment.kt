package mako.com.lite.presentation.ui.start

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_start.*
import kotlinx.android.synthetic.main.popup_open_option.view.*
import mako.com.lite.R
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.ui.base.BaseFragment
import javax.inject.Inject


class StartFragment : BaseFragment(), StartInterface.StartView {
    @Inject
    lateinit var presenter: StartPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity?.hideBreadCrumbToolbar()
        mainActivity?.search_toolbar?.visibility = View.INVISIBLE
        mainActivity?.brand?.visibility = View.VISIBLE
        presenter.startWork()
        open_button.setOnClickListener {
            presenter.onPickFileButtonClicked()
        }

        create_button.setOnClickListener {
            presenter.onCreateFileButtonClicked()
        }
    }

    override fun drawRecentFiles(recentFileList: List<RecentFileModel>, events: RecentFileAdapterEvents) {
        recent_open_files_recycle.layoutManager = LinearLayoutManager(activityContext)
        if(recent_open_files_recycle.adapter != null) {
            (recent_open_files_recycle.adapter as RecentFileAdapter).updateRecentFileList(recentFileList)
        } else {
            recent_open_files_recycle.adapter = RecentFileAdapter(recentFileList, events)
        }
    }

    override fun showOpenOptions() {
        val builder = AlertDialog.Builder(activityContext)
        val dialogView = mainActivity!!.layoutInflater.inflate(R.layout.popup_open_option, null)
        builder.setView(dialogView)
        val dialog = builder.create()
        dialog.show()

        dialogView.open_from_local_storage.setOnClickListener {
            presenter.onPickLocalFileButtonClicked()
            dialog.dismiss()
        }
        dialogView.open_from_google_storage.setOnClickListener {
            presenter.onPickGoogleFileButtonClicked()
            dialog.dismiss()
        }
    }

    override fun showDeleteDialog(events: StartInterface.StartView.DeleteDialogEvents) {
        val builder = AlertDialog.Builder(activityContext)
        val dialog = builder
                .setNegativeButton(R.string.cancel) { dialog, which ->
                    dialog.dismiss()
                }
                .setPositiveButton(R.string.delete_preview_model) { dialog, which ->
                    events.onDelete()
                    dialog.dismiss()
                }
                .setTitle(R.string.delete_dialog_title)
                .setMessage(R.string.delete_dialog_message)
                .create()
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    //====================================================
    //      Working with Google Drive Service
    //====================================================


    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.googleDriveService.onActivityResult(requestCode, data)
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

}