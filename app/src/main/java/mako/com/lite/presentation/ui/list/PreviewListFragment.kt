package mako.com.lite.presentation.ui.list

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.elements_add_subfab.*
import kotlinx.android.synthetic.main.fragment_main_list.*
import kotlinx.android.synthetic.main.popup_create_group.view.*
import kotlinx.android.synthetic.main.popup_sort.view.*
import mako.com.lite.R
import mako.com.lite.domain.model.*
import mako.com.lite.presentation.ui.base.BaseFragment
import mako.com.lite.presentation.ui.views.notifications.InsetNotSyncedNotification
import mako.com.lite.presentation.ui.views.notifications.InsetOfflineNotification
import java.util.*
import javax.inject.Inject


class PreviewListFragment : BaseFragment(), PreviewListInterface.ListView {
    @Inject
    lateinit var presenter: PreviewListPresenter

    companion object {
        private const val SUB_FAB_MARGINS = 10
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)

    }

    override fun showToolbars() {
        mainActivity?.showMainToolbar()
        mainActivity?.showBreadcrumbToolbar()
        mainActivity?.search_toolbar?.visibility = View.VISIBLE
        mainActivity?.brand?.visibility = View.INVISIBLE
    }

    override fun hideToolbars(events: PreviewListInterface.ListView.OnHideEvents) {
        mainActivity?.hideBreadCrumbToolbar()
        mainActivity?.search_toolbar?.visibility = View.INVISIBLE
        mainActivity?.brand?.visibility = View.VISIBLE
        events.onHide()
    }

    override fun hideMainToolbar() {
        mainActivity?.hideMainToolbar()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.startWork(arguments!!.getSerializable("preview_models") as PreviewList, arguments!!.getSerializable("recent_file_model") as RecentFileModel)
        super.onViewCreated(view, savedInstanceState)
        add_fab.setOnClickListener {
            presenter.bigAddFabClicked()
        }
        mainActivity!!.sort_by_button.setOnClickListener {
            presenter.sortClicked()
        }
    }

    override fun setEmptyListPlaceholder() {
        val builder = SpannableStringBuilder()
        builder.append(getString(R.string.empty_list_placeholder_title_text))
                .append(getString(R.string.empty_list_placeholder_instruction_press_text))
                .append(" ", ImageSpan(activityContext, R.drawable.add_black), 0)
                .append(getString(R.string.empty_list_placeholder_instruction_to_create_new_text))

        placeholder_text.text = builder
        placeholder_text.visibility = View.VISIBLE
    }

    override fun setEmptySearchResultPlaceholder() {
        placeholder_text.text = getString(R.string.empty_search_result_placeholder)
        placeholder_text.visibility = View.VISIBLE
    }

    override fun hidePlaceholder() {
        placeholder_text.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    override fun setNotSyncedNotification(notSyncedEvents: InsetNotSyncedNotification.NotSyncedEvents) {
        val notificatView = InsetNotSyncedNotification(activityContext).showNotSynced(notSyncedEvents)
        notifications_list_container.addView(notificatView)
    }

    override fun setOfflineNotification(offlineEvents: InsetOfflineNotification.OfflineEvents) {
        val notificatView = InsetOfflineNotification(activityContext).showNotification(offlineEvents)
        notifications_list_container.addView(notificatView)
    }

    override fun hideNotificationAt(index: Int) {
        notifications_list_container.removeViewAt(index)
    }

    override fun showCreateGroupDialog() {
        val builder = AlertDialog.Builder(activityContext)
        val dialogView = mainActivity!!.layoutInflater.inflate(R.layout.popup_create_group, null)
        builder.setView(dialogView)
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.show()
        dialogView.save_group_button.setOnClickListener {
            presenter.onCreateGroupClicked(dialogView.label.text.toString())
            dialog.dismiss()
        }
        dialogView.cancel_new_group_button.setOnClickListener {
            dialog.dismiss()
        }

    }

    override fun showSortOptions(byFavorite: Boolean, byName: Boolean) {
        val builder = AlertDialog.Builder(activityContext)
        val dialogView = mainActivity!!.layoutInflater.inflate(R.layout.popup_sort, null)
        builder.setView(dialogView)
        val dialog = builder.create()
        dialog.show()
        dialogView.checkbox_by_favourite.isChecked = byFavorite
        dialogView.checkbox_by_name.isChecked = byName
        dialogView.save_sort.setOnClickListener {
            presenter.onSortOptionsSelected(dialogView.checkbox_by_favourite.isChecked, dialogView.checkbox_by_name.isChecked)
            dialog.dismiss()
        }
        dialogView.cancel_sort.setOnClickListener {
            dialog.dismiss()
        }
    }

    override fun showDeleteDialog(events: PreviewListInterface.ListView.DeleteDialogEvents) {
        val builder = AlertDialog.Builder(activityContext)
        val dialog = builder
                .setNegativeButton(R.string.cancel) { dialog, which ->
                    dialog.dismiss()
                }
                .setPositiveButton(R.string.delete_preview_model) { dialog, which ->
                    events.onDelete()
                    dialog.dismiss()
                }
                .setTitle(R.string.delete_dialog_title)
                .setMessage(R.string.delete_dialog_message)
                .create()
        dialog.show()
    }

    override fun showAddOptions() {
        mainActivity?.bread_crumbs_toolbar?.bringToFront()
        add_options.visibility = View.VISIBLE

        val showAddEntryOptionAnimation = AnimationUtils.loadAnimation(activityContext, R.anim.add_entry_show_anim)
        val addEntryOptionLayoutParams = add_entry_container.layoutParams as RelativeLayout.LayoutParams

        val showAddGroupOptionAnimation = AnimationUtils.loadAnimation(activityContext, R.anim.add_group_show_anim)
        val addGroupOptionLayoutParams = add_group_container.layoutParams as RelativeLayout.LayoutParams

        addEntryOptionLayoutParams.bottomMargin += (add_entry_container.height * 2.4 + SUB_FAB_MARGINS * 2).toInt()
        add_entry_container.layoutParams = addEntryOptionLayoutParams
        add_entry_container.startAnimation(showAddEntryOptionAnimation)

        addGroupOptionLayoutParams.bottomMargin += (add_group_container.height * 1.2 + SUB_FAB_MARGINS).toInt()
        add_group_container.layoutParams = addGroupOptionLayoutParams
        add_group_container.startAnimation(showAddGroupOptionAnimation)

        val animSet = AnimationSet(true)
        animSet.interpolator = DecelerateInterpolator()
        animSet.fillAfter = true
        animSet.isFillEnabled = true

        val animRotate = RotateAnimation(0.0f, -45.0f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f)

        animRotate.duration = 200
        animRotate.fillAfter = true
        animSet.addAnimation(animRotate)

        add_fab.startAnimation(animSet)

        add_entry_fab.setOnClickListener {
            presenter.addEntryClicked()
        }

        add_group_fab.setOnClickListener {
            presenter.addGroupClicked()
        }
        add_options.setOnClickListener {
            presenter.bigAddFabClicked()
        }
    }

    override fun hideAddOptions() {
        val hideAddEntryOptionAnimation = AnimationUtils.loadAnimation(activityContext, R.anim.add_entry_hide_anim)
        val addEntryOptionLayoutParams = add_entry_container.layoutParams as RelativeLayout.LayoutParams

        val hideAddGroupOptionAnimation = AnimationUtils.loadAnimation(activityContext, R.anim.add_group_hide_anim)
        val addGroupOptionLayoutParams = add_group_container.layoutParams as RelativeLayout.LayoutParams

        addEntryOptionLayoutParams.bottomMargin -= (add_entry_container.height * 2.4 + SUB_FAB_MARGINS * 2).toInt()
        add_entry_container.layoutParams = addEntryOptionLayoutParams
        add_entry_container.startAnimation(hideAddEntryOptionAnimation)

        addGroupOptionLayoutParams.bottomMargin -= (add_group_container.height * 1.2 + SUB_FAB_MARGINS).toInt()
        add_group_container.layoutParams = addGroupOptionLayoutParams
        add_group_container.startAnimation(hideAddGroupOptionAnimation)

        val animSet = AnimationSet(true)
        animSet.interpolator = DecelerateInterpolator()
        animSet.fillAfter = true
        animSet.isFillEnabled = true

        val animRotate = RotateAnimation(-45.0f, 0.0f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f)

        animRotate.duration = 200
        animRotate.fillAfter = true
        animSet.addAnimation(animRotate)

        add_fab.startAnimation(animRotate)

        add_entry_fab.setOnClickListener(null)

        add_group_fab.setOnClickListener(null)

        add_options.setOnClickListener(null)

        hideAddGroupOptionAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                add_options?.visibility = View.GONE
            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })


    }

    override fun drawPreviewList(previewList: MutableList<PreviewInterface>, events: PreviewItemEvents) {

        val recentFileAdapter = PreviewListAdapter(previewList, events)
        val viewManager: RecyclerView.LayoutManager
        viewManager = LinearLayoutManager(activityContext)
        preview_recycle_list?.apply {
            layoutManager = viewManager
            adapter = recentFileAdapter
        }

        val itemTouchHelper = ItemTouchHelper(object : SwipeToCopyCallback(activityContext) {
            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                presenter.copyClicked(previewList[p0.adapterPosition] as PreviewModel, p0.adapterPosition)
            }
        })
        itemTouchHelper.attachToRecyclerView(preview_recycle_list)

    }

    override fun startSearchListener() {
        mainActivity!!.search_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.searchTextChanged(mainActivity!!.search_edit_text.text.toString())
            }
        })
    }

    override fun clearSearchText() {
        mainActivity!!.search_edit_text?.text?.clear()
    }

    override fun setGlobalSearchButton() {
        do_global_search_button?.text = resources.getText(R.string.global_search_text)
        do_global_search_button?.setOnClickListener {
            presenter.doGlobalSearchClicked(mainActivity!!.search_edit_text.text.toString())
        }
    }

    override fun setBackFromSearchButton() {
        do_global_search_button?.text = resources.getText(R.string.back)
        do_global_search_button?.setOnClickListener {
            mainActivity!!.search_edit_text.clearFocus()
            presenter.backFromSearchClicked()
        }
    }

    override fun showSearchButton() {
        do_global_search_button?.visibility = View.VISIBLE
    }

    override fun hideSearchButton() {
        do_global_search_button?.visibility = View.GONE
    }

    override fun drawBreadCrumbs(breadCrumbs: LinkedList<BreadcrumbModel>) {
        mainActivity!!.bread_crumbs_container.removeAllViews()
        for (breadCrumb in breadCrumbs) {
            val breadCrumbButton = (LayoutInflater.from(activityContext).inflate(R.layout.custom_view_bread_crumb_item, null, false)).findViewById<Button>(R.id.bread_crumb_button)

            breadCrumbButton.text = breadCrumb.title
            breadCrumbButton.setOnClickListener {
                presenter.onBreadCrumbClicked(breadCrumb)
            }
            mainActivity!!.bread_crumbs_container.addView(breadCrumbButton)
        }
    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.googleDriveService.onActivityResult(requestCode, data)
    }

    override fun notifyListItemChanged(position: Int) {
        preview_recycle_list.adapter?.notifyItemChanged(position)
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun showCopiedToast() {
        Toast.makeText(activityContext, R.string.copied_message, Toast.LENGTH_SHORT).show()
    }
}