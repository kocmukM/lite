package mako.com.lite.presentation.ui.settings.recycler.viewHolders

import android.view.View
import kotlinx.android.synthetic.main.item_settings_switch.view.*
import mako.com.lite.presentation.ui.settings.assets.SettingsBase
import mako.com.lite.presentation.ui.settings.assets.SettingsSwitch

class SwitchViewHolder(itemView: View) : BaseViewHolder(itemView) {

    override fun performSetData(settingsBase: SettingsBase) {
        super.performSetData(settingsBase)
        val data = settingsBase as SettingsSwitch
        itemView.settings_switch.isChecked = data.value
        itemView.settings_switch.setOnCheckedChangeListener { switch, isChecked ->
            data.listener.onSwitchStateChanged(isChecked)
        }


    }
}