package mako.com.lite.presentation.ui.start

import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract

interface StartInterface {
    interface StartView : ViewContract {
        fun drawRecentFiles(recentFileList: List<RecentFileModel>, events: RecentFileAdapterEvents)
        fun showOpenOptions()
        fun showDeleteDialog(deleteDialogEvents: DeleteDialogEvents)

        interface DeleteDialogEvents {
            fun onDelete()
        }
    }

    interface StartPresenter : Presenter<StartView> {
        fun startWork()
        fun onPickFileButtonClicked()
        fun onPickLocalFileButtonClicked()
        fun onPickGoogleFileButtonClicked()
        fun onCreateFileButtonClicked()

        fun onResume()
    }
}