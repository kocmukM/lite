package mako.com.lite.presentation.ui.startup

import mako.com.lite.data.helpers.icons.IconRepositoryCallbacks
import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract

interface StartupInterface {
    interface StartupView : ViewContract {
        suspend fun loadIcons(iconRepositoryCallbacks: IconRepositoryCallbacks)
        fun setLoadingProgress(loadingProgress: Int)
        fun setLoadingProgressTest(status: StartupFragment.LoadingStatus)
        fun showToolbar()
        fun showSkipLoadingButton()
        fun hideSearchIfNeed()
    }

    interface StartupPresenter : Presenter<StartupView> {
        fun startWork()
        fun onSkipClicked()
    }
}