package mako.com.lite.presentation.ui.blockedPermission

import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import javax.inject.Inject

class BlockedPermissionPresenter @Inject constructor(private val router: RouterInterface)
    : BasePresenter<BlockedPermissionInterface.BlockedPermissionView>(),
        BlockedPermissionInterface.BlockedPermissionPresenter {
    override fun backPressed() {
        router.goBack()
    }

    override fun openPermissionSettingsClicked() {
        router.openPermissionSettings()
    }
}