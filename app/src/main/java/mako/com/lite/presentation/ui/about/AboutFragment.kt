package mako.com.lite.presentation.ui.about

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_about.*
import mako.com.lite.R
import mako.com.lite.presentation.ui.base.BaseFragment
import javax.inject.Inject


class AboutFragment : BaseFragment(), AboutInterface.AboutView {
    @Inject
    lateinit var presenter: AboutPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.startWork()
        super.onViewCreated(view, savedInstanceState)
        try {
            val pInfo = activityContext.packageManager.getPackageInfo(activityContext.packageName, 0)
            version.text = pInfo.versionName
            build.text = pInfo.versionCode.toString()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        close.setOnClickListener {
            presenter.closeClicked()
        }

    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun hideToolbar() {
        mainActivity?.hideMainToolbar()
    }

    override fun showToolbar() {
        mainActivity?.showMainToolbar()
    }

    override fun disableDrawer() {
        mainActivity?.disableNavigationDrawer()
    }

    override fun enableDrawer() {
        mainActivity?.enableNavigationDrawer()
    }
}