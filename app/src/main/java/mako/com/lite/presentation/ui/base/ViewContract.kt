package mako.com.lite.presentation.ui.base


interface ViewContract : ViewContractErrors, ViewContractProgress, ViewContractKeyboard

interface ViewContractErrors {
    fun showError(exception: Exception)
}

interface ViewContractKeyboard {
    fun hideKeyboard()
}

interface ViewContractProgress {

}