package mako.com.lite.presentation.ui.start

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.zawadz88.materialpopupmenu.popupMenu
import kotlinx.android.synthetic.main.item_recent_file.view.*
import mako.com.lite.R
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel
import mako.com.lite.domain.model.RecentFileModel


class RecentFileAdapter(private var recentFileList: List<RecentFileModel>, private val events: RecentFileAdapterEvents) : RecyclerView.Adapter<RecentFileAdapter.ViewHolder>() {
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recent_file, parent, false)
        context = v.context
        return ViewHolder(v)

    }

    fun updateRecentFileList(recentFileList: List<RecentFileModel>) {
        this.recentFileList = recentFileList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recentFile = recentFileList[position]
        val fileName = recentFile.fileName
        val fileDirectory: String
        val iconResource: Int
        when (recentFile) {
            is GoogleRecentFileModel -> {
                fileDirectory = context.resources.getString(R.string.google_drive_file_directory_text)
                iconResource = R.drawable.google_drive
            }
            else -> {
                fileDirectory = recentFile.fileDirectory
                iconResource = R.drawable.smartfone
            }
        }

        holder.recentSourceType.setImageResource(iconResource)
        holder.recentFileDir.text = fileDirectory
        holder.recentFileName.text = fileName
        holder.recentFileMenuButton.setOnClickListener {
            val popupMenu = popupMenu {
                style = R.style.Widget_MPM_Menu_Dark_CustomBackgroundBottom
                dropdownGravity = Gravity.END
                section {
                    item {
                        label = context.getString(R.string.forget)
                        iconDrawable = context.getDrawable(R.drawable.close)
                        callback = {
                            events.forgetRecentFileClicked(recentFile)
                        }
                    }
                    item {
                        label = context.getString(R.string.safe_delete)
                        iconDrawable = context.getDrawable(R.drawable.delete)
                        callback = {
                            events.deleteRecentFileClicked(recentFile)
                        }
                    }
                    if(recentFile is LocalRecentFileModel) {
                        item {
                            label = context.getString(R.string.upload)
                            iconDrawable = context.getDrawable(R.drawable.upload)
                            callback = {
                                throw RuntimeException("Not implemented")
                            }
                        }
                    }
                }
            }
            popupMenu.show(context, holder.separator)
        }

        holder.container.setOnClickListener {
            events.openRecentFileClicked(recentFile)
        }
    }

    override fun getItemCount(): Int {
        return recentFileList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container = itemView
        val recentFileDir = itemView.recent_file_dir!!
        val recentFileName = itemView.recent_file_name!!
        val recentSourceType = itemView.recent_file_source_type!!
        val recentFileMenuButton = itemView.recent_file_menu
        val separator = itemView.separator!!

    }

}

interface RecentFileAdapterEvents {
    fun openRecentFileClicked(recentFileModel: RecentFileModel)
    fun deleteRecentFileClicked(recentFileModel: RecentFileModel)
    fun forgetRecentFileClicked(recentFileModel: RecentFileModel)
}