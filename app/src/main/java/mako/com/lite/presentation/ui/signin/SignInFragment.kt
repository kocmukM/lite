package mako.com.lite.presentation.ui.signin

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.async
import mako.com.lite.R
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.ui.base.BaseFragment
import java.io.File
import javax.inject.Inject

class SignInFragment : BaseFragment(), SignInInterface.SignInView {
    @Inject
    lateinit var presenter: SignInPresenter
    lateinit var mView: View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_sign_in, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
       super.onViewCreated(view, savedInstanceState)
        mainActivity?.hideBreadCrumbToolbar()
        mainActivity?.search_toolbar?.visibility = View.INVISIBLE
        mainActivity?.brand?.visibility = View.VISIBLE
        presenter.startWork(arguments!!.getSerializable("file_model") as RecentFileModel, arguments!!.getSerializable("file") as File)

        this.sign_in_button.setOnClickListener {
            presenter.signInButtonClicked(password.text.toString())
        }
        another_action_button.setOnClickListener {
            presenter.anotherActionButtonClicked()
        }
    }

    override fun startTransitionAnimation() = CoroutineScope(kotlinx.coroutines.Dispatchers.Main).async {
        //        RevealAnimation().registerCircularRevealAnimation(activityContext,mView, RevealAnimationSetting(sign_in_button.x, sign_in_button.y, main_layout.width, main_layout.height), Color.RED, Color.RED)
//        sign_in_button.animate().scaleX(1080f).scaleY(1920f).setDuration(2000).setInterpolator(LinearInterpolator()).start()
    }

    override fun setFileDirectory(fileDirectory: String) {
        file_directory.text = fileDirectory
    }

    override fun setGoogleDriveSource() {
        file_directory.text = activityContext.getText(R.string.google_drive_file_directory_text)
        file_directory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.google_drive, 0)
    }

    override fun setFileName(fileName: String) {
        file_name.text = fileName
    }

    override fun setCorrectFingerprint() {
        fingerprint_image?.setImageDrawable(activityContext.getDrawable(R.drawable.check))
    }

    override fun setWrongFingerprint() {
        fingerprint_image?.setImageDrawable(activityContext.getDrawable(R.drawable.error_red))
    }

    override fun hideFingerprint() {
        fingerprint_image.visibility = View.GONE
    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun animateOpenTask() {
        sign_in_button?.startAnimation()

    }

    override fun animFailOpenTask() {
        sign_in_button.revertAnimation()
    }

    override fun getSignInButton(): View {
        return sign_in_button
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }
}