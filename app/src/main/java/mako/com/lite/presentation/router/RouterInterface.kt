package mako.com.lite.presentation.router

import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.PreviewList
import mako.com.lite.domain.model.RecentFileModel
import java.io.File


interface RouterInterface {
    fun showStartupScreen()
    fun showNoPermissionsScreen()
    fun showStartScreen(needBackStack: Boolean)
    fun showSignInScreen(recentFileModel: RecentFileModel, kdbxFile: File, needBackStack: Boolean, reverseAnimation: Boolean)
    fun showCreateFileScreen()
    fun showListScreen(previewList: PreviewList, recentFileModel: RecentFileModel, needBackStack: Boolean, reverseAnimation: Boolean)
    fun showEntryScreen(entryModel: EntryModel?, parentPath: String, recentFileModel: RecentFileModel)
    fun openPermissionSettings()
    fun openSettings()
    fun openAbout()

    fun goBack()
}