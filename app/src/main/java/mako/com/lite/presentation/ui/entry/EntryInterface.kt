package mako.com.lite.presentation.ui.entry

import mako.com.lite.data.repository.dto.IconDto
import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract
import mako.com.lite.presentation.ui.views.notifications.InsetOfflineNotification

interface EntryInterface {
    interface ViewInterface : ViewContract {
        fun setLabel(label: String)
        fun setUrl(url: String)
        fun setUsername(username: String)
        fun setPassword(password: String)
        fun setComments(comments: String)

        fun getLabel(): String
        fun getUrl(): String
        fun getUsername(): String
        fun getPassword(): String
        fun getComments(): String

        fun setReadMode()
        fun setWriteMode()

        fun hideNotificationAt(index: Int)
        fun setOfflineNotification(offlineEvents: InsetOfflineNotification.OfflineEvents)

        fun expandBottomSheet()
        fun hideBottomSheet()

        fun setIcon(iconBytes: ByteArray)
    }

    interface PresenterInterface : Presenter<ViewInterface> {
        fun startWork(entryModel: EntryModel?, parentPath: String, recentFileModel: RecentFileModel)
        fun onEditSaveClicked()
        fun onCloseClicked()

        fun iconClicked()
        fun iconPicked(iconDto: IconDto)
        fun cancelButtonSheetClicked()
        fun copyButtonClicked()

        fun onResume()
        fun onPause()
    }
}