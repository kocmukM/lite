package mako.com.lite.presentation.ui.entry

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mako.com.lite.data.helpers.icons.IconHelper
import mako.com.lite.data.repository.dto.IconDto
import mako.com.lite.domain.EntryUseCase
import mako.com.lite.domain.FilesUseCase
import mako.com.lite.domain.SettingsUseCase
import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.copyNotification.CopyNotificationInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.LogInListener
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import mako.com.lite.presentation.ui.views.notifications.InsetOfflineNotification
import javax.inject.Inject

class EntryPresenter @Inject constructor(private val router: RouterInterface,
                                         private val entryUseCase: EntryUseCase,
                                         private val filesUseCase: FilesUseCase,
                                         private val copyNotification: CopyNotificationInterface,
                                         val googleDriveService: GoogleDriveServiceInterface,
                                         private val settingsUseCase: SettingsUseCase) :
        BasePresenter<EntryInterface.ViewInterface>(),
        EntryInterface.PresenterInterface {

    enum class Mode {
        READ,
        EDIT,
        ADD
    }

    private lateinit var oldEntryModel: EntryModel
    private lateinit var recentFileModel: RecentFileModel
    private lateinit var parentPath: String

    private var mode: Mode = Mode.READ

    private var notificationCount = 0

    private var icon: IconDto? = null


    override fun onResume() {
        if (settingsUseCase.needReLogin()) {
            router.showStartupScreen()
        }
    }

    override fun onPause() {
        settingsUseCase.saveLastSessionEnd()
    }

    override fun startWork(entryModel: EntryModel?, parentPath: String, recentFileModel: RecentFileModel) {
        notificationCount = 0
        if (settingsUseCase.getGoogleSyncEnabled()) {
            googleDriveService.logInGoogle(object : LogInListener {
                override fun onLoggedInGoogle() {
                    continueWork(entryModel, parentPath, recentFileModel)
                }

                override fun onErrorLogIn(exception: Exception) {
                    view.showError(exception)
                    notificationCount++
                    view.setOfflineNotification(object : InsetOfflineNotification.OfflineEvents {
                        override fun onCloseClicked() {
                            view.hideNotificationAt(notificationCount)
                            notificationCount--
                        }

                        override fun onTryAgainClicked() {
                            startWork(entryModel, parentPath, recentFileModel)
                            view.hideNotificationAt(notificationCount)
                            notificationCount--
                        }

                    })
                    continueWork(entryModel, parentPath, recentFileModel)
                }

            })
        } else {
            continueWork(entryModel, parentPath, recentFileModel)
        }
        view.hideBottomSheet()
    }

    private fun continueWork(entryModel: EntryModel?, parentPath: String, recentFileModel: RecentFileModel) {
        if (entryModel != null) {
            oldEntryModel = entryModel
            view.run {
                setReadMode()
                if (entryModel.iconData != null)
                    setIcon(entryModel.iconData!!)
                setLabel(entryModel.label!!)
                setUrl(entryModel.url!!)
                setUsername(entryModel.username!!)
                setPassword(entryModel.password!!)
                setComments(entryModel.notes!!)
            }
            if (entryModel.customIconUUID != null)
                icon = IconHelper.getIconByUUID(entryModel.customIconUUID!!)
        } else {
            mode = Mode.ADD
            view.setWriteMode()
        }

        this.parentPath = parentPath
        this.recentFileModel = recentFileModel
    }

    override fun onEditSaveClicked() {
        when (mode) {
            Mode.READ -> {
                view.setWriteMode()
                mode = Mode.EDIT
            }

            Mode.EDIT -> {
                view.setReadMode()
                saveChanges()
                mode = Mode.READ
            }

            Mode.ADD -> {
                view.setReadMode()
                addEntry()
                mode = Mode.READ
            }
        }
    }

    override fun onCloseClicked() {
        val previewModel = entryUseCase.getParentPreviewList(parentPath)
        router.showListScreen(previewModel, recentFileModel, false, true)
    }

    override fun copyButtonClicked() {
        copyNotification.buildCopyNotification(oldEntryModel.label!!, oldEntryModel.url, oldEntryModel.username, oldEntryModel.password)
    }

//    private fun setNotSyncedNotification() {
////        filesUseCase.apply {
////            googleRecentFileModel = recentFileModel as GoogleRecentFileModel
////            resultEvents = object : FilesUseCase.ResultEvents {
////                override fun onSuccess() {
////
////                    mView.hideNotifications()
////
////                }
////
////                override fun onErrorLogIn() {
////
////                }
////
////                override fun onErrorUpdating() {
////
////                }
////
////            }
////        }
////        mView.setNotSyncedNotification(object : InsetNotification.NotSyncedEvents {
////            override fun onCloseClicked() {
////                mView.hideNotifications()
////            }
////
////            override fun onReplaceByGoogleClicked() {
////                filesUseCase.updateGoogleCacheFromTmpCache(googleDriveService)
////
////            }
////
////            override fun onReplaceByLocalClicked() {
////                CoroutineScope(Dispatchers.Main).launch {
////                    filesUseCase.updateGoogleDriveFileFromGoogleCacheFile(googleDriveService)
////                }
////            }
////
////        })
//    }

    private fun buildEntryModelFromFields(label: String, url: String, username: String, password: String, comments: String): EntryModel {
        return EntryModel().apply {
            this.label = label
            this.url = url
            this.username = username
            this.password = password
            this.notes = comments
            if (icon != null) {
                this.iconData = icon!!.iconData
                this.customIconUUID = icon!!.uuid
            } else {
                this.iconData = IconHelper.icons[0].iconData
                this.customIconUUID = IconHelper.icons[0].uuid
            }
            if (::oldEntryModel.isInitialized) {
                this.realIndex = oldEntryModel.realIndex
                this.isFavourite = oldEntryModel.isFavourite
            } else {
                this.isFavourite = false
            }
        }
    }


    private fun saveChanges() {
        val newEntryModel = buildEntryModelFromFields(view.getLabel(), view.getUrl(), view.getUsername(), view.getPassword(), view.getComments())
        CoroutineScope(Dispatchers.Default).launch {
            try {
                entryUseCase.saveEntry(oldEntryModel, newEntryModel, parentPath)
            } catch (e: Exception) {
                view.showError(e)
                return@launch
            }
            if (settingsUseCase.getGoogleSyncEnabled())
                filesUseCase.writeToFile(recentFileModel)
            oldEntryModel = newEntryModel
        }
    }

    private fun addEntry() {
        val newEntryModel = buildEntryModelFromFields(view.getLabel(), view.getUrl(), view.getUsername(), view.getPassword(), view.getComments())
        CoroutineScope(Dispatchers.Default).launch {
            try {
                entryUseCase.addEntry(newEntryModel, parentPath)
            } catch (e: Exception) {
                view.showError(e)
                return@launch
            }
            if (settingsUseCase.getGoogleSyncEnabled())
                filesUseCase.writeToFile(recentFileModel)
            oldEntryModel = newEntryModel
        }
    }


    override fun iconClicked() {
        if (mode == Mode.EDIT || mode == Mode.ADD)
            view.expandBottomSheet()
    }

    override fun iconPicked(iconDto: IconDto) {
        icon = iconDto
        view.setIcon(iconDto.iconData!!)
        view.hideBottomSheet()
    }

    override fun cancelButtonSheetClicked() {
        view.hideBottomSheet()
    }

    override fun backPressed() {
        onCloseClicked()
    }
}