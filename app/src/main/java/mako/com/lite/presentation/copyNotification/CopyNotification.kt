package mako.com.lite.presentation.copyNotification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mako.com.lite.R
import javax.inject.Inject


class CopyNotification @Inject constructor(private val context: Context) : CopyNotificationInterface {
    override fun buildCopyNotification(label: String, url: String?, login: String?, password: String?) {
        val builder = NotificationCompat.Builder(context, "copy")
                .setSmallIcon(R.drawable.copy)
                .setContentTitle(context.getString(R.string.copy_notification_header))
                .setContentText(context.getString(R.string.copy_notification_text))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        if (url != null)
            builder.addAction(R.drawable.copy, context.getString(R.string.copy_url),
                    getPendingIntent(url, 0))
        if (login != null)
            builder.addAction(R.drawable.copy, context.getString(R.string.copy_login),
                    getPendingIntent(login, 1))

        if (password != null)
            builder.addAction(R.drawable.copy, context.getString(R.string.copy_password),
                    getPendingIntent(password, 2))
        val nm = NotificationManagerCompat.from(context)
        nm.notify(0, builder.build())

        CoroutineScope(Dispatchers.Main).launch {
            delay(2 * 1000 * 60)
            nm.cancel(0)
        }
    }

    private fun getPendingIntent(value: String, requestCode: Int): PendingIntent {
        val intent = Intent(context, BroadcastCopyReceiver::class.java).apply {

            putExtra("value", value)
        }
        return PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }
}