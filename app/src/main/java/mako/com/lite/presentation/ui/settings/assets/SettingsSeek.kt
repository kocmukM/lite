package mako.com.lite.presentation.ui.settings.assets

class SettingsSeek(val value: Int, val listener: SeekListener, val minValue: Int?, val maxValue: Int) : SettingsBase() {
    interface SeekListener {
        fun onSeekStateChanged(newValue: Int)
    }
}