package mako.com.lite.presentation.ui.list

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import mako.com.lite.domain.*
import mako.com.lite.domain.model.*
import mako.com.lite.presentation.copyNotification.CopyNotificationInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.LogInListener
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import mako.com.lite.presentation.ui.views.notifications.InsetOfflineNotification
import java.util.*
import javax.inject.Inject

class PreviewListPresenter @Inject constructor(private val previewListUseCase: PreviewListUseCase,
                                               private val router: RouterInterface,
                                               val googleDriveService: GoogleDriveServiceInterface,
                                               private val fileUseCase: FilesUseCase,
                                               private val settingsUseCase: SettingsUseCase,
                                               private val copyNotification: CopyNotificationInterface)
    : BasePresenter<PreviewListInterface.ListView>(),
        PreviewListInterface.ListPresenter {

    private enum class SearchStatus(var isSearching: Boolean) {
        NOT_SEARCHING(false),
        FAST_SEARCHING(true),
        GLOBAL_SEARCHING(true)
    }

    private var notificationCount = 0

    private var addFabExtended = false

    private var searchStatus = SearchStatus.NOT_SEARCHING
    private lateinit var previewList: PreviewList
    private lateinit var recentFileModel: RecentFileModel

    override fun onResume() {
        if (settingsUseCase.needReLogin()) {
            router.showStartupScreen()
        }
    }

    override fun onPause() {
        settingsUseCase.saveLastSessionEnd()
    }

    override fun startWork(previewList: PreviewList, recentFileModel: RecentFileModel) {
        view.showToolbars()
        notificationCount = 0
        if (settingsUseCase.getGoogleSyncEnabled()) {
            googleDriveService.logInGoogle(object : LogInListener {
                override fun onLoggedInGoogle() {
                    continueWork(previewList, recentFileModel)
                }

                override fun onErrorLogIn(exception: Exception) {
                    view.showError(exception)
                    notificationCount++
                    view.setOfflineNotification(object : InsetOfflineNotification.OfflineEvents {
                        override fun onCloseClicked() {
                            view.hideNotificationAt(notificationCount)
                            notificationCount--
                        }

                        override fun onTryAgainClicked() {
                            startWork(previewList, recentFileModel)
                            view.hideNotificationAt(notificationCount)
                            notificationCount--
                        }

                    })
                    continueWork(previewList, recentFileModel)
                }

            })
        } else {
            continueWork(previewList, recentFileModel)
        }
    }

    private fun continueWork(previewList: PreviewList, recentFileModel: RecentFileModel) {
        this.previewList = previewList
        this.recentFileModel = recentFileModel
        previewListUseCase.currentGroupModelPath = previewList.currentGroupModelPath
        try {
            view.startSearchListener()
            view.hidePlaceholder()
            if (previewList.previewModels.size <= 1) {
                if (searchStatus.isSearching) {
                    view.setEmptySearchResultPlaceholder()
                } else {
                    view.setEmptyListPlaceholder()
                }
            }
            drawList(previewList)
            view.drawBreadCrumbs(previewListUseCase.getBreadcrumbs())
        } catch (npe: NullPointerException) {
            router.showStartupScreen()
        }
    }

    override fun copyClicked(previewModel: PreviewModel, position: Int) {
        previewListUseCase.openChild(previewModel, object : PreviewListEvents {
            override fun onGroupOpened(previewList: PreviewList) {

            }

            override fun onEntryOpened(entryModel: EntryModel) {
                copyNotification.buildCopyNotification(entryModel.label!!, entryModel.url, entryModel.username, entryModel.password)
                view.showCopiedToast()
                view.notifyListItemChanged(position)
            }

        })
    }

    override fun bigAddFabClicked() {
        if (!addFabExtended) {
            view.showAddOptions()
        } else {
            view.hideAddOptions()
        }
        addFabExtended = !addFabExtended
    }

    override fun addEntryClicked() {
        view.hideAddOptions()
        addFabExtended = !addFabExtended
        view.hideMainToolbar()
        view.hideToolbars(object : PreviewListInterface.ListView.OnHideEvents {
            override fun onHide() {
                router.showEntryScreen(null, previewList.currentGroupModelPath, recentFileModel)
            }

        })

    }

    override fun addGroupClicked() {
        view.hideAddOptions()
        addFabExtended = !addFabExtended
        view.showCreateGroupDialog()
    }


    override fun searchTextChanged(keyword: String) {
        CoroutineScope(Dispatchers.Main).launch {
            if (keyword == "") {
                searchStatus = SearchStatus.NOT_SEARCHING
                drawList(previewList)

                view.hideSearchButton()

                return@launch
            }
            if (searchStatus != SearchStatus.GLOBAL_SEARCHING) {
                searchStatus = SearchStatus.FAST_SEARCHING
                view.setGlobalSearchButton()
                view.showSearchButton()

                drawList(previewListUseCase.fastSearchBy(keyword).await())
            } else {
                drawList(globalSearch(keyword))
            }
        }
    }

    override fun doGlobalSearchClicked(keyword: String) {
        searchStatus = SearchStatus.GLOBAL_SEARCHING

        view.setBackFromSearchButton()
        view.showSearchButton()

        searchTextChanged(keyword)
    }

    override fun backFromSearchClicked() {
        searchStatus = SearchStatus.NOT_SEARCHING
        drawList(previewList)

        view.clearSearchText()
        view.hideSearchButton()

    }

    override fun onCreateGroupClicked(groupName: String) {
        val groupModel = GroupModel()
        groupModel.label = groupName
        groupModel.isFavourite = false
        CoroutineScope(Dispatchers.Main).launch {
            try {
                previewList = previewListUseCase.addGroup(groupModel)
                drawList(previewList)
                if (settingsUseCase.getGoogleSyncEnabled())
                    fileUseCase.writeToFile(recentFileModel)

            } catch (e: Exception) {
                view.showError(e)
            }
        }
    }

    override fun backPressed() {
        if (searchStatus == SearchStatus.NOT_SEARCHING) {
            previewListUseCase.goBack(object : BackStackEvents {
                override fun onSignOut() {
                    view.hideToolbars(object : PreviewListInterface.ListView.OnHideEvents {
                        override fun onHide() {
                            router.showSignInScreen(recentFileModel, fileUseCase.getFile(recentFileModel), false, true)

                        }

                    })
                }

                override fun onParentGroupOpened(previewList: PreviewList) {
                    router.showListScreen(previewList, recentFileModel, false, true)
                }

            })

        } else {
            backFromSearchClicked()
        }


    }


    private suspend fun globalSearch(keyword: String): PreviewList = previewListUseCase.globalSearchBy(keyword).await()

    private fun drawList(previewList: PreviewList) {

        view.drawPreviewList(previewList.previewModels, object : PreviewItemEvents {
            override fun editGroupNameEvent(newGroupName: String) {
                changeParentGroupName(newGroupName)
            }

            override fun openEvent(index: Int) {
                previewListUseCase.openChild(getPreviewModelByIndex(index), object : PreviewListEvents {
                    override fun onGroupOpened(previewList: PreviewList) {
                        router.showListScreen(previewList, recentFileModel, false, false)
                    }

                    override fun onEntryOpened(entryModel: EntryModel) {
                        view.hideMainToolbar()
                        view.hideToolbars(object : PreviewListInterface.ListView.OnHideEvents {
                            override fun onHide() {
                                router.showEntryScreen(entryModel, previewList.currentGroupModelPath, recentFileModel)
                            }

                        })
                    }

                })
            }

            override fun deleteClicked(index: Int) {
                startDeleting(getPreviewModelByIndex(index))
            }

            override fun addToFavouriteClicked(index: Int) {
                switchFavourite(getPreviewModelByIndex(index))
            }

        })

    }

    private fun getPreviewModelByIndex(index: Int): PreviewModel {
        return previewList.previewModels[index] as PreviewModel
    }

    private fun changeParentGroupName(newGroupName: String) {
        CoroutineScope(Dispatchers.Main).launch {
            val previewList = previewListUseCase.changeParentGroupName(newGroupName)
            if (settingsUseCase.getGoogleSyncEnabled())
                fileUseCase.writeToFile(recentFileModel)
            startWork(previewList, recentFileModel)
        }
    }

    private fun switchFavourite(previewModel: PreviewModel) {
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            previewList = previewListUseCase.switchFavourite(previewModel)
            drawList(previewList)
            if (settingsUseCase.getGoogleSyncEnabled())
                fileUseCase.writeToFile(recentFileModel)
        }
    }

    private fun startDeleting(previewModel: PreviewModel) {
        view.showDeleteDialog(object : PreviewListInterface.ListView.DeleteDialogEvents {
            override fun onDelete() {
                CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                    previewList = previewListUseCase.removeChild(previewModel)
                    drawList(previewList)
                    if (settingsUseCase.getGoogleSyncEnabled())
                        fileUseCase.writeToFile(recentFileModel)
                }
            }

        })
    }

    override fun onBreadCrumbClicked(breadCrumbModel: BreadcrumbModel) {
        previewListUseCase.openGroupAt(breadCrumbModel.path, object : PreviewListEvents {
            override fun onGroupOpened(previewList: PreviewList) {
                router.showListScreen(previewList, recentFileModel, false, true)
            }

            override fun onEntryOpened(entryModel: EntryModel) {
                throw RuntimeException()
            }

        })
    }

    override fun sortClicked() {
        view.showSortOptions(settingsUseCase.getSortByFavoriteValue(), settingsUseCase.getSortByNameValue())
    }

    override fun onSortOptionsSelected(byFavourite: Boolean, byName: Boolean) {
        val sortPropertyList = LinkedList<PreviewProperties>()
        settingsUseCase.saveBySortByVavorite(byFavourite)
        settingsUseCase.saveSortByName(byName)

        if (byFavourite)
            sortPropertyList.add(PreviewProperties.FAVOURITE_AT_FIRST)
        if (byName)
            sortPropertyList.add(PreviewProperties.SORT_BY_LABEL)
        drawList(previewListUseCase.setPreviewOrder(previewList, *(sortPropertyList.toTypedArray())))
    }
}