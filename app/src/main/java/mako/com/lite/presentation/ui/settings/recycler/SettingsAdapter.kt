package mako.com.lite.presentation.ui.settings.recycler

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mako.com.lite.R
import mako.com.lite.presentation.ui.settings.assets.SettingsBase
import mako.com.lite.presentation.ui.settings.assets.SettingsPick
import mako.com.lite.presentation.ui.settings.assets.SettingsSeek
import mako.com.lite.presentation.ui.settings.assets.SettingsSwitch
import mako.com.lite.presentation.ui.settings.recycler.viewHolders.BaseViewHolder
import mako.com.lite.presentation.ui.settings.recycler.viewHolders.PickerViewHolder
import mako.com.lite.presentation.ui.settings.recycler.viewHolders.SeekViewHolder
import mako.com.lite.presentation.ui.settings.recycler.viewHolders.SwitchViewHolder

class SettingsAdapter(private val data: List<SettingsBase>, private val activity: Activity) : RecyclerView.Adapter<BaseViewHolder>() {
    private companion object {
        const val PICK = 0
        const val SEEK = 1
        const val SWITCH = 2
        const val UNKNOWN = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            PICK -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_settings_picker, parent, false)
                PickerViewHolder(view, activity)
            }
            SEEK -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_settings_seek_bar, parent, false)
                SeekViewHolder(view)
            }
            SWITCH -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_settings_switch, parent, false)
                SwitchViewHolder(view)
            }
            else -> {
                throw RuntimeException("Unknown element")
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is SettingsPick -> PICK
            is SettingsSeek -> SEEK
            is SettingsSwitch -> SWITCH
            else -> UNKNOWN
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.performSetData(data[position])
    }
}