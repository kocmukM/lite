package mako.com.lite.presentation.ui.createFile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_create.*
import mako.com.lite.R
import mako.com.lite.presentation.ui.base.BaseFragment
import javax.inject.Inject

class CreateFileFragment : BaseFragment(), CreateFileInterface.CreateFileView {
    @Inject
    lateinit var presenter: CreateFilePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)

    }

    override fun disableGoogleRadio() {
        radio_google_image.visibility = View.INVISIBLE
        radio_google_source.visibility = View.GONE
    }

    override fun enableGoogleRadio() {
        radio_google_image.visibility = View.VISIBLE
        radio_google_source.visibility = View.VISIBLE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity?.hideBreadCrumbToolbar()
        mainActivity?.search_toolbar?.visibility = View.INVISIBLE
        mainActivity?.brand?.visibility = View.VISIBLE
        radio_google_source.setOnClickListener {
            presenter.googleDriveSourceSelected()
        }

        radio_local_source.setOnClickListener {
            presenter.localFileSourceSelected()
        }

        select_directory_button.setOnClickListener {
            presenter.pickFolderClicked()
        }

        create_button.setOnClickListener {
            presenter.createFileButtonClicked(new_file_name.text.toString(), new_password.text.toString(), confirm_password.text.toString())
        }
    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.googleDriveService.onActivityResult(requestCode, data)
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }
}