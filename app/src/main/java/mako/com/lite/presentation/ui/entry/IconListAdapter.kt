package mako.com.lite.presentation.ui.entry

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_icon.view.*
import mako.com.lite.R
import mako.com.lite.data.helpers.icons.IconHelper
import mako.com.lite.data.repository.dto.IconDto


class IconListAdapter(private val events: IconPickerEvents) : RecyclerView.Adapter<IconListAdapter.ViewHolder>() {
    lateinit var context: Context
    val iconList = IconHelper.icons
    var itemWidth: Float? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_icon, parent, false)
        context = v.context
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        itemWidth = width.toFloat() / 4
        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.iconButton.layoutParams = LinearLayout.LayoutParams(itemWidth!!.toInt(), LinearLayout.LayoutParams.WRAP_CONTENT)
        val iconDto = iconList[position]
        val bmp = BitmapFactory.decodeByteArray(iconDto.iconData, 0, iconDto.iconData!!.size)
        holder.iconImage.setImageBitmap(bmp)
        holder.iconDescription.text = iconDto.description
        holder.iconButton.setOnClickListener {
            events.onPick(iconDto)
        }
    }

    override fun getItemCount(): Int {
        return iconList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iconImage = itemView.icon_image
        val iconButton = itemView.icon_button
        val iconDescription = itemView.icon_description

    }

    interface IconPickerEvents {
        fun onPick(iconDto: IconDto)
    }

}