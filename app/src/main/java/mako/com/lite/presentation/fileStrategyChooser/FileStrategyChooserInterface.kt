package mako.com.lite.presentation.fileStrategyChooser

interface FileStrategyChooserInterface {
    fun showChooser(chooseCallbacks: FileStrategyChooser.ChooserCallbacks)
}