package mako.com.lite.presentation.ui.blockedPermission

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_no_permissions.*
import mako.com.lite.R
import mako.com.lite.presentation.ui.base.BaseFragment
import javax.inject.Inject

class BlockedPermissionFragment : BaseFragment(), BlockedPermissionInterface.BlockedPermissionView {
    @Inject
    lateinit var presenter: BlockedPermissionPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_no_permissions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        open_permission_settings_button.setOnClickListener {
            presenter.openPermissionSettingsClicked()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)

    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun onBackPressed() {
        presenter.backPressed()
    }
}