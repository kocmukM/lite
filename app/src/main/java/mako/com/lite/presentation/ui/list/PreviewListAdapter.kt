package mako.com.lite.presentation.ui.list

import android.content.Context
import android.graphics.BitmapFactory
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import com.github.zawadz88.materialpopupmenu.popupMenu
import kotlinx.android.synthetic.main.item_preview_header_layout.view.*
import kotlinx.android.synthetic.main.item_preview_layout.view.*
import mako.com.lite.R
import mako.com.lite.domain.model.PrevieHeader
import mako.com.lite.domain.model.PreviewInterface
import mako.com.lite.domain.model.PreviewModel

private const val PREVIEW_HEADER = 0
private const val PREVIEW_ITEM = 1

class PreviewListAdapter(private val previewList: MutableList<PreviewInterface>, private val events: PreviewItemEvents) : RecyclerView.Adapter<PreviewListAdapter.ViewHolder>() {


    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == PREVIEW_ITEM) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_preview_layout, parent, false)
            context = v.context
            return Item(v)
        }
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_preview_header_layout, parent, false)
        context = v.context
        return Header(v)

    }

    override fun getItemViewType(position: Int): Int {
        if (previewList[position] is PreviewModel)
            return PREVIEW_ITEM
        return PREVIEW_HEADER
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is Header) {
            holder.headerText.setText((previewList[position] as PrevieHeader).groupName)
            setEditable(holder.headerText, false)
            if (!(previewList.get(position) as PrevieHeader).isEditable) {
                holder.editGroupNameButton.visibility = View.GONE
            } else {
                holder.editGroupNameButton.setOnClickListener {
                    doEditHeaderJob(holder.headerText, holder.editGroupNameButton)
                }
            }
        } else if (holder is Item) {
            holder.type = (previewList[position] as PreviewModel).instance
            val previewModel = (previewList[position] as PreviewModel)
            holder.icon.visibility = View.GONE
            if (previewModel.iconData != null) {
                val bmp = BitmapFactory.decodeByteArray(previewModel.iconData, 0, previewModel.iconData!!.size)
                holder.icon.setImageBitmap(bmp)
                holder.icon.visibility = View.VISIBLE
            }
            holder.title.text = previewModel.title
            holder.subtitle.visibility = View.GONE
            if (previewModel.subtitle != "") {
                holder.subtitle.text = previewModel.subtitle
                holder.subtitle.visibility = View.VISIBLE
            }
            holder.favorite.visibility = View.GONE
            if (previewModel.isFavourite)
                holder.favorite.visibility = View.VISIBLE
            holder.menuButton.setOnClickListener {
                val popupMenu = popupMenu {
                    style = R.style.Widget_MPM_Menu_Dark_CustomBackgroundBottom
                    dropdownGravity = Gravity.END
                    section {
                        item {
                            label = context.getString(R.string.favourite_preview_model)
                            iconDrawable = context.getDrawable(R.drawable.star_outline)
                            callback = {
                                events.addToFavouriteClicked(position)

                            }
                        }
                        item {
                            label = context.getString(R.string.delete_preview_model)
                            iconDrawable = context.getDrawable(R.drawable.delete)
                            callback = {
                                events.deleteClicked(position)
                            }
                        }
                    }
                }
                popupMenu.show(context, holder.separator)
            }

            holder.container.setOnClickListener {
                events.openEvent(position)
            }
        }


    }

    override fun getItemCount(): Int {
        return previewList.size
    }

    class Item(itemView: View) : ViewHolder(itemView) {
        var type: Class<Any>? = null
        val separator = itemView.separator!!
        val container = itemView.container!!
        val title = itemView.title!!
        val subtitle = itemView.subtitle!!
        val icon = itemView.icon!!
        val favorite = itemView.favorite!!
        val menuButton = itemView.menu!!

    }

    class Header(itemView: View) : ViewHolder(itemView) {
        val headerText = itemView.header_text_view!!
        val editGroupNameButton = itemView.edit_group_name_button!!
    }

    abstract class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    private fun doEditHeaderJob(headerEditText: EditText, headerEditButton: ImageButton) {
        setEditable(headerEditText, true)
        headerEditText.requestFocus()
        headerEditText.callOnClick()
        headerEditButton.background = context.getDrawable(R.drawable.done)
        headerEditButton.setOnClickListener {
            events.editGroupNameEvent(headerEditText.text.toString())
            setEditable(headerEditText, false)
            headerEditButton.background = context.getDrawable(R.drawable.edit_black)
        }
    }

    private fun setEditable(editText: EditText, isClickable: Boolean) {
        editText.isClickable = isClickable
        editText.isFocusable = isClickable
        editText.isFocusableInTouchMode = isClickable
        editText.isClickable = isClickable
        editText.isLongClickable = isClickable
    }
}

interface PreviewItemEvents {
    fun editGroupNameEvent(newGroupName: String)
    fun openEvent(index: Int)
    fun deleteClicked(index: Int)
    fun addToFavouriteClicked(index: Int)
}