package mako.com.lite.presentation.vibro

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import kotlinx.coroutines.delay

class VibroHelper(activityContext: Context) : VibroHelperInterface {
    private val vibrator = activityContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

    @Suppress("DEPRECATION")
    override fun vibrate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(20, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(20)
        }
    }

    override suspend fun vibrateTwice() {
        vibrate()
        delay(70)
        vibrate()
    }
}