package mako.com.lite.presentation.copyNotification

interface CopyNotificationInterface {
    fun buildCopyNotification(label: String, url: String?, login: String?, password: String?)
}