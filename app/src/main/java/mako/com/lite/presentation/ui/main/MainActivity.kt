package mako.com.lite.presentation.ui.main

import android.Manifest
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.common.api.ApiException
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mako.com.lite.LiteApplication
import mako.com.lite.R
import mako.com.lite.presentation.ui.base.BaseActivity
import java.util.*
import javax.inject.Inject


class MainActivity : BaseActivity(), MainInterface.MainView {
    @Inject
    lateinit var mainPresenter: MainPresenter

    private var isErrorInProgress = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent.inject(this)
        mainPresenter.attachView(this)
        setContentView(R.layout.activity_main)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        error.y = height.toFloat()
        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_settings -> {
                    mainPresenter.settingsMenuSelected()
                }
                R.id.nav_about -> {
                    mainPresenter.aboutMenuSelected()
                }
            }
            drawer_layout.closeDrawers()
            hideBreadCrumbToolbar()
            true
        }
        menu_button.setOnClickListener {
            drawer_layout.openDrawer(nav_view)
        }

        val mDrawerToggle = object : ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.acc_drawer_open, R.string.acc_drawer_close) {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val moveFactor = nav_view.width * slideOffset

                fragment_container.translationX = moveFactor
                toolbar.translationX = moveFactor
                if (bread_crumbs_toolbar.visibility == View.VISIBLE)
                    bread_crumbs_toolbar.translationX = moveFactor

            }
        }
        drawer_layout.addDrawerListener(mDrawerToggle)
        drawer_layout.setScrimColor(Color.TRANSPARENT)
    }

    fun disableNavigationDrawer() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    fun enableNavigationDrawer() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    override fun showError(exception: Exception) {
        if (isErrorInProgress)
            return
        isErrorInProgress = true
        when (exception) {
            is de.slackspace.openkeepass.exception.KeePassDatabaseUnreadableException -> {
                error_message.text = getString(R.string.wrong_password_exception)
            }
            is LiteApplication.Exceptions.GroupWithoutLabelException -> {
                error_message.text = getString(R.string.group_without_label_exception)
            }
            is LiteApplication.Exceptions.UpdateRemoteFileException -> {
                error_message.text = getString(R.string.remote_file_update_exception)
            }
            is ApiException -> {
                error_message.text = getString(R.string.download_from_google_drive_exception)
            }
            is LiteApplication.Exceptions.WrongExtensionException -> {
                error_message.text = "${getString(R.string.wrong_extension_exception)} ${exception.extension}"
            }
            is LiteApplication.Exceptions.EmptyLabelException -> {
                error_message.text = getString(R.string.empty_label_exception)
            }
            is LiteApplication.Exceptions.EmptyPasswordException -> {
                error_message.text = getString(R.string.empty_password_exception)
            }
            is LiteApplication.Exceptions.EmptyFileNameException -> {
                error_message.text = getString(R.string.empty_file_name_error_message)
            }

            is LiteApplication.Exceptions.WrongConfirmationPasswordException -> {
                error_message.text = getString(R.string.wrong_confirmation_password_error_message)
            }
            is LiteApplication.Exceptions.ShortPasswordException -> {
                error_message.text = getString(R.string.short_password_error_message)
            }
            is LiteApplication.Exceptions.GoogleAuthorizationFailedException -> {
                error_message.text = getString(R.string.google_authorization_failed)
            }
            else -> {
                error_message.text = "${getString(R.string.unknown_exception)} ${exception::class}"
            }
        }
        error.animate().translationY(0f)
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            delay(3000)
            error.animate().translationY(error.height.toFloat())
//            error.visibility = View.INVISIBLE
            isErrorInProgress = false
        }
    }

    override fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showMainToolbar() {
        if (toolbar.visibility == View.INVISIBLE || toolbar.visibility == View.GONE) {
            toolbar.visibility = View.INVISIBLE
            toolbar.y = (-search_toolbar.height).toFloat()
            toolbar.visibility = View.VISIBLE
            toolbar.animate().translationY(0f)
        }
        brand.visibility = View.VISIBLE
    }

    fun showBreadcrumbToolbar() {
        if (bread_crumbs_toolbar.visibility == View.INVISIBLE || bread_crumbs_toolbar.visibility == View.GONE) {
            bread_crumbs_toolbar.visibility = View.INVISIBLE
            bread_crumbs_toolbar.y = (-bread_crumbs_toolbar.height).toFloat()
            bread_crumbs_toolbar.visibility = View.VISIBLE
            bread_crumbs_toolbar.animate().translationY(0f)
        }
    }

    fun hideMainToolbar() {
        if (toolbar.visibility == View.VISIBLE) {
            toolbar.animate().translationY(-toolbar.height.toFloat()).withEndAction {
                toolbar.visibility = View.GONE
            }
        }
        brand.visibility = View.INVISIBLE
    }

    fun hideBreadCrumbToolbar() {
        if (bread_crumbs_toolbar.visibility == View.VISIBLE) {
            bread_crumbs_toolbar.animate().translationY(-bread_crumbs_toolbar.height.toFloat() - search_toolbar.height.toFloat()).withEndAction {
                bread_crumbs_toolbar.visibility = View.GONE
            }
        }
    }

}

class RuntimePermissions @Inject constructor(private val permissionBackgroundActivity: Activity) {
    fun requestPermissions(permissionsEvents: PermissionsEvents) {
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        Permissions.check(permissionBackgroundActivity, permissions, null, null, object : PermissionHandler() {
            override fun onGranted() {
                permissionsEvents.onGranted()
            }

            override fun onBlocked(context: Context?, blockedList: ArrayList<String>?): Boolean {
                permissionsEvents.onBlocked()
                return false
            }

            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                permissionsEvents.onDenied()
            }

        })
    }


    interface PermissionsEvents {
        fun onGranted()
        fun onDenied()
        fun onBlocked()
    }
}