package mako.com.lite.presentation.filePickers.googleDriveFilePicker

import android.app.Activity
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.android.gms.drive.*
import mako.com.lite.LiteApplication
import java.util.*


class GoogleDriveService(private val activity: Activity) : GoogleDriveServiceInterface {
    companion object {
        private val SCOPES = setOf<Scope>(Drive.SCOPE_FILE, Drive.SCOPE_APPFOLDER)
        const val REQUEST_CODE_OPEN_ITEM = 100
        const val REQUEST_CODE_SIGN_IN = 101
    }

    private var logInListener: LogInListener? = null
    private var pickListener: PickListener? = null

    private var driveClient: DriveClient? = null
    private var signInAccount: GoogleSignInAccount? = null

    override fun logInGoogle(logInListener: LogInListener) {
        this.logInListener = logInListener
        val requiredScopes = HashSet<Scope>(2)
        requiredScopes.add(Drive.SCOPE_FILE)
        requiredScopes.add(Drive.SCOPE_APPFOLDER)
        signInAccount = GoogleSignIn.getLastSignedInAccount(activity)
        val containsScope = signInAccount?.grantedScopes?.containsAll(requiredScopes)
        val account = signInAccount
        if (account == null || containsScope != true) {
            activity.startActivityForResult(googleSignInClient.signInIntent, REQUEST_CODE_SIGN_IN)
        } else {
            initializeDriveClient(signInAccount!!)
        }
    }

    @Throws(Exception::class)
    override fun pickFiles(pickListener: PickListener) {
        this.pickListener = pickListener
        pickItem(OpenFileActivityOptions.Builder().build())
    }

    @Throws(Exception::class)
    override fun pickFolder(pickListener: PickListener) {
        this.pickListener = pickListener
        val mimeFolderType = LinkedList<String>()
        mimeFolderType.add(DriveFolder.MIME_TYPE)
        val openFileActivityOptionsBuilder = OpenFileActivityOptions.Builder()
                .setMimeType(mimeFolderType)
        pickItem(openFileActivityOptionsBuilder.build())
    }



    override fun onActivityResult(requestCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_SIGN_IN -> {
                if (data != null) {
                    handleSignIn(data)
                }
            }

            REQUEST_CODE_OPEN_ITEM -> {
                handleFilePick(data)
            }
        }
    }



    private fun handleFilePick(data: Intent?) {
        if (data != null) {
            val driveId = data.getParcelableExtra<DriveId>(OpenFileActivityOptions.EXTRA_RESPONSE_DRIVE_ID)
            pickListener?.onPicked(driveId.encodeToString())
        }
    }

    private fun initializeDriveClient(signInAccount: GoogleSignInAccount) {
        driveClient = Drive.getDriveClient(activity.applicationContext, signInAccount)
        logInListener?.onLoggedInGoogle()
    }

    @Throws(Exception::class)
    private fun pickItem(openOptions: OpenFileActivityOptions) {
        val openTask = driveClient!!.newOpenFileActivityIntentSender(openOptions)
        openTask?.let {
            openTask.continueWith { task ->
                activity.startIntentSenderForResult(task.result, REQUEST_CODE_OPEN_ITEM, null, 0, 0, 0, null)
            }
        }
    }

    private val googleSignInClient: GoogleSignInClient by lazy {
        val builder = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        for (scope in SCOPES) {
            builder.requestScopes(scope)
        }
        val signInOptions = builder.build()
        GoogleSignIn.getClient(activity, signInOptions)
    }

    private fun handleSignIn(data: Intent) {
        val getAccountTask = GoogleSignIn.getSignedInAccountFromIntent(data)
        if (getAccountTask.isSuccessful) {
            initializeDriveClient(getAccountTask.result!!)
        } else {
            logInListener?.onErrorLogIn(LiteApplication.Exceptions.GoogleAuthorizationFailedException())
        }
    }
}

interface PickListener {
    fun onPicked(stringDriveId: String)
}

interface LogInListener {
    fun onLoggedInGoogle()
    fun onErrorLogIn(exception: Exception)
}


