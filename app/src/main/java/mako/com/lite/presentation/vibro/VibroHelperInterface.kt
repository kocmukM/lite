package mako.com.lite.presentation.vibro

interface VibroHelperInterface {
    fun vibrate()
    suspend fun vibrateTwice()
}