package mako.com.lite.presentation.filePickers.localFilePicker

interface FilePickerInterface {
    fun pickFile(pickFileListener: AndgasFilePicker.OnFilePickListener)
    fun pickFolder(pickFolderListener: AndgasFilePicker.OnFolderPickListener)
}