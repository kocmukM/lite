package mako.com.lite.presentation.ui.signin

import android.view.View
import kotlinx.coroutines.Deferred
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract
import java.io.File

interface SignInInterface {
    interface SignInView : ViewContract {
        fun setFileName(fileName: String)
        fun setFileDirectory(fileDirectory: String)
        fun setGoogleDriveSource()
        fun hideFingerprint()
        fun setWrongFingerprint()
        fun setCorrectFingerprint()
        fun animateOpenTask()
        fun animFailOpenTask()
        fun getSignInButton(): View
        fun startTransitionAnimation(): Deferred<Unit>
    }

    interface SignInPresenter : Presenter<SignInView> {
        fun startWork(recentFileModel: RecentFileModel, kdbxFile: File)
        fun anotherActionButtonClicked()
        fun signInButtonClicked(password: String)

    }
}