package mako.com.lite.presentation.ui.settings.recycler.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_settings_switch.view.*
import mako.com.lite.presentation.ui.settings.assets.SettingsBase

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val context = itemView.context!!
    open fun performSetData(settingsBase: SettingsBase) {
        itemView.settings_title.text = settingsBase.settingsTitle
        itemView.settings_subtitle.text = settingsBase.settingsSubtitle
    }
}