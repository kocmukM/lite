package mako.com.lite.presentation.ui.settings.assets

class SettingsSwitch(val value: Boolean, val listener: SwitchListener) : SettingsBase() {
    interface SwitchListener {
        fun onSwitchStateChanged(newState: Boolean)
    }
}