package mako.com.lite.presentation.ui.entry

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_entry.*
import kotlinx.android.synthetic.main.menu_bottom_sheet_image_picker.*
import mako.com.lite.R
import mako.com.lite.data.repository.dto.IconDto
import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveService
import mako.com.lite.presentation.ui.base.BaseFragment
import mako.com.lite.presentation.ui.views.notifications.InsetOfflineNotification
import javax.inject.Inject


class EntryFragment : BaseFragment(), EntryInterface.ViewInterface {

    @Inject
    lateinit var presenter: EntryPresenter
    lateinit var googleDriveService: GoogleDriveService

    lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_entry, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val arguments = arguments!!
        val recentFileModel = arguments.getSerializable("recent_file_model") as RecentFileModel
        var entryModel: EntryModel? = null
        if(arguments.containsKey("entry_model"))
            entryModel = arguments.getSerializable("entry_model") as EntryModel
        val parentPath = arguments.getString("parent_path")!!
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_container)

        cancel_bottom_sheet_button.setOnClickListener {
            presenter.cancelButtonSheetClicked()
        }
        presenter.startWork(entryModel, parentPath, recentFileModel)

        close.setOnClickListener {
            presenter.onCloseClicked()
        }

        edit_save.setOnClickListener {
            presenter.onEditSaveClicked()
        }

        icon.setOnClickListener {
            presenter.iconClicked()
        }
    }

    override fun expandBottomSheet() {
        val recyclerView = icon_recycler
        val recentFileAdapter = IconListAdapter(object : IconListAdapter.IconPickerEvents {
            override fun onPick(iconDto: IconDto) {
                presenter.iconPicked(iconDto)
            }

        })
        val viewManager: RecyclerView.LayoutManager
        viewManager = GridLayoutManager(activityContext, 4)
        recyclerView.apply {
            this!!.layoutManager = viewManager
            adapter = recentFileAdapter
        }
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun hideBottomSheet() {
        bottomSheetBehavior.isHideable = true
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.googleDriveService.onActivityResult(requestCode, data)
    }

//    override fun setNotSyncedNotification(notSyncedEvents: InsetNotification.NotSyncedEvents) {
//        val notificatView = InsetNotification(activityContext).showNotSynced(notSyncedEvents)
//        notifications_container.addView(notificatView)
//    }
//
//    override fun hideNotifications() {
//        notifications_container.removeAllViews()
//    }

    override fun setIcon(iconBytes: ByteArray) {
        val bmp = BitmapFactory.decodeByteArray(iconBytes, 0, iconBytes.size)
        icon.setImageBitmap(bmp)
    }

    override fun setLabel(label: String) {
        entry_label.setText(label)
    }

    override fun setUrl(url: String) {
        entry_url.setText(url)
    }

    override fun setUsername(username: String) {
        entry_username.setText(username)
    }

    override fun setPassword(password: String) {
        entry_password.setText(password)
    }

    override fun setComments(comments: String) {
        entry_comments.setText(comments)
    }

    override fun getLabel(): String {
        return entry_label.text.toString()
    }

    override fun getUrl(): String {
        return entry_url.text.toString()
    }

    override fun getUsername(): String {
        return entry_username.text.toString()
    }

    override fun getPassword(): String {
        return entry_password.text.toString()
    }

    override fun getComments(): String {
        return entry_comments.text.toString()
    }

    override fun setReadMode() {
        entry_copy_button.visibility = View.VISIBLE
        entry_copy_button.setOnClickListener {
            presenter.copyButtonClicked()
        }
        edit_save.setImageDrawable(activityContext.getDrawable(R.drawable.edit))
        val textViews = arrayOf<com.google.android.material.textfield.TextInputEditText>(entry_label, entry_url, entry_username, entry_password, entry_comments)
        for (textView in textViews) {
            textView.isClickable = false
            textView.isFocusable = false
            textView.isFocusableInTouchMode = false
            textView.isClickable = false
            textView.isLongClickable = false
        }
    }

    override fun setWriteMode() {
        entry_copy_button.visibility = View.GONE
        edit_save.setImageDrawable(activityContext.getDrawable(R.drawable.save))
        val textViews = arrayOf<com.google.android.material.textfield.TextInputEditText>(entry_label, entry_url, entry_username, entry_password, entry_comments)
        for (textView in textViews) {
            textView.isClickable = true
            textView.isFocusable = true
            textView.isFocusableInTouchMode = true
            textView.isClickable = true
            textView.isLongClickable = true
        }
    }

    override fun setOfflineNotification(offlineEvents: InsetOfflineNotification.OfflineEvents) {
        val notificatView = InsetOfflineNotification(activityContext).showNotification(offlineEvents)
        notifications_entry_container.addView(notificatView)
    }

    override fun hideNotificationAt(index: Int) {
        notifications_entry_container.removeViewAt(index)
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onStop()
        presenter.onPause()
    }

}

