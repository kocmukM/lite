package mako.com.lite.presentation.fileStrategyChooser

import android.app.Activity
import android.view.View
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.popup_choose_file_strategy.view.*
import mako.com.lite.R

class FileStrategyChooser(private val activity: Activity) : FileStrategyChooserInterface {
    enum class FileStrategy {
        REPLACE_BY_GOOGLE,
        REPLACE_BY_LOCAL,
        OPEN_LOCAL
    }

    private var fileStrategy: FileStrategy = FileStrategy.REPLACE_BY_GOOGLE
    private lateinit var dialogView: View

    override fun showChooser(chooseCallbacks: ChooserCallbacks) {
        val builder = AlertDialog.Builder(activity)
        dialogView = activity.layoutInflater.inflate(R.layout.popup_choose_file_strategy, null)
        builder.setView(dialogView)
        lateinit var dialog: AlertDialog
        dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.show()

        dialogView.replace_by_google.setOnClickListener {
            unselectAll()
            dialogView.replace_by_google.background = activity.getDrawable(R.drawable.pressed_state)
            fileStrategy = FileStrategy.REPLACE_BY_GOOGLE
        }

        dialogView.replace_by_local.setOnClickListener {
            unselectAll()
            dialogView.replace_by_local.background = activity.getDrawable(R.drawable.pressed_state)
            fileStrategy = FileStrategy.REPLACE_BY_LOCAL
        }

        dialogView.open_local.setOnClickListener {
            unselectAll()
            dialogView.open_local.background = activity.getDrawable(R.drawable.pressed_state)
            fileStrategy = FileStrategy.OPEN_LOCAL
        }

        dialogView.apply_selected.setOnClickListener {
            onApplyClicked(chooseCallbacks)
            dialog.dismiss()
        }


    }

    private fun onApplyClicked(chooseCallbacks: ChooserCallbacks) {
        when (fileStrategy) {
            FileStrategy.REPLACE_BY_GOOGLE -> {
                chooseCallbacks.onReplaceByGoogleClicked()
            }
            FileStrategy.REPLACE_BY_LOCAL -> {
                chooseCallbacks.onReplaceByLocalClicked()
            }
            FileStrategy.OPEN_LOCAL -> {
                chooseCallbacks.onOpenLocalClicked()
            }
        }
    }

    private fun unselectAll() {
        dialogView.replace_by_google.background = null
        dialogView.replace_by_local.background = null
        dialogView.open_local.background = null
    }

    interface ChooserCallbacks {
        fun onReplaceByGoogleClicked()
        fun onReplaceByLocalClicked()
        fun onOpenLocalClicked()
    }
}