package mako.com.lite.presentation.ui.views.notifications

import android.content.Context
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.notification_not_synced.view.*
import mako.com.lite.R


class InsetNotSyncedNotification(private val parentContext: Context) : RelativeLayout(parentContext) {

    val view = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.notification_not_synced, this)


    fun showNotSynced(notSyncedEvents: NotSyncedEvents): View {
        val replaceByGoogle = makeLinkSpan(parentContext.getText(R.string.replace_by_google), OnClickListener {
            notSyncedEvents.onReplaceByGoogleClicked()
        })

        val replaceByLocale = makeLinkSpan(parentContext.getText(R.string.replace_by_local), OnClickListener {
            notSyncedEvents.onReplaceByLocalClicked()
        })
        view.notification_text.text = parentContext.getText(R.string.not_synced_top_text)
        view.notification_text.append(" ")
        view.notification_text.append(replaceByGoogle)
        view.notification_text.append(" ")
        view.notification_text.append(parentContext.getText(R.string.not_synced_between_options_text))
        view.notification_text.append(" ")
        view.notification_text.append(replaceByLocale)
        view.notification_icon.setImageDrawable(parentContext.getDrawable(R.drawable.cloud_off))
        view.close_notification.setOnClickListener {
            notSyncedEvents.onCloseClicked()
        }
        makeLinksFocusable(view.notification_text)
        return view
    }

    interface NotSyncedEvents {
        fun onCloseClicked()
        fun onReplaceByGoogleClicked()
        fun onReplaceByLocalClicked()
    }

    private fun makeLinkSpan(text: CharSequence, listener: View.OnClickListener): SpannableString {
        val link = SpannableString(text)
        link.setSpan(ClickableString(listener), 0, text.length,
                SpannableString.SPAN_INCLUSIVE_EXCLUSIVE)
        return link
    }

    private fun makeLinksFocusable(tv: TextView) {
        val m = tv.movementMethod
        if (m == null || m !is LinkMovementMethod) {
            if (tv.linksClickable) {
                tv.movementMethod = LinkMovementMethod.getInstance()
                tv.setTextColor(parentContext.resources.getColor(R.color.colorPrimary))
            }
        }
    }

    private class ClickableString(private val mListener: View.OnClickListener) : ClickableSpan() {
        override fun onClick(v: View) {
            mListener.onClick(v)
        }
    }

}