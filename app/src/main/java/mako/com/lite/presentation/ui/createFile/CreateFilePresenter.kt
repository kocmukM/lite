package mako.com.lite.presentation.ui.createFile

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import mako.com.lite.LiteApplication
import mako.com.lite.domain.FilesUseCase
import mako.com.lite.domain.PreviewListUseCase
import mako.com.lite.domain.SettingsUseCase
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.LogInListener
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.PickListener
import mako.com.lite.presentation.filePickers.localFilePicker.AndgasFilePicker
import mako.com.lite.presentation.filePickers.localFilePicker.FilePickerInterface
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import java.io.File
import javax.inject.Inject

class CreateFilePresenter @Inject constructor(private val localFilePicker: FilePickerInterface,
                                              val googleDriveService: GoogleDriveServiceInterface,
                                              val previewListUseCase: PreviewListUseCase,
                                              private val router: RouterInterface,
                                              private val filesUseCase: FilesUseCase,
                                              private val settingsUseCase: SettingsUseCase) :
        BasePresenter<CreateFileInterface.CreateFileView>(), CreateFileInterface.CreateFilePresenter {

    enum class SourceType {
        LOCAL,
        GOOGLE_DRIVE
    }

    override fun onResume() {
        if (!settingsUseCase.getGoogleSyncEnabled())
            view.disableGoogleRadio()
    }

    var sourceType = SourceType.LOCAL
    var googleStringFolderDriveId: String? = null
    var recentFileModel: RecentFileModel = LocalRecentFileModel("", "", "", 0)

    override fun googleDriveSourceSelected() {
        sourceType = SourceType.GOOGLE_DRIVE
        recentFileModel = GoogleRecentFileModel("", recentFileModel.fileName, "", recentFileModel.filePassword, recentFileModel.fileOpenTime)
    }

    override fun localFileSourceSelected() {
        sourceType = SourceType.LOCAL
        recentFileModel = LocalRecentFileModel("", recentFileModel.fileName, recentFileModel.filePassword, recentFileModel.fileOpenTime)
    }

    override fun pickFolderClicked() {
        when (sourceType) {
            SourceType.LOCAL -> {
                localFilePicker.pickFolder(object : AndgasFilePicker.OnFolderPickListener {
                    override fun onPick(directoryPath: String) {
                        recentFileModel.fileDirectory = directoryPath
                    }

                })
            }

            SourceType.GOOGLE_DRIVE -> {
                logInGoogleAndPickFolder()
            }
        }
    }

    private fun logInGoogleAndPickFolder() {
        googleDriveService.logInGoogle(object : LogInListener {
            override fun onLoggedInGoogle() {
                try {
                    pickFolder()
                } catch (e: Exception) {
                    view.showError(e)
                }
            }

            override fun onErrorLogIn(exception: Exception) {
                view.showError(exception)
                view.disableGoogleRadio()
            }

        })
    }

    @Throws(Exception::class)
    private fun pickFolder() {
        googleDriveService.pickFolder(object : PickListener {
            override fun onPicked(stringDriveId: String) {
                googleStringFolderDriveId = stringDriveId
            }

        })
    }

    override fun createFileButtonClicked(fileName: String, filePassword: String, fileConfirmPassword: String) {
        try {
            if (fileName == "") throw LiteApplication.Exceptions.EmptyFileNameException()
            if (filePassword.length < 3) throw LiteApplication.Exceptions.ShortPasswordException(3)
            if (filePassword != fileConfirmPassword) throw LiteApplication.Exceptions.WrongConfirmationPasswordException()
            recentFileModel.fileName = "$fileName.kdbx"
            recentFileModel.filePassword = filePassword
            filesUseCase.createFile(recentFileModel, object : FilesUseCase.CreateFileEvents {
                override fun onCreated(recentFileModel: RecentFileModel, kdbxFile: File) {
                    CoroutineScope(Dispatchers.Main).launch {
                        val previewList = previewListUseCase.openFile(recentFileModel, kdbxFile, recentFileModel.filePassword)
                        router.showListScreen(previewList, recentFileModel, false, false)
                    }
                }

                override fun onError(exception: Exception) {
                    view.showError(exception)
                }

            }, googleStringFolderDriveId)
        } catch (e: Exception) {
            view.showError(e)
        }
    }

    override fun backPressed() {
        router.goBack()
    }
}