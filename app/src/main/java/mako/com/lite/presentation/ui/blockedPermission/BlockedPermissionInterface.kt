package mako.com.lite.presentation.ui.blockedPermission

import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract

interface BlockedPermissionInterface {
    interface BlockedPermissionView : ViewContract {

    }

    interface BlockedPermissionPresenter : Presenter<BlockedPermissionView> {
        fun openPermissionSettingsClicked()
    }
}