//package mako.com.lite.presentation.ui.views.progressButtons
//
//import android.animation.*
//import android.content.Context
//import android.graphics.*
//import android.util.AttributeSet
//import android.view.View
//import android.view.animation.AccelerateDecelerateInterpolator
//import android.view.animation.AnimationUtils
//import androidx.appcompat.widget.AppCompatButton
//import androidx.core.content.ContextCompat
//import androidx.core.view.ViewCompat
//import mako.com.lite.R
//
//
//private const val cornerRadius = 100
//
//enum class ButtonState {
//    BUTTON,
//    MORPHING,
//    PROGRESS,
//    INPROGRESS
//}
//
//class MorphingButton(context: Context, attributeSet: AttributeSet) : AppCompatButton(context, attributeSet) {
//    var state = ButtonState.BUTTON
//    private val mGradientDrawable = ContextCompat.getDrawable(context, R.drawable.sign_in_button);
//    init {
//        background = mGradientDrawable
//
//    }
//
//
//
//    var buttonWidth = 0
//    var progressWidth = 0
//
//    fun startToProgressAnimation() {
//        if(state == ButtonState.PROGRESS || state == ButtonState.MORPHING)
//            return
//        state = ButtonState.MORPHING
//        buttonWidth = width
//        progressWidth = width / 6
//        this.text = null
//        isClickable = false
//
//        val cornerAnimation = ObjectAnimator.ofFloat(mGradientDrawable,
//                "cornerRadius",
//                cornerRadius.toFloat(),
//                cornerRadius.toFloat())
//
//        val widthAnimation = ValueAnimator.ofInt(buttonWidth, progressWidth)
//        widthAnimation.addUpdateListener { valueAnimator ->
//            val valAnim = valueAnimator.animatedValue as Int
//            val layoutParams = layoutParams
//            layoutParams.width = valAnim
//            setLayoutParams(layoutParams)
//        }
//
//        val mMorphingAnimatorSet = AnimatorSet()
//        mMorphingAnimatorSet.duration = 100
//        mMorphingAnimatorSet.playTogether(cornerAnimation, widthAnimation)
//        mMorphingAnimatorSet.addListener(object : AnimatorListenerAdapter() {
//            override fun onAnimationEnd(animation: Animator) {
//                state = ButtonState.PROGRESS
////                mIsMorphingInProgress = false
//            }
//        })
//        mMorphingAnimatorSet.start()
//    }
//
//    fun startToButtonAnimation() {
//        if (state == ButtonState.BUTTON || state == ButtonState.MORPHING)
//            return
//        state = ButtonState.MORPHING
//        this.text = context.getText(R.string.open_file_text_button)
//        isClickable = true
//
//        val cornerAnimation = ObjectAnimator.ofFloat(mGradientDrawable,
//                "cornerRadius",
//                cornerRadius.toFloat(),
//                cornerRadius.toFloat())
//
//        val widthAnimation = ValueAnimator.ofInt(progressWidth, buttonWidth)
//        widthAnimation.addUpdateListener { valueAnimator ->
//            val valAnim = valueAnimator.animatedValue as Int
//            val layoutParams = layoutParams
//            layoutParams.width = valAnim
//            setLayoutParams(layoutParams)
//        }
//
//        val mMorphingAnimatorSet = AnimatorSet()
//        mMorphingAnimatorSet.duration = 100
//        mMorphingAnimatorSet.playTogether(cornerAnimation, widthAnimation)
//        mMorphingAnimatorSet.addListener(object : AnimatorListenerAdapter() {
//            override fun onAnimationEnd(animation: Animator) {
//                state = ButtonState.BUTTON
////                mIsMorphingInProgress = false
//            }
//        })
//        mMorphingAnimatorSet.start()
//    }
//
//    override fun onDraw(canvas: Canvas?) {
//        super.onDraw(canvas)
//        if(state == ButtonState.PROGRESS  && state != ButtonState.MORPHING) {
//            if(progressBar == null) {
//                progressBar = mako.com.lite.presentation.ui.views.progressButtons.ProgressBar(this)
//                setupProgressBarBounds()
//                progressBar!!.setColorScheme(Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);
//                progressBar!!.start();
//            }
//            progressBar!!.draw(canvas!!);
//
//        }
//    }
//}
//
//
//
//
