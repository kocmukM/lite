package mako.com.lite.presentation.ui.startup

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_brand.*
import mako.com.lite.R
import mako.com.lite.data.helpers.icons.IconHelper
import mako.com.lite.data.helpers.icons.IconRepositoryCallbacks
import mako.com.lite.presentation.ui.base.BaseFragment
import javax.inject.Inject

class StartupFragment : BaseFragment(), StartupInterface.StartupView {
    @Inject
    lateinit var presenter: StartupPresenter

    enum class LoadingStatus{
        UPDATING_GOOGLE_FILE,
        PARSING_FILES,
        COMPARING_FILES,
        SOME_BUSINESS,
        LOADING_IMAGES
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.attachView(this)
        return inflater.inflate(R.layout.fragment_brand, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity?.hideBreadCrumbToolbar()
        mainActivity?.hideMainToolbar()
        presenter.startWork()
    }

    override suspend fun loadIcons(iconRepositoryCallbacks: IconRepositoryCallbacks) {
        IconHelper.loadIcons(activityContext, iconRepositoryCallbacks).await()
    }

    override fun setLoadingProgress(loadingProgress: Int) {
        if(start_progress == null)
            return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            start_progress.setProgress(loadingProgress, true)
        } else {
            start_progress.progress = loadingProgress
        }
    }

    override fun setLoadingProgressTest(status: LoadingStatus) {
        if(start_progress == null)
            return
        when (status) {
            LoadingStatus.UPDATING_GOOGLE_FILE -> {
                progress_status_text.text = activityContext.getText(R.string.downloading_from_google_text_progress)
            }

            LoadingStatus.PARSING_FILES -> {
                progress_status_text.text = activityContext.getText(R.string.parsing_files)
            }

            LoadingStatus.COMPARING_FILES -> {
                progress_status_text.text = activityContext.getText(R.string.comparing_files)
            }

            LoadingStatus.SOME_BUSINESS -> {
                progress_status_text.text = activityContext.getText(R.string.some_business)
            }
            LoadingStatus.LOADING_IMAGES -> {
                progress_status_text.text = activityContext.getText(R.string.loading_images_text_progress)
            }
        }
    }

    override fun showSkipLoadingButton() {
        skip_button.visibility = View.VISIBLE
        skip_button.setOnClickListener {
            presenter.onSkipClicked()
        }
    }

    override fun showToolbar() {
        mainActivity?.showMainToolbar()
    }


    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.googleDriveService.onActivityResult(requestCode, data)
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun hideSearchIfNeed() {
        activity!!.search_toolbar.visibility = View.INVISIBLE
    }
}