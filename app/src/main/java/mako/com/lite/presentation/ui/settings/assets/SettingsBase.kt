package mako.com.lite.presentation.ui.settings.assets

abstract class SettingsBase {
    var settingsTitle: String? = null
    var settingsSubtitle: String? = null
}