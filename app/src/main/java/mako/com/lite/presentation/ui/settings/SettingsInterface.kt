package mako.com.lite.presentation.ui.settings

import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract
import mako.com.lite.presentation.ui.settings.assets.SettingsBase

interface SettingsInterface {
    interface SettingsView : ViewContract {
        fun fillSettings(data: List<SettingsBase>)
        fun hideToolbar()
        fun showToolbar()
        fun disableDrawer()
        fun enableDrawer()

        fun getGoogleSyncTextTitle(): String
        fun getGoogleSyncTextSubtitle(): String

        fun getPasswordLengthTitle(): String
        fun getPasswordLengthSubtitle(): String

        fun getTimeoutPickTitle(): String
        fun getTimeoutPickSubtitle(): String

        fun getSortTitle(): String
        fun getSortSubtitle(): String
    }

    interface SettingsPresenter : Presenter<SettingsView> {
        fun startWork()
        fun closeClicked()
    }
}