package mako.com.lite.presentation.ui.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import mako.com.lite.LiteApplication
import mako.com.lite.di.components.DaggerConfigPersistentComponent
import mako.com.lite.di.components.FragmentComponent
import mako.com.lite.di.modules.FragmentModule
import mako.com.lite.presentation.ui.main.MainActivity


abstract class BaseFragment : Fragment(), ViewContract {

    lateinit var fragmentComponent: FragmentComponent
        private set

    lateinit var activityContext: Context
    var mainActivity: MainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val configPersistentComponent = DaggerConfigPersistentComponent.builder()
                    .applicationComponent(LiteApplication.getComponent())
                    .build()
        fragmentComponent = configPersistentComponent.fragmentComponent(FragmentModule(this))
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activityContext = context!!
        if (context is MainActivity)
            mainActivity = context
        (context as BaseActivity).attachFragment(this)
    }

    abstract fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    abstract fun onBackPressed()

    override fun showError(exception: Exception) {
        (activity!! as BaseActivity).showError(exception)
    }

    override fun hideKeyboard() {
        val imm = activityContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity!!.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}