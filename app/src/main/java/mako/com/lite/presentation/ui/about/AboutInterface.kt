package mako.com.lite.presentation.ui.about

import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract

interface AboutInterface {
    interface AboutView : ViewContract {
        fun hideToolbar()
        fun showToolbar()
        fun disableDrawer()
        fun enableDrawer()
    }

    interface AboutPresenter : Presenter<AboutView> {
        fun startWork()
        fun closeClicked()
    }
}