package mako.com.lite.presentation.ui.views.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.notification_offline.view.*
import mako.com.lite.R

class InsetOfflineNotification(parentContext: Context) : RelativeLayout(parentContext) {
    val view = (parentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.notification_offline, this)

    fun showNotification(events: OfflineEvents): View {
        view.close_notification.setOnClickListener {
            events.onCloseClicked()
        }
        view.notification_try_again.setOnClickListener {
            events.onTryAgainClicked()
        }
        return view
    }

    interface OfflineEvents {
        fun onCloseClicked()
        fun onTryAgainClicked()
    }
}