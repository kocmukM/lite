package mako.com.lite.presentation.ui.main

import mako.com.lite.presentation.ui.base.ViewContract

interface MainInterface {
    interface MainView: ViewContract
    interface MainPresenter : mako.com.lite.presentation.ui.base.Presenter<MainView> {
        fun settingsMenuSelected()
        fun aboutMenuSelected()
    }
}