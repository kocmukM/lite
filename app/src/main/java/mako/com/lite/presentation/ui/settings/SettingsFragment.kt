package mako.com.lite.presentation.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_settings.*
import mako.com.lite.R
import mako.com.lite.presentation.ui.base.BaseFragment
import mako.com.lite.presentation.ui.settings.assets.SettingsBase
import mako.com.lite.presentation.ui.settings.recycler.SettingsAdapter
import javax.inject.Inject

class SettingsFragment : BaseFragment(), SettingsInterface.SettingsView {
    @Inject
    lateinit var presenter: SettingsPresenter

    override fun hideToolbar() {
        mainActivity?.hideMainToolbar()
    }

    override fun showToolbar() {
        mainActivity?.showMainToolbar()
    }

    override fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun disableDrawer() {
        mainActivity?.disableNavigationDrawer()
    }

    override fun enableDrawer() {
        mainActivity?.enableNavigationDrawer()
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.startWork()
        close.setOnClickListener {
            presenter.closeClicked()
        }
    }

    override fun fillSettings(data: List<SettingsBase>) {
        settings_recycler.apply {
            adapter = SettingsAdapter(data, activity!!)
            layoutManager = LinearLayoutManager(activityContext)
        }
    }

    override fun getGoogleSyncTextSubtitle(): String {
        return activityContext.getString(R.string.google_sync_settings_subtitle)
    }

    override fun getGoogleSyncTextTitle(): String {
        return activityContext.getString(R.string.google_sync_settings_title)
    }

    override fun getPasswordLengthSubtitle(): String {
        return activityContext.getString(R.string.password_length_subtitle)
    }

    override fun getPasswordLengthTitle(): String {
        return activityContext.getString(R.string.password_length_title)
    }

    override fun getTimeoutPickSubtitle(): String {
        return activityContext.getString(R.string.timeout_subtitle)
    }

    override fun getTimeoutPickTitle(): String {
        return activityContext.getString(R.string.timeout_title)
    }

    override fun getSortSubtitle(): String {
        return activityContext.getString(R.string.sort_subtitle)
    }

    override fun getSortTitle(): String {
        return activityContext.getString(R.string.sort_title)
    }

}