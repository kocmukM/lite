package mako.com.lite.presentation.ui.start

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import mako.com.lite.domain.FilesUseCase
import mako.com.lite.domain.RecentFilesUseCase
import mako.com.lite.domain.SettingsUseCase
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.LogInListener
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.PickListener
import mako.com.lite.presentation.filePickers.localFilePicker.AndgasFilePicker
import mako.com.lite.presentation.filePickers.localFilePicker.FilePickerInterface
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooser
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooserInterface
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import java.io.File
import javax.inject.Inject

class StartPresenter @Inject constructor(private val recentFilesUseCase: RecentFilesUseCase,
                                         private val router: RouterInterface,
                                         private val filePicker: FilePickerInterface,
                                         val googleDriveService: GoogleDriveServiceInterface,
                                         private val filesUseCase: FilesUseCase,
                                         private val fileStrategyChooser: FileStrategyChooserInterface,
                                         private val settingsUseCase: SettingsUseCase) :
        BasePresenter<StartInterface.StartView>(), StartInterface.StartPresenter {

    override fun onPickFileButtonClicked() {
        if (settingsUseCase.getGoogleSyncEnabled()) {
            view.showOpenOptions()
        } else {
            onPickLocalFileButtonClicked()
        }
    }

    override fun onResume() {

    }

    override fun onPickLocalFileButtonClicked() {
        filePicker.pickFile(object : AndgasFilePicker.OnFilePickListener {
            override fun onPick(filePath: String) {

                val localRecentFileModel = recentFilesUseCase.getLocalFileModelFromPath(filePath)
                filesUseCase.openNew(localRecentFileModel, object : FilesUseCase.OpenNewListener {
                    override fun onOpen(recentFileModel: RecentFileModel, fileCache: File) {
                        router.showSignInScreen(recentFileModel, fileCache, true, false)
                    }

                    override fun onError(exception: Exception) {
                        view.showError(exception)
                    }
                })

            }

        })
    }
    override fun onPickGoogleFileButtonClicked() {
        try {
            startFilePicking()
        } catch (e: Exception) {
            view.showError(e)
        }

    }

    override fun onCreateFileButtonClicked() {
        router.showCreateFileScreen()
    }

    override fun startWork() {
        if (settingsUseCase.getGoogleSyncEnabled()) {
            googleDriveService.logInGoogle(object : LogInListener {
                override fun onLoggedInGoogle() {
                    workWithRecycler()
                }

                override fun onErrorLogIn(exception: Exception) {
                    //Offline мод
                    workWithRecycler()
                    view.showError(exception)
                }
            })
        }
        workWithRecycler()

    }

    private fun workWithRecycler() {
        CoroutineScope(Dispatchers.Main).launch {
            recentFilesUseCase.init()
            drawList()
        }
    }

    private fun drawList() {
        view.run {
            drawRecentFiles(recentFilesUseCase.getRecentFiles(), object : RecentFileAdapterEvents {
                override fun openRecentFileClicked(recentFileModel: RecentFileModel) {
                    openFile(recentFileModel)
                }

                override fun deleteRecentFileClicked(recentFileModel: RecentFileModel) {

                    view.showDeleteDialog(object : StartInterface.StartView.DeleteDialogEvents {
                        override fun onDelete() {
                            CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                                recentFilesUseCase.deleteRecentFileModel(recentFileModel)
                                drawList()
                            }
                        }

                    })

                }

                override fun forgetRecentFileClicked(recentFileModel: RecentFileModel) {
                    CoroutineScope(Dispatchers.Main).launch {
                        recentFilesUseCase.forgetRecentFileModel(recentFileModel)
                        drawList()
                    }
                }

            })
        }
    }


    private fun openFile(recentFileModel: RecentFileModel) {
        filesUseCase.getFile(settingsUseCase.getGoogleSyncEnabled(), recentFileModel,

                object : FilesUseCase.GetFileEvents {
                    override fun onFile(file: File) {
                        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                            router.showSignInScreen(recentFileModel, file, true, false)
                        }
                    }

                    override fun onError(e: Exception) {
                        view.showError(e)
                    }

                },

                object : FilesUseCase.OpenChooseListener {
                    override fun onRequestOpenChooser(tmpCacheFile: File, cacheFile: File) {
                        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
                            fileStrategyChooser.showChooser(object : FileStrategyChooser.ChooserCallbacks {
                                override fun onReplaceByGoogleClicked() {
                                    filesUseCase.updateLocalFile(recentFileModel, object : FilesUseCase.GetFileEvents {
                                        override fun onFile(file: File) {
                                            showSignIn(recentFileModel, file)
                                        }

                                        override fun onError(e: Exception) {
                                            view.showError(e)
                                        }

                                    }, tmpCacheFile)
                                }

                                override fun onReplaceByLocalClicked() {
                                    try {
                                        filesUseCase.updateRemoteFile(recentFileModel)
                                    } catch (e: Exception) {
                                        view.showError(e)
                                    }
                                    showSignIn(recentFileModel, cacheFile)
                                }

                                override fun onOpenLocalClicked() {
                                    filesUseCase.continueWithoutChanges(cacheFile, object : FilesUseCase.GetFileEvents {
                                        override fun onFile(file: File) {
                                            showSignIn(recentFileModel, file)
                                        }

                                        override fun onError(e: Exception) {
                                            view.showError(e)
                                        }

                                    })
                                }

                            })
                        }

                    }

                })
    }

    private fun showSignIn(recentFileModel: RecentFileModel, file: File) {
        CoroutineScope(kotlinx.coroutines.Dispatchers.Main).launch {
            router.showSignInScreen(recentFileModel, file, true, false)
        }
    }
    @Throws(Exception::class)
    private fun startFilePicking() {
        googleDriveService.pickFiles(object : PickListener {
            override fun onPicked(stringDriveId: String) {
                val googleRecentFileModel = GoogleRecentFileModel("", "", stringDriveId, "", 0L)

                filesUseCase.openNew(googleRecentFileModel, object : FilesUseCase.OpenNewListener {
                    override fun onOpen(recentFileModel: RecentFileModel, fileCache: File) {
                        showSignIn(recentFileModel, fileCache)
                    }

                    override fun onError(exception: Exception) {
                        view.showError(exception)
                    }

                })

            }

        })

    }

    override fun backPressed() {
        router.goBack()
    }

}