package mako.com.lite.presentation.ui.about

import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import javax.inject.Inject

class AboutPresenter @Inject constructor(private val router: RouterInterface) : BasePresenter<AboutInterface.AboutView>(), AboutInterface.AboutPresenter {
    override fun backPressed() {
        router.goBack()
        view.showToolbar()
        view.enableDrawer()
    }

    override fun closeClicked() {
        backPressed()
    }

    override fun startWork() {
        view.disableDrawer()
        view.hideToolbar()
    }
}