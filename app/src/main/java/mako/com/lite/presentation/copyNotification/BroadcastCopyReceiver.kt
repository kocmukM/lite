package mako.com.lite.presentation.copyNotification

import android.content.*
import android.widget.Toast
import mako.com.lite.R


class BroadcastCopyReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("text", intent!!.getStringExtra("value"))
        clipboard.primaryClip = clip
        Toast.makeText(context, R.string.copy_message, Toast.LENGTH_SHORT).show()
    }
}