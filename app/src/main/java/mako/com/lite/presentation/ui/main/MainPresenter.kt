package mako.com.lite.presentation.ui.main

import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import javax.inject.Inject

class MainPresenter
@Inject
constructor(private val router: RouterInterface)
    : BasePresenter<MainInterface.MainView>(), MainInterface.MainPresenter {
    init {
        router.showStartupScreen()
    }

    override fun backPressed() {

    }

    override fun settingsMenuSelected() {
        router.openSettings()
    }

    override fun aboutMenuSelected() {
        router.openAbout()
    }
}