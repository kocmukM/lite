package mako.com.lite.presentation.ui.createFile

import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract

interface CreateFileInterface {
    interface CreateFileView : ViewContract {
        fun disableGoogleRadio()
        fun enableGoogleRadio()
    }

    interface CreateFilePresenter : Presenter<CreateFileView> {
        fun onResume()
        fun googleDriveSourceSelected()
        fun localFileSourceSelected()
        fun pickFolderClicked()
        fun createFileButtonClicked(fileName: String, filePassword: String, fileConfirmPassword: String)
    }
}