package mako.com.lite.presentation.ui.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import mako.com.lite.LiteApplication
import mako.com.lite.di.components.ActivityComponent
import mako.com.lite.di.components.DaggerConfigPersistentComponent
import mako.com.lite.di.modules.ActivityModule

abstract class BaseActivity : AppCompatActivity(), ViewContract {
    lateinit var baseFragment: BaseFragment
    lateinit var activityComponent: ActivityComponent
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .applicationComponent(LiteApplication.getComponent())
                .build()
        activityComponent = configPersistentComponent.activityComponent(ActivityModule(this))

    }

    fun attachFragment(baseFragment: BaseFragment) {
        this.baseFragment = baseFragment
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        baseFragment.onParentActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        baseFragment.onBackPressed()
    }

    abstract override fun showError(exception: Exception)
}