package mako.com.lite.presentation.ui.base

import java.lang.ref.WeakReference

abstract class BasePresenter<T : ViewContract> : Presenter<T> {
    private lateinit var mvpView: WeakReference<T>

    override fun attachView(view: T) {
        this.mvpView = WeakReference(view)
    }

    override fun detachView() {
        mvpView.clear()
    }

    val view: T
        get() = mvpView.get()!!

    val isViewAttached: Boolean
        get() = mvpView.get() != null

}