package mako.com.lite.presentation.ui.settings

import mako.com.lite.domain.SettingsUseCase
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.base.BasePresenter
import mako.com.lite.presentation.ui.settings.assets.SettingsBase
import java.util.*
import javax.inject.Inject

class SettingsPresenter @Inject constructor(private val router: RouterInterface, private val settingsUseCase: SettingsUseCase) : BasePresenter<SettingsInterface.SettingsView>(), SettingsInterface.SettingsPresenter {


    override fun backPressed() {
        router.goBack()
        view.showToolbar()
        view.enableDrawer()
    }

    override fun closeClicked() {
        backPressed()
    }

    val settingsItems = LinkedList<SettingsBase>()

    override fun startWork() {
        view.disableDrawer()
        view.hideToolbar()
        addGoogleSync()
        addPasswordLimitSeek()
        addTimeoutTime()
        addSortOptions()
        view.fillSettings(settingsItems)
    }

    private fun addGoogleSync() {
        val switch = settingsUseCase.getGoogleSyncSettings()
        switch.settingsTitle = view.getGoogleSyncTextTitle()
        switch.settingsSubtitle = view.getGoogleSyncTextSubtitle()
        settingsItems.add(switch)
    }

    private fun addPasswordLimitSeek() {
        //Будет добавлено, как только появится необходимый функционал
//        val seek = settingsUseCase.getPasswordLimitSettings()
//        seek.settingsTitle = view.getPasswordLengthTitle()
//        seek.settingsSubtitle = view.getPasswordLengthSubtitle()
//        settingsItems.add(seek)
    }

    private fun addTimeoutTime() {
        val pick = settingsUseCase.getTimeoutSettingsObject()
        pick.settingsTitle = view.getTimeoutPickTitle()
        pick.settingsSubtitle = view.getTimeoutPickSubtitle()
        settingsItems.add(pick)
    }

    private fun addSortOptions() {
        val pick = settingsUseCase.getSortSettingsObject()
        pick.settingsTitle = view.getSortTitle()
        pick.settingsSubtitle = view.getSortSubtitle()
        settingsItems.add(pick)
    }
}