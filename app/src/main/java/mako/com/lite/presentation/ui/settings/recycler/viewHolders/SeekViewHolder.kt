package mako.com.lite.presentation.ui.settings.recycler.viewHolders

import android.view.View
import android.widget.SeekBar
import kotlinx.android.synthetic.main.item_settings_seek_bar.view.*
import mako.com.lite.presentation.ui.settings.assets.SettingsBase
import mako.com.lite.presentation.ui.settings.assets.SettingsSeek

class SeekViewHolder(itemView: View) : BaseViewHolder(itemView) {
    override fun performSetData(settingsBase: SettingsBase) {
        super.performSetData(settingsBase)
        val data = settingsBase as SettingsSeek
        itemView.settings_seekbar.max = data.maxValue
        val minValue = data.minValue ?: 0
        itemView.settings_seekbar.progress = data.value - minValue
        itemView.settings_seek_value.text = data.value.toString()
        itemView.settings_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            var currentProgressValue = minValue
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                currentProgressValue = progress + minValue
                itemView.settings_seek_value.text = currentProgressValue.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                data.listener.onSeekStateChanged(currentProgressValue)
            }

        })
    }
}