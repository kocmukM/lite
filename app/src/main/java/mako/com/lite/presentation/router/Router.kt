package mako.com.lite.presentation.router

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import mako.com.lite.R
import mako.com.lite.domain.model.EntryModel
import mako.com.lite.domain.model.PreviewList
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.presentation.ui.about.AboutFragment
import mako.com.lite.presentation.ui.blockedPermission.BlockedPermissionFragment
import mako.com.lite.presentation.ui.createFile.CreateFileFragment
import mako.com.lite.presentation.ui.entry.EntryFragment
import mako.com.lite.presentation.ui.list.PreviewListFragment
import mako.com.lite.presentation.ui.settings.SettingsFragment
import mako.com.lite.presentation.ui.signin.SignInFragment
import mako.com.lite.presentation.ui.start.StartFragment
import mako.com.lite.presentation.ui.startup.StartupFragment
import java.io.File


class Router: RouterInterface {
    lateinit var fragmentActivity: FragmentActivity
    constructor(fragmentActivity: AppCompatActivity) {
        this.fragmentActivity = fragmentActivity
    }
    constructor()

    override fun showStartupScreen() {
        val fragmentManager = fragmentActivity.supportFragmentManager
        for(i in 0 until fragmentManager.backStackEntryCount) {
            fragmentManager.popBackStack()
        }
        fragmentManager.beginTransaction().add(R.id.fragment_container, StartupFragment()).commit()

    }

    override fun showNoPermissionsScreen() {
        fragmentActivity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, BlockedPermissionFragment()).commit()
    }

    override fun showStartScreen(needBackStack: Boolean) {
        val transaction = fragmentActivity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, StartFragment())
        if (needBackStack)
            transaction.addToBackStack(null)
        transaction.commit()

    }

    override fun showSignInScreen(recentFileModel: RecentFileModel, kdbxFile: File, needBackStack: Boolean, reverseAnimation: Boolean) {
        val fragment = SignInFragment()
        fragment.arguments = Bundle().apply {
            putSerializable("file_model", recentFileModel)
            putSerializable("file", kdbxFile)
        }
        val transaction = fragmentActivity.supportFragmentManager.beginTransaction()
        if (reverseAnimation) {
            transaction.setCustomAnimations(R.anim.in_reverse_animation, R.anim.out_reverse_animation)
        } else {
            transaction.setCustomAnimations(R.anim.in_animation, R.anim.out_animation)
        }
        transaction.replace(R.id.fragment_container, fragment)
        if (needBackStack)
            transaction.addToBackStack(null)
        transaction.commit()

    }

    override fun showCreateFileScreen() {
        fragmentActivity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, CreateFileFragment()).addToBackStack(null).commit()
    }

    override fun showListScreen(previewList: PreviewList, recentFileModel: RecentFileModel, needBackStack: Boolean, reverseAnimation: Boolean) {
        val fragment = PreviewListFragment()
        fragment.arguments = Bundle().apply {
            putSerializable("preview_models", previewList)
            putSerializable("recent_file_model", recentFileModel)
        }
        val transaction = fragmentActivity.supportFragmentManager.beginTransaction()
        if (reverseAnimation) {
            transaction.setCustomAnimations(R.anim.in_reverse_animation, R.anim.out_reverse_animation)
        } else {
            transaction.setCustomAnimations(R.anim.in_animation, R.anim.out_animation)
        }
        transaction.replace(R.id.fragment_container, fragment)
        if (needBackStack)
            transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun showEntryScreen(entryModel: EntryModel?, parentPath: String, recentFileModel: RecentFileModel) {
        val fragment = EntryFragment()
        fragment.arguments = Bundle().apply {
            if(entryModel != null)
                putSerializable("entry_model", entryModel)
            putString("parent_path", parentPath)
            putSerializable("recent_file_model", recentFileModel)
        }
        fragmentActivity.supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.in_animation, R.anim.out_animation).replace(R.id.fragment_container, fragment).addToBackStack(null).commit()
    }

    override fun openPermissionSettings() {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", fragmentActivity.packageName, null)
        intent.data = uri
        fragmentActivity.startActivity(intent)
    }

    override fun openSettings() {
        fragmentActivity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, SettingsFragment()).addToBackStack(null).commit()
    }

    override fun openAbout() {
        fragmentActivity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, AboutFragment()).addToBackStack(null).commit()
    }

    override fun goBack() {
        val fm = fragmentActivity.supportFragmentManager
        if (fm.backStackEntryCount > 0) {
            fm.popBackStack()
        }
    }
}
