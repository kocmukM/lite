package mako.com.lite.presentation.ui.base


interface Presenter<in V : ViewContract> {
    fun attachView(view: V)
    fun detachView()
    fun backPressed()
}