package mako.com.lite.presentation.ui.list

import mako.com.lite.domain.model.*
import mako.com.lite.presentation.ui.base.Presenter
import mako.com.lite.presentation.ui.base.ViewContract
import mako.com.lite.presentation.ui.views.notifications.InsetNotSyncedNotification
import mako.com.lite.presentation.ui.views.notifications.InsetOfflineNotification
import java.util.*

interface PreviewListInterface {
    interface ListView : ViewContract {
        fun drawPreviewList(previewList: MutableList<PreviewInterface>, events: PreviewItemEvents)
        fun drawBreadCrumbs(breadCrumbs: LinkedList<BreadcrumbModel>)
        fun startSearchListener()
        fun clearSearchText()
        fun setGlobalSearchButton()
        fun setBackFromSearchButton()
        fun showSearchButton()
        fun hideSearchButton()

        fun showAddOptions()
        fun hideAddOptions()
        fun showCreateGroupDialog()

        fun setNotSyncedNotification(notSyncedEvents: InsetNotSyncedNotification.NotSyncedEvents)
        fun hideNotificationAt(index: Int)
        fun setOfflineNotification(offlineEvents: InsetOfflineNotification.OfflineEvents)
        fun showSortOptions(byFavorite: Boolean, byName: Boolean)
        fun showDeleteDialog(events: DeleteDialogEvents)

        interface DeleteDialogEvents {
            fun onDelete()
        }

        fun showToolbars()
        fun hideToolbars(events: OnHideEvents)
        fun hideMainToolbar()

        interface OnHideEvents {
            fun onHide()
        }

        fun setEmptyListPlaceholder()
        fun setEmptySearchResultPlaceholder()
        fun hidePlaceholder()
        fun showCopiedToast()
        fun notifyListItemChanged(position: Int)
    }

    interface ListPresenter : Presenter<ListView> {
        fun startWork(previewList: PreviewList, recentFileModel: RecentFileModel)
        fun searchTextChanged(keyword: String)
        fun doGlobalSearchClicked(keyword: String)
        fun backFromSearchClicked()
        fun bigAddFabClicked()
        fun addEntryClicked()
        fun addGroupClicked()
        fun onCreateGroupClicked(groupName: String)
        fun onBreadCrumbClicked(breadCrumbModel: BreadcrumbModel)
        fun sortClicked()
        fun onSortOptionsSelected(byFavourite: Boolean, byName: Boolean)
        fun copyClicked(previewModel: PreviewModel, position: Int)
        fun onResume()
        fun onPause()
    }
}