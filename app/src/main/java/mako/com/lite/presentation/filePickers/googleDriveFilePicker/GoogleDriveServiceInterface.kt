package mako.com.lite.presentation.filePickers.googleDriveFilePicker

import android.content.Intent
import mako.com.lite.domain.model.GoogleRecentFileModel
import java.io.File

interface GoogleDriveServiceInterface {
    fun logInGoogle(logInListener: LogInListener)
    fun pickFiles(pickListener: PickListener)
    fun pickFolder(pickListener: PickListener)
    fun onActivityResult(requestCode: Int, data: Intent?)

}