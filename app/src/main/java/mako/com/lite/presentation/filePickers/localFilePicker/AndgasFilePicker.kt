package mako.com.lite.presentation.filePickers.localFilePicker

import android.content.Context
import com.github.angads25.filepicker.model.DialogConfigs
import com.github.angads25.filepicker.model.DialogProperties
import com.github.angads25.filepicker.view.FilePickerDialog
import java.io.File

class AndgasFilePicker (private val context: Context) : FilePickerInterface {
    val properties = DialogProperties()
    init{
        properties.selection_mode = DialogConfigs.SINGLE_MODE
        properties.root = File("/storage/emulated/0/")
        properties.error_dir = File("/storage/")
        properties.offset = File("/storage/emulated/0/")
        properties.extensions = null
    }

    override fun pickFile(pickFileListener: OnFilePickListener) {


        properties.selection_type = DialogConfigs.FILE_SELECT



        val dialog = FilePickerDialog(context, properties)
        dialog.setTitle("Select a File");
        dialog.setDialogSelectionListener { files ->
            pickFileListener.onPick(files[0])
        }


        dialog.show()
    }

    override fun pickFolder(pickFolderListener: OnFolderPickListener) {

        properties.selection_type = DialogConfigs.DIR_SELECT

        val dialog = FilePickerDialog(context, properties)
        dialog.setTitle("Select a folder")
        dialog.setDialogSelectionListener { files ->
           pickFolderListener.onPick(files[0]+"/")

        }
        dialog.show()
    }

    interface OnFolderPickListener{
        fun onPick(directoryPath: String)
    }

    interface OnFilePickListener{
        fun onPick(filePath: String)
    }

}

