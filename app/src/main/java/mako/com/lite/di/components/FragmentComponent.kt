package mako.com.lite.di.components

import dagger.Subcomponent
import mako.com.lite.di.PerFragment
import mako.com.lite.di.modules.FragmentModule
import mako.com.lite.presentation.copyNotification.CopyNotificationInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.localFilePicker.FilePickerInterface
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooserInterface
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.about.AboutFragment
import mako.com.lite.presentation.ui.blockedPermission.BlockedPermissionFragment
import mako.com.lite.presentation.ui.createFile.CreateFileFragment
import mako.com.lite.presentation.ui.entry.EntryFragment
import mako.com.lite.presentation.ui.list.PreviewListFragment
import mako.com.lite.presentation.ui.settings.SettingsFragment
import mako.com.lite.presentation.ui.signin.SignInFragment
import mako.com.lite.presentation.ui.start.StartFragment
import mako.com.lite.presentation.ui.startup.StartupFragment
import mako.com.lite.presentation.vibro.VibroHelperInterface


@PerFragment
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(blockedPermissionFragment: BlockedPermissionFragment)
    fun inject(startFragment: StartFragment)
    fun inject(signInFragment: SignInFragment)
    fun inject(startupFragment: StartupFragment)
    fun inject(createFileFragment: CreateFileFragment)
    fun inject(previewListFragment: PreviewListFragment)
    fun inject(entryFragment: EntryFragment)
    fun inject(fragment: SettingsFragment)
    fun inject(fragment: AboutFragment)

    fun provideRouter(): RouterInterface
    fun provideFilePicker(): FilePickerInterface
    fun provideVibrator(): VibroHelperInterface
    fun provideGoogleDriveService(): GoogleDriveServiceInterface
    fun provideFileStrategyChooser(): FileStrategyChooserInterface
    fun provideCopy(): CopyNotificationInterface
}