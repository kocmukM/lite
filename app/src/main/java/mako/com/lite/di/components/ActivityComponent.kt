package mako.com.lite.di.components

import dagger.Subcomponent
import mako.com.lite.di.PerActivity
import mako.com.lite.di.modules.ActivityModule
import mako.com.lite.presentation.filePickers.localFilePicker.FilePickerInterface
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.main.MainActivity
import mako.com.lite.presentation.ui.main.RuntimePermissions


@PerActivity
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
   fun inject(mainActivity: MainActivity)
   fun inject(permissions: RuntimePermissions)

   fun provideRouter(): RouterInterface
   fun provideFilePicker(): FilePickerInterface


}