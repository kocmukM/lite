package mako.com.lite.di.components

import dagger.Component
import mako.com.lite.di.ConfigPersistent
import mako.com.lite.di.modules.ActivityModule
import mako.com.lite.di.modules.FragmentModule
import mako.com.lite.presentation.router.RouterInterface


@ConfigPersistent
@Component(dependencies = [ApplicationComponent::class])
interface ConfigPersistentComponent {
    fun activityComponent(activityModule: ActivityModule): ActivityComponent
    fun fragmentComponent(fragmentModule: FragmentModule): FragmentComponent
    fun serviceComponent(serviceComponent: ServiceComponent): ServiceComponent
}