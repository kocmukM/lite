package mako.com.lite.di.modules

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import mako.com.lite.di.ActivityContext
import mako.com.lite.presentation.copyNotification.CopyNotification
import mako.com.lite.presentation.copyNotification.CopyNotificationInterface
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveService
import mako.com.lite.presentation.filePickers.googleDriveFilePicker.GoogleDriveServiceInterface
import mako.com.lite.presentation.filePickers.localFilePicker.AndgasFilePicker
import mako.com.lite.presentation.filePickers.localFilePicker.FilePickerInterface
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooser
import mako.com.lite.presentation.fileStrategyChooser.FileStrategyChooserInterface
import mako.com.lite.presentation.router.Router
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.vibro.VibroHelper
import mako.com.lite.presentation.vibro.VibroHelperInterface


@Module
class FragmentModule(private val fragment: Fragment) {

    private val activity: Activity = fragment.activity!!

    @Provides
    internal fun provideActivity(): Activity = activity

    @Provides
    internal fun provideFragment(): Fragment = fragment

    @Provides
    fun provideRouter(): RouterInterface = Router(activity as AppCompatActivity)

    @Provides
    fun provideFilePicker(): FilePickerInterface = AndgasFilePicker(activity)

    @Provides
    fun provideVibrator(): VibroHelperInterface = VibroHelper(activity)

    @Provides
    fun provideGoogleDriveService(): GoogleDriveServiceInterface = GoogleDriveService(activity)

    @Provides
    @ActivityContext
    internal fun provideContext(): Context = activity

    @Provides
    fun provideFileStrategyChooser(): FileStrategyChooserInterface = FileStrategyChooser(activity)


    @Provides
    fun provideCopy(): CopyNotificationInterface = CopyNotification(activity)
}