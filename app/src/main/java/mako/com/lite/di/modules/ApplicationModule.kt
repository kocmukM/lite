package mako.com.lite.di.modules

import dagger.Module
import dagger.Provides
import mako.com.lite.LiteApplication
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.GoogleDriveFileHelper
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.GoogleDriveFileHelperInterface
import mako.com.lite.data.helpers.crypto.cipherCrypto.CipherCrypto
import mako.com.lite.data.helpers.crypto.cipherCrypto.CipherCryptoInterface
import mako.com.lite.data.helpers.database.DatabaseHelper
import mako.com.lite.data.helpers.database.DatabaseHelperInterface
import mako.com.lite.data.helpers.filesystem.localHelper.FileHelper
import mako.com.lite.data.helpers.filesystem.localHelper.FileHelperInterface
import mako.com.lite.data.helpers.fingerprint.FingerprintHelper
import mako.com.lite.data.helpers.fingerprint.FingerprintHelperInterface
import mako.com.lite.data.helpers.icons.IconHelper
import mako.com.lite.data.helpers.icons.IconHelperInterface
import mako.com.lite.data.helpers.sharedPreferences.SharedPreferencesHelper
import mako.com.lite.data.helpers.sharedPreferences.SharedPreferencesHelperInterface

@Module
class ApplicationModule(private val application: LiteApplication) {

    @Provides
    fun provideCipherCryptoInterface(): CipherCryptoInterface {
        return CipherCrypto()
    }

    @Provides
    fun provideDatabaseHelperInterface(): DatabaseHelperInterface {
        return DatabaseHelper(application.applicationContext)
    }

    @Provides
    fun provideFileHelperInterface(): FileHelperInterface {
        return FileHelper(application.applicationContext)
    }

    @Provides
    fun provideGoogleDriveHelperInterface(): GoogleDriveFileHelperInterface = GoogleDriveFileHelper(application.applicationContext)

    @Provides
    fun provideFingerprintHelperInterface(): FingerprintHelperInterface {
        return FingerprintHelper(application.applicationContext, provideCipherCryptoInterface())
    }

    @Provides
    fun provideSharedPreferencesHelperInterface(): SharedPreferencesHelperInterface {
        return SharedPreferencesHelper(application.applicationContext)
    }

    @Provides
    fun provideIconHelperInterface(): IconHelperInterface {
        return IconHelper
    }


}