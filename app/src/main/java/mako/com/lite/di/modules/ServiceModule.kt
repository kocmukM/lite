package mako.com.lite.di.modules

import android.app.Service
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import mako.com.lite.di.ServiceContext
import mako.com.lite.presentation.router.Router
import mako.com.lite.presentation.router.RouterInterface


@Module
class ServiceModule(private val service: Service) {

    @Provides
    internal fun provideService(): Service = service

    @Provides
    @ServiceContext
    internal fun providesContext(): Context = service

    @Provides
    fun provideRouter(): RouterInterface = Router()
}