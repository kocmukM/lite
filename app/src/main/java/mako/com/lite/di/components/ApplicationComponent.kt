package mako.com.lite.di.components

import dagger.Component
import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.GoogleDriveFileHelperInterface
import mako.com.lite.data.helpers.crypto.cipherCrypto.CipherCryptoInterface
import mako.com.lite.data.helpers.database.DatabaseHelperInterface
import mako.com.lite.data.helpers.filesystem.localHelper.FileHelperInterface
import mako.com.lite.data.helpers.fingerprint.FingerprintHelperInterface
import mako.com.lite.data.helpers.icons.IconHelperInterface
import mako.com.lite.data.helpers.sharedPreferences.SharedPreferencesHelperInterface
import mako.com.lite.di.modules.ApplicationModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun provideCipherCryptoInterface(): CipherCryptoInterface
    fun provideDatabaseHelperInterface(): DatabaseHelperInterface
    fun provideFileHelperInterface(): FileHelperInterface
    fun provideFingerprintHelperInterface(): FingerprintHelperInterface
    fun provideSharedPreferencesHelperInterface(): SharedPreferencesHelperInterface
    fun provideIconHelperInterface(): IconHelperInterface

    fun provideGoogleDriveHelperInterface(): GoogleDriveFileHelperInterface
}