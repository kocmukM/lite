package mako.com.lite.di.components

import dagger.Component
import mako.com.lite.di.PerService
import mako.com.lite.di.modules.ServiceModule
import mako.com.lite.service.overlayingButton.OverlayingButtonService


@PerService
@Component(dependencies = [ApplicationComponent::class], modules = [ServiceModule::class])
interface ServiceComponent {
    fun inject(overlayingButtonService: OverlayingButtonService)
}