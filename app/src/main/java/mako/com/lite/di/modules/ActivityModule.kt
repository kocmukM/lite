package mako.com.lite.di.modules

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import mako.com.lite.di.ActivityContext
import mako.com.lite.presentation.filePickers.localFilePicker.AndgasFilePicker
import mako.com.lite.presentation.filePickers.localFilePicker.FilePickerInterface
import mako.com.lite.presentation.router.Router
import mako.com.lite.presentation.router.RouterInterface
import mako.com.lite.presentation.ui.main.RuntimePermissions


@Module
class ActivityModule(private val mActivity: Activity) {

    @Provides
    fun provideActivity(): Activity = mActivity


    @Provides
    fun provideRouter(): RouterInterface = Router(mActivity as AppCompatActivity)

    @Provides
    fun provideFilePicker(): FilePickerInterface = AndgasFilePicker(mActivity)

    @Provides
    fun providePermissions(): RuntimePermissions = RuntimePermissions(mActivity as AppCompatActivity)


    @Provides
    @ActivityContext
    fun providesContext(): Context = mActivity
}