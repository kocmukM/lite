package mako.com.lite.testHelpers

import mako.com.lite.data.helpers.filesystem.localHelper.FileHelperInterface
import java.io.File
import java.io.FileOutputStream

class TestFileHelper : FileHelperInterface {
    override fun replaceRestrictedSymbolsFromFileNameFrom(fileName: String): String {
        var resultFileName = fileName
        resultFileName = resultFileName.replace("\\", "")
        resultFileName = resultFileName.replace("/", "")
        resultFileName = resultFileName.replace("?", "")
        resultFileName = resultFileName.replace("!", "")
        resultFileName = resultFileName.replace("=", "")
        return resultFileName
    }

    @Throws(Exception::class)
    override fun createFile(fileDirectory: String, fileName: String): File {
        val restrictedSymbols = arrayOf("/", "\\", "?", "!", "\"")
        for (restrictedSymbol in restrictedSymbols) {
            if (fileName.contains(restrictedSymbol))
                throw Exception()
        }


        val newFile = getTestingFile(fileDirectory, fileName)
        if (newFile.exists()) {
//            throw FileAlreadyExistsException(newFile)
        }

        newFile.createNewFile()
        return newFile
    }

    override fun deleteFile(fileDirectory: String, fileName: String): Boolean {
        val file = getTestingFile(fileDirectory, fileName)
        return file.delete()
    }

    override fun getFile(fileDirectory: String, fileName: String): File? {
        val file = getTestingFile(fileDirectory, fileName)
        return if (file.exists()) file else null

    }

    override fun getGoogleCahceDirectory(): String {
        val applicationDirectory = getApplicationDirectory()
        val cacheDirectory = "$applicationDirectory/google_cache/"
        val cacheDirectoryFile = File(cacheDirectory)
        if (!cacheDirectoryFile.exists())
            cacheDirectoryFile.mkdir()
        return cacheDirectory
    }

    override fun writeToFile(src: ByteArray, dst: File) {
        val fileOS = FileOutputStream(dst)
        fileOS.write(src)
        fileOS.flush()
        fileOS.close()
    }

    private fun getTestingFile(fileDirectory: String, fileName: String): File {
        val realFileDirectory = getApplicationDirectory()
        val directory = File(realFileDirectory).mkdir()
        return File("$realFileDirectory${getFakeFileName(fileDirectory, fileName)}")
    }

    private fun getFakeFileName(fileDirectory: String, fileName: String): String {
        val fromDirectory = fileDirectory.replace("/", "").replace("\\", "")
        return "$fromDirectory$fileName"
    }

    fun getApplicationDirectory(): String {
        return "${System.getProperty("user.dir")!!}/testsEnvirontment/"
    }
}