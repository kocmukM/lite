package mako.com.lite.testHelpers

import mako.com.lite.data.helpers.filesystem.cloudHelpers.googleDriveHelper.*
import java.io.File

class TestGoogleDriveFileHelper : GoogleDriveFileHelperInterface {

    override fun updateCacheFile(fileCache: File, stringDriveId: String, fileUpdateListener: FileUpdateListener) {
        fileUpdateListener.onFileUpdated(fileCache)
    }

    override fun updateDriveFile(fileCache: File, stringDriveId: String) {

    }

    override fun createFile(stringDriveFolderId: String, fileCache: File, fileName: String, fileCreateListener: FileCreateListener) {
        val directory = getDriveFolder()
        val file = File(directory + "testDriveId")
        file.createNewFile()
        file.outputStream().use {
            val fileBytes = fileCache.readBytes()
            it.write(fileBytes)
            it.flush()
        }
        fileCreateListener.onFileCreated("testDriveId")
    }

    override fun deleteDriveFile(stringDriveId: String) {
        val directory = getDriveFolder()
        File(directory + "testDriveId").delete()
    }

    override fun createFile(stringDriveFolderId: String, fileName: String, fileCreateListener: FileCreateListener) {
        val directory = getDriveFolder()
        val directoryFile = File(directory)
        directoryFile.mkdir()
        val file = File(directory + "testDriveId")
        file.createNewFile()
    }

    override fun getFileNameBy(stringDriveId: String, fileNameListener: FileNameListener) {
        fileNameListener.onFileName("test", "kdbx")
    }


    override fun getFolderNameBy(stringDriveId: String, folderNameListener: FolderNameListener) {
        folderNameListener.onFolderName("cloudEmulation")
    }

    private fun getDriveFolder(): String {
        return "${System.getProperty("user.dir")!!}/testsEnvirontment/cloudEmulation/"
    }
}