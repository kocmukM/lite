package mako.com.lite

import mako.com.lite.data.repository.FileRepository
import mako.com.lite.domain.FilesUseCase
import mako.com.lite.domain.model.GoogleRecentFileModel
import mako.com.lite.domain.model.LocalRecentFileModel
import mako.com.lite.domain.model.RecentFileModel
import mako.com.lite.testHelpers.TestFileHelper
import mako.com.lite.testHelpers.TestGoogleDriveFileHelper
import org.junit.Test
import java.io.File

class FileUseCaseTest {
    val testFileHelper = TestFileHelper()
    val fileRepository = FileRepository(testFileHelper, TestGoogleDriveFileHelper())
    val fileUseCase = FilesUseCase(fileRepository)

    private val localFileDirectory = "/localtest/creationText/"
    private val fileName = "file.kdbx"

    @Test
    fun testCreateFile() {
        fileUseCase.createFile(LocalRecentFileModel(localFileDirectory, fileName, "123", System.currentTimeMillis()), object : FilesUseCase.CreateFileEvents {
            override fun onCreated(recentFileModel: RecentFileModel, kdbxFile: File) {
                check(kdbxFile.exists())
                System.out.println("Файл успешно создан")
                testDeleteFile(recentFileModel)
            }

            override fun onError(exception: Exception) {
                throw exception
            }

        }, localFileDirectory)
    }

    private fun testDeleteFile(recentFileModel: RecentFileModel) {
        val kdbxFile = fileUseCase.getFile(recentFileModel)
        System.out.println("Получен инстанс файла")
        fileUseCase.deleteCachedFile(recentFileModel)
        check(!kdbxFile.exists())
        System.out.println("Файл успешно удален")
    }


    @Test
    fun testGoogleDrive() {
        testCreateGoogleFile(LocalRecentFileModel(testFileHelper.getApplicationDirectory() + "source/", "lite.kdbx", "123", System.currentTimeMillis()))
    }


    private fun testCreateGoogleFile(recentFileModel: RecentFileModel) {
        val googleRecentFileModel = GoogleRecentFileModel(recentFileModel.fileDirectory, recentFileModel.fileName, "", recentFileModel.filePassword, recentFileModel.fileOpenTime)
        fileUseCase.createFile(googleRecentFileModel, object : FilesUseCase.CreateFileEvents {
            override fun onCreated(recentFileModel: RecentFileModel, kdbxFile: File) {
                System.out.println("Гугл файл успешно создан")
                testWriteFileBytesToGoogleFile()
            }

            override fun onError(exception: Exception) {
                throw exception
            }

        }, localFileDirectory)
    }

    private fun testWriteFileBytesToGoogleFile() {
        checkNotNull(null)
    }

    private fun testUpdateCacheFile(recentFileModel: RecentFileModel) {
        fileUseCase.updateLocalFile(recentFileModel, object : FilesUseCase.GetFileEvents {
            override fun onFile(file: File) {
                check(file.length() > 0)
                System.out.println("Успешно записаны данные в файл")
                testDeleteFile(recentFileModel)
            }

            override fun onError(e: Exception) {
                throw e
            }

        }, File("${testFileHelper.getApplicationDirectory()}lite.kdbx"))
    }
}